SCHVisor
========

Visor de imágenes simple y liviano, con buscador de similitud y borrado seguro.


Descripción
----

SCHVisor es un visor de imágenes simple y liviano. Carga las imágenes por carpeta. 
Se puede marcar las imágenes con grupos (teclas `1` al `9`) para moverlas, 
renombrarlas, copiarlas, limpiarlas (programa externo MAT), normalizar sus 
extensiones (en función del formato del archivo). Puede marcarse las imágenes para 
ser borradas (tecla `Supr`). Tiene un buscador de imágenes (recla `f`) por 
parecido de forma y/o histograma (color o gris). Pueden limitarse las imágenes 
listadas por tamaño y extensión. Cuenta con una lista negra de imágenes dañadas 
o no reconocidas. Al hacer zoom, rotar o reflejar (accesibles desde las teclas 
`+`/`-`, `h`/`v`, `e`/`r`) el programa las recuerda entre sesiones además de 
permitir guardar el archivo con esas modificaciones. Permite ir a una imagen 
(tecla `i`) por posiciones o nombre (se busca la similitud del nombre dado). 
Desde configuración se puede fijar entre 13 niveles de borrado (1 normal y 12 seguros 
con los programas externos SHRED y/o SRM). Se avanza/retrocede entre las imágenes 
y carpetas con las teclas de flechas.

La subcarpeta `SCHVisor/app/css` puede borrarse sin problemas, si SCHVisor la 
encuentra la usa sino la ignora, fija el aspecto (Gtk-theme) de la aplicación. 
El archivo `SCHVisor/app/cfg.dir` fija la carpeta de configuraciones, si no 
existe se usa `~/.config/schcriher/schvisor`.


Requisitos
----
Requiere de Python 3.2 o mayor y GTK 3.


Instalación
----
Copiar la carpeta `SCHVisor` en cualquier lugar y ejecutar `SCHVisor/app/main`.


Desinstalación
----
Eliminar la carpeta `SCHVisor` y opcionalmente la carpeta que figura en el archivo 
`SCHVisor/app/cfg.dir`.


Sitio web
----
 * Descargar la última versión <https://github.com/schcriher/SCHVisor/releases>
 * Informar fallos y sugerencias <https://github.com/schcriher/SCHVisor/issues>


Programas externos
----
 * MAT: Metadata anonymisation toolkit <https://mat.boum.org/>
 * SHRED: Herramientas básicas de GNU <http://gnu.org/software/coreutils>
 * SRM: Secure Remove <http://www.thc.org>
