#!/usr/bin/env python3
#-*- coding: utf-8 -*-
#
# Copyright (C) 2013 Cristian Hernán Schmidt
#
# This file is part of SCHVisor.
#
# SCHVisor is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# SCHVisor is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with SCHVisor. If not, see <http://www.gnu.org/licenses/>.

# TODO Restructurar los archivos py para el frizado, cambiar el uso de __file__

import os
import gi
import sys
import logging

gi.require_version('Gtk', '3.0')

VERSION = '1.0b'

if sys.version_info[0:2] < (3, 2):
    raise RuntimeError('SCHVisor {} funciona con Python >= 3.2'.format(VERSION))

base = os.path.dirname(__file__)
sys.path.insert(0, os.path.join(base, 'py'))

from viewer import Visor, GetCfg

cfg_dir = os.path.join(base, 'cfg.dir')
try:
    with open(cfg_dir) as fid:
        path = fid.read().strip()
except:
    path = os.path.join('~', '.config', 'schcriher', 'schvisor')
    print('Usando cfg.dir por defecto ({})'.format(path))
    path = os.path.expanduser(path)
finally:
    getcfg = GetCfg(path, cfg_dir)

logging.basicConfig(
    level = logging.DEBUG,
    #format = '%(m)s%(message)s'
    format = '%(asctime)s %(levelname)-8s %(threadName)-12s %(module)-8s%(funcName)-29s %(m)s%(message)s',
    datefmt = '%Y/%m/%d %H:%M:%S',
    filename = getcfg('logging').path,
    filemode = 'w',
)

if __name__ == "__main__":
    if len(sys.argv) > 1:
        d = abspath(sys.argv[1])
        ini = d if os.path.isfile(d) or os.path.isdir(d) else None
    else:
        ini = None
    app = Visor(VERSION, getcfg, ini)
    app.run()
