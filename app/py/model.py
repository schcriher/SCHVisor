#!/usr/bin/env python3
#-*- coding: utf-8 -*-

# Copyright (C) 2013 Cristian Hernán Schmidt
#
# This file is part of SCHVisor.
#
# SCHVisor is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# SCHVisor is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with SCHVisor. If not, see <http://www.gnu.org/licenses/>.

import os
import re
import sys
import json
import time
import zlib
import shutil
import pickle
import logging
import inspect
import weakref
import functools
import collections

from copy import copy
from datetime import datetime
from itertools import combinations, product, accumulate, compress  # Python>=3.2
from subprocess import Popen, PIPE
from gi.repository import Gdk, GObject

from debug import Log, Logging
from utils import (fborrar, frenombrar, normalizar, unidad_a_multiplo, color, get_ext)

log = Log(logging.getLogger(__name__), sub=1)



class Fichero:

    def __repr__(self):
        return '<{}>'.format(os.path.basename(self.dirección))

    def __str__(self):
        return self.dirección

    def __eq__(self, obj):  # self == obj
        normpath = os.path.normpath
        return normpath(self.dirección) == normpath(self.__obj_to_path(obj))

    def __ne__(self, obj):  # self != obj
        return not self.__eq__(obj)

    def __lt__(self, obj):  # self < obj
        return normalizar(self.dirección) < normalizar(self.__obj_to_path(obj))

    def __le__(self, obj):  # self <= obj
        return self.__lt__(obj) or self.__eq__(obj)

    def __gt__(self, obj):  # self > obj
        return normalizar(self.dirección) > normalizar(self.__obj_to_path(obj))

    def __ge__(self, obj):  # self >= obj
        return self.__gt__(obj) or self.__eq__(obj)

    def __bool__(self):     # bool(self), sino usa len(self)
        return os.path.exists(self.dirección)

    def __hash__(self):
        return hash(self.dirección)

    def __len__(self):
        return self.tamaño

    def __obj_to_path(self, obj):
        if hasattr(obj, 'dirección'):
            return obj.dirección
        elif isinstance(obj, str):
            return obj
        else:
            return ''

    @property
    def mtepoch(self):
        return os.path.getmtime(self.dirección)

    def mfecha(self, formato=None):
        fecha = datetime.fromtimestamp(os.path.getmtime(self.dirección))
        return fecha.strftime(formato) if formato else fecha



class Archivo(Fichero):

    def __init__(self, carpeta, nombre):
        self.carpeta = carpeta  # objeto
        self.nombre = nombre  # str

    @property
    def dirección(self):
        return os.path.join(self.carpeta.dirección, self.nombre)

    @property
    def base(self):
        return os.path.splitext(self.nombre)[0]

    @property
    def ext(self):
        return os.path.splitext(self.nombre)[1]

    @property
    def ext_format(self):
        return get_ext(self.dirección)

    @property
    def formato(self):
        return self.ext_format.strip(os.extsep)

    @property
    def tamaño(self):
        return os.path.getsize(self.dirección)

    #@Logging
    def borrar(self, borseg=None):
        if os.path.exists(self.dirección):
            método, comando = fborrar(self.dirección, borseg)
            log.info('Se eliminó [%s%s]: %s', método, comando, self.dirección)
            self.carpeta.eliminar(self)
            self.carpeta = None
            self.nombre = None
            return True

    #@Logging
    def renombrar(self, carpeta=None, nombre=None, borseg=None, ordenar=True):
        dirección = os.path.join(
            (carpeta or self.carpeta).dirección,
            nombre or self.nombre)
        if dirección != self.dirección:
            método, comando = frenombrar(self.dirección, dirección, borseg)
            var = (método, comando, self.dirección, dirección)
            log.info('Se renombró [%s%s]: %s -> %s', *var)
            if carpeta:
                self.carpeta.eliminar(self)
                self.carpeta = carpeta
            if nombre:
                self.nombre = nombre
            if carpeta:
                self.carpeta.agregar(self)
            if ordenar:
                self.carpeta.ordenar()
            return True

    #@Logging
    def copiar(self, carpeta=None, nombre=None, ordenar=True):
        carpeta = carpeta or self.carpeta
        nombre = nombre or self.nombre
        dirección = os.path.join(carpeta.dirección, nombre)
        if dirección != self.dirección:
            shutil.copy2(self.dirección, dirección)
            log.info('Se copio: %s -> %s', self.dirección, dirección)
            archivo = self.__class__(carpeta, nombre)
            carpeta.agregar(archivo)
            if ordenar:
                carpeta.ordenar()
            return archivo



class Carpeta(Fichero):

    def __init__(self, dirección, cfg=None, lisneg=None):
        self.dirección = os.path.normpath(dirección)  # str
        self.archivos = []
        self.lisneg = lisneg
        self.cfg = cfg
        self.aa = None  # archivo activo
        self._iaa = None  # índice del archivo activo
        self._mfecha = None  # fecha de modificación

    def __iter__(self):
        return iter(self.archivos)

    #@Logging
    def __getitem__(self, key):
        if isinstance(key, int):
            return self.archivos[key]
        elif isinstance(key, (str, Archivo)):
            for archivo in self.archivos:
                if archivo == key:
                    return archivo
        raise KeyError('key={} (type={})'.format(key, type(key)))

    #@Logging
    def __contains__(self, key):
        if isinstance(key, (Archivo, str)):
            try:
                archivo = self.__getitem__(key)
                return True
            except:
                pass
        return False

    #@Logging
    def get(self, key, default=None):
        try:
            return self[key]
        except:
            return default

    #@Logging
    def listar_archivos(self):
        self._mfecha = self.mfecha()
        for nombre in sorted(os.listdir(self.dirección), key=normalizar):
            dirección = os.path.join(self.dirección, nombre)
            if os.path.isfile(dirección) and dirección not in self.lisneg:
                fext = get_ext(dirección, self.cfg['extpc']).lstrip(os.extsep)
                ext = fext in self.cfg['exts']
                tam = self.cfg['tmin'] <= os.path.getsize(dirección) <= self.cfg['tmax']
                if ext and tam:
                    yield Archivo(self, nombre)  # ojo si se tarda mucho (self._mfecha)

    #@Logging
    def listar(self, activar=0):
        "activar puede ser int, Archivo o str (dirección)"
        self.archivos = []
        for archivo in self.listar_archivos():
            self.archivos.append(archivo)
        self.i = activar

    #@Logging
    def actualizar(self, forzar=False):
        if forzar or self._mfecha != self.mfecha():
            cargados = set(self.archivos)
            existentes = set(self.listar_archivos())
            eliminados = []
            nuevos = []
            for archivo in cargados - existentes:
                self.eliminar(archivo)
                eliminados.append(archivo)
            for archivo in existentes - cargados:
                self.agregar(archivo)
                nuevos.append(archivo)
            if nuevos:
                self.ordenar()
            return eliminados, nuevos

    @property
    def nombre(self):
        return os.path.basename(self.dirección)

    @property
    def tamaño(self):
        return len(self.archivos)

    @property
    def i(self):
        return self._iaa

    @i.setter
    def i(self, i):
        if i in self.archivos:
            i = self.archivos.index(i)
        if isinstance(i, int) and 0 <= i < self.tamaño:
            self.aa = self.archivos[i]
            self._iaa = i
        else:
            self.aa = None
            self._iaa = None

    #@Logging
    def set(self, offset=0, overflow='auto', setter=None):
        n = self.tamaño - 1
        if n >= 0:
            if isinstance(setter, int):
                i = setter
            elif isinstance(setter, (str, Archivo)):
                i = self.archivos.index(setter)
            else:
                if offset == 'ini':
                    i = 0
                elif offset == 'fin':
                    i = n
                else:
                    i = self.i + offset
            if not (0 <= i <= n):
                if overflow == 'ini':
                    i = 0
                elif overflow == 'fin':
                    i = n
                elif overflow == 'auto' and isinstance(setter, int):
                    i = 0 if setter <= 0 else n
                elif overflow == 'auto' and isinstance(offset, int):
                    i = 0 if offset <= 0 else n
                else:
                    e = '0 <= i({}) <= n({}), offset={}, overflow={}'
                    raise OverflowError(e.format(i, n, offset, overflow))
            self.i = i
        else:
            self.i = None

    #@Logging
    def agregar(self, archivo):
        if isinstance(archivo, Archivo) and archivo not in self.archivos:
            self.archivos.append(archivo)
            if self.i is None:
                self.i = 0

    #@Logging
    def eliminar(self, archivo):
        if archivo in self.archivos:
            self.archivos.remove(archivo)
            if archivo == self.aa:
                self.set(offset=-1)
            else:
                self.i = self.aa

    #@Logging
    def ordenar(self):
        self.archivos = sorted(self.archivos, key=normalizar)
        self.i = self.aa



class Carpetas:

    def __init__(self, dog):
        self.carpetas = []
        self.watchdog = {}
        self.timeout = None
        self.dog = dog  # watchdog de viewer.Visor

    def __repr__(self):
        return '<{}:{}>'.format(self.__class__.__name__, id(self))

    def __bool__(self):
        return bool(self.carpetas)

    def __len__(self):
        return len(self.carpetas)

    def __iter__(self):
        return iter(copy(self.carpetas))

    #@Logging
    def __getitem__(self, key):
        if isinstance(key, int):
            return self.carpetas[key]
        elif isinstance(key, (str, Carpeta)):
            for carpeta in self:
                if carpeta == key:
                    return carpeta
        raise KeyError('key={} (type={})'.format(key, type(key)))

    #@Logging
    def __contains__(self, key):
        if isinstance(key, (Carpeta, str)):
            try:
                carpeta = self[key]
                return True
            except:
                pass
        return False

    #@Logging
    def get(self, key, default=None):
        try:
            return self[key]
        except:
            return default

    #@Logging
    def get_archivo(self, key, default=None):
        for carpeta in self:
            for archivo in carpeta:
                if key == archivo:
                    return archivo
        return default

    #@Logging
    def index(self, carpeta):
        return self.carpetas.index(carpeta)

    #@Logging
    def agregar(self, carpeta):
        self.carpetas.append(carpeta)
        self.set_watchdog(carpeta)

    #@Logging
    def eliminar(self, carpeta):
        self.unset_watchdog(carpeta)
        self.carpetas.remove(carpeta)

    #@Logging
    def set_watchdog(self, carpeta):
        if self.timeout and carpeta not in self.watchdog:
            dog = GObject.timeout_add(self.timeout, self.dog, carpeta)
            self.watchdog[carpeta] = dog
            log.debug('Watchdog activado, id=%s', dog)

    #@Logging
    def unset_watchdog(self, carpeta):
        if self.timeout and carpeta in self.watchdog:
            dog = self.watchdog.pop(carpeta)
            GObject.source_remove(dog)
            log.debug('Watchdog desactivado, id=%s', dog)

    #@Logging
    def set_all_watchdogs(self):
        for carpeta in self.carpetas:
            self.set_watchdog(carpeta)

    #@Logging
    def unset_all_watchdogs(self):
        for carpeta in self.carpetas:
            self.unset_watchdog(carpeta)
        if self.watchdog:
            log.error('Quedaron watchdog sin desactivado: %s', self.watchdog)

    #@Logging
    def set_timeout(self, timeout):
        if timeout != self.timeout:
            self.unset_all_watchdogs()
            self.timeout = int(timeout)
            self.set_all_watchdogs()



class Banderas:

    def __init__(self):
        self.multiple = None  # multiples banderas por archivo
        self.colores = None  # colores a mostrar en estado
        self.estados = {i:set() for i in range(1, 11)}
        self.plantillas = {
            True:  '<span foreground="{}">{{}}</span>',
            False: '<span foreground="{}">{{}}</span>',
        }

    def __bool__(self):
        return any(bool(i) for i in self.estados.values())

    def __repr__(self):
        return '<{}:{}>'.format(self.__class__.__name__, id(self))

    @property
    def estado(self):
        plantillas = {
            False: self.plantillas[False].format(self.colores[0]),
            True:  self.plantillas[True].format(self.colores[1]),
        }
        label = [plantillas[bool(self.estados[10])].format('B')]
        for b in range(1, 10):
            label.append(plantillas[bool(self.estados[b])].format(b))
        return ' '.join(label)

    #@Logging
    def get(self, key, default=None, lista=False):
        """key puede ser int, str o Archivo
           lista=True devuelve una lista si key es str o Archivo"""
        if isinstance(key, int):
            return sorted(self.estados.get(key, default))
        elif isinstance(key, (str, Archivo)):
            banderas = []
            for bandera, archivos in self.estados.items():
                if key in archivos:
                    banderas.append(bandera)
            banderas.sort()
            if banderas:
                return banderas if self.multiple or lista else banderas[0]
        return default

    #@Logging
    def pop(self, archivo, default=None):
        if isinstance(archivo, (str, Archivo)):
            banderas = self.get(archivo, default)
            self.unset(archivo)
            return banderas
        else:
            raise KeyError(archivo)

    #@Logging
    def set(self, archivo, banderas, unicos=False):
        "unicos=True deja seteadas solo las banderas pasadas aquí"
        b_existentes = set(self.get(archivo, [], lista=True))
        b_aplicables = set([banderas] if isinstance(banderas, int) else banderas)
        if 10 in b_aplicables:
            for b in b_existentes:
                self.unset(archivo, b)
            for b in b_aplicables:
                self.unset(archivo, b)
            aplicar = [10]
        elif 10 in b_existentes:
            self.unset(archivo, 10)
        else:
            if not self.multiple or unicos:
                for b in b_existentes - b_aplicables:
                    self.unset(archivo, b)
                    log.debug('sacando bandera %s del archivo %s', b, archivo)

            aplicar = b_aplicables - b_existentes
            if not self.multiple and len(aplicar) > 1:
                raise ValueError('Modo "no multiple" solo puede aplicar una bandera')
        for b in aplicar:
            self.estados[b].add(archivo)

    #@Logging
    def unset(self, archivo=None, banderas=None):
        banderas = [banderas] if isinstance(banderas, int) else banderas
        banderas = banderas or self.get(archivo, lista=True)
        for bandera in banderas or self.estados:
            if archivo:
                if archivo in self.estados[bandera]:
                    self.estados[bandera].remove(archivo)
            else:
                self.estados[bandera] = set()

    #@Logging
    def listar(self, banderas=None):
        if banderas is None:
            banderas = list(self.estados)
        elif isinstance(banderas, int):
            banderas = [banderas]
        if any(b not in self.estados for b in banderas):
            raise ValueError('Alguna bandera incorrecta: {}'.format(banderas))
        return sorted(banderas)

    #@Logging
    def get_activas(self, posibles=None):
        p = self.listar(posibles)
        return {b:len(self.estados[b]) for b in p if self.estados[b]}

    #@Logging
    def set_multiple(self, multiple):
        if multiple != self.multiple:
            self.multiple = multiple
            if not multiple:
                archivos = set()
                for a in self.estados.values():
                    archivos.update(a)
                for archivo in archivos:
                    banderas = self.get(archivo, lista=True)
                    if len(banderas) > 1:
                        self.set(archivo, banderas[0], unicos=True)



class FormatosDatos:
    "Información del pixbuf"

    def __init__(self, default, nmap, imap):
        self.default = default
        self.nmap = nmap
        self.imap = imap
        self.n = len(nmap)
        self.__dict__.update(default)

    def __repr__(self):
        return '<{}:{}>'.format(self.__class__.__name__, id(self))

    def __eq__(self, obj):  # self == obj
        for key in self.imap:
            if getattr(self, key) != getattr(obj, key):
                return False
        return True

    def __ne__(self, obj):  # self != obj
        return not self.__eq__(obj)

    #@Logging
    def reset(self, *keys):
        keys = keys or ('ancho', 'alto', 'rel', 'fh', 'fv')
        for key in keys:
            setattr(self, key, self.default[key])

    #@Logging
    def girar(self, giro):
        self.giro = (self.giro + giro) % 360
        giro = giro % 360
        if giro == 90:
            self.ancho, self.alto = self.alto, self.ancho
            self.rel = 1 / self.rel
            self.fh, self.fv = self.fv, 1 - self.fh
        elif giro == 180:
            self.fh, self.fv = 1 - self.fh, 1 - self.fv
        elif giro == 270:
            self.ancho, self.alto = self.alto, self.ancho
            self.rel = 1 / self.rel
            self.fh, self.fv = 1 - self.fv, self.fh

    #@Logging
    def reflejo(self, eje):
        if eje == 'h':
            self.rh = not self.rh
            self.fh = 1 - self.fh
        elif eje == 'v':
            self.rv = not self.rv
            self.fv = 1 - self.fv

    #@Logging
    def indices(self, keys):
        """Devuelve una lista con los índices numéricos.
           Excepcion: en el caso de pasar 'nombre1':'nombre2' ambos se incluyen
        """
        if not isinstance(keys, tuple):
            keys = (keys,)
        for key in keys:
            if isinstance(key, slice):
                start = self.imap[key.start] if key.start in self.imap else key.start
                stop = self.imap[key.stop] + 1 if key.stop in self.imap else key.stop
                for i in range(*slice(start, stop, key.step).indices(self.n)):
                    yield i
            elif isinstance(key, int):
                if key < 0:
                    key += self.n
                if not (0 <= key < self.n):
                    raise IndexError("Índice ({}) fuera de rango".format(key))
                yield key
            elif key in self.imap:
                yield self.imap[key]
            else:
                raise TypeError("Tipo de argumento inválido para generar índices")

    #@Logging
    def __getitem__(self, key):
        values = [getattr(self, self.nmap[i]) for i in self.indices(key)]
        return values if len(values) > 1 else values[0]

    #@Logging
    def __setitem__(self, key, value):
        indices = self.indices(key)
        if hasattr(value, '__iter__'):
            for iv, ik in enumerate(indices):
                setattr(self, self.nmap[ik], value[iv])
        else:
            for ik in indices:
                setattr(self, self.nmap[ik], value)

    #@Logging
    def is_zoom(self):
        return all((self.ancho, self.alto, self.rel))

    #@Logging
    def is_giro_90(self):
        return self.giro in (90, 270)



class Formatos:

    def __init__(self, getcfg):
        self.archivo = getcfg('formatos')
        self.imágenes = weakref.WeakKeyDictionary()
        self.buffer = {}
        self.default = collections.OrderedDict([
            # zoom
            ('ancho',   None),  # 0  pixel
            ('alto',    None),  # 1  pixel
            ('rel',     None),  # 2  ancho / alto
            # Fracciones
            ('fh',       0.5),  # 3  alineacion de la imagen en x
            ('fv',       0.5),  # 4  alineacion de la imagen en y
            # Giro
            ('giro',       0),  # 5  ángulo
            # Reflejos
            ('rh',     False),  # 6  ¿aplicar reflejo horizontal?
            ('rv',     False),  # 7  ¿aplicar reflejo vertical?
        ])
        self.nmap = dict(enumerate(self.default))
        self.imap = {n: i for i, n in self.nmap.items()}

    def __repr__(self):
        return '<{}:{}>'.format(self.__class__.__name__, id(self))

    def __bool__(self):
        return bool(self.imágenes)

    #@Logging
    def cargar(self):
        try:
            log.info('Cargando formatos')
            with open(self.archivo.path, mode='rb') as fichero:
                zdatos, crc32 = pickle.load(fichero)
            if crc32 == zlib.crc32(zdatos):
                datos = zlib.decompress(zdatos)
                formatos = json.loads(datos.decode())
                for ckey, archivos in formatos.items():
                    for akey, formato in archivos.items():
                        self.buffer.setdefault(ckey, {})[akey] = formato
            else:
                log.warning('CRC32 inválido para %s', self.archivo)
        except:
            exctype, value = sys.exc_info()[:2]
            log.debug('ERROR en formatos: %s: %s', exctype.__name__, value)

    #@Logging
    def guardar(self):
        try:
            log.info('Guardando formatos')
            formatos = {c: f for c, f in self.buffer.items() if f}
            for archivo, formato in self.imágenes.items():
                ckey = archivo.carpeta.dirección
                akey = archivo.nombre
                formatos.setdefault(ckey, {})[akey] = formato[:]
            datos = json.dumps(formatos, separators=(',', ':')).encode()
            zdatos = zlib.compress(datos)
            crc32 = zlib.crc32(zdatos)
            tamaño = unidad_a_multiplo(len(datos))
            ratio = len(zdatos) / len(datos)
            log.debug('Datos de formatos %s (compresión %.3f)', tamaño, ratio)
            with open(self.archivo.path, mode='wb') as fichero:
                pickle.dump((zdatos, crc32), fichero)
        except:
            log.exception('Archivo %s', self.archivo)

    #@Logging
    def get(self, archivo, setter=False, default=None):
        if default is None:
            nuevo = FormatosDatos(self.default, self.nmap, self.imap)
        else:
            nuevo = default
        datos = self.imágenes.get(archivo, nuevo)
        if default is None:
            try:
                datos[:] = self.buffer[archivo.carpeta.dirección].pop(archivo.nombre)
                self.imágenes[archivo] = datos
                log.sch('Formato recuperado del buffer')
            except:
                if setter:
                    self.imágenes[archivo] = datos
        return datos

    #@Logging
    def set(self, archivo, *args, **kwargs):
        "Los argumentos pasado por nombre sobreescriben a los pasados por posición"
        if len(args) == 1 and isinstance(args[0], FormatosDatos):
            self.imágenes[archivo] = args[0]
        else:
            datos = self.get(archivo)
            for i, v in enumerate(args):
                setattr(datos, self.nmap[i], v)
            for n, v in kwargs.items():
                setattr(datos, n, v)
            self.imágenes[archivo] = datos

    #@Logging
    def buscar_cambios(self):
        variables = ('ancho', 'alto', 'giro', 'rh', 'rv')
        valores = tuple(self.default[v] for v in variables)
        imágenes = {}
        for archivo, fimagen in self.imágenes.items():
            for var, val in zip(variables, valores):
                if fimagen[var] != val:
                    imágenes[archivo] = fimagen
                    break
        return imágenes

    #@Logging
    def cargar_archivo(self, archivo):
        try:
            datos = FormatosDatos(self.default, self.nmap, self.imap)
            datos[:] = self.buffer[archivo.carpeta.dirección].pop(archivo.nombre)
            self.imágenes[archivo] = datos
            log.sch('Formato recuperado del buffer')
            return True
        except KeyError:
            return False

    #@Logging
    def buff(self):
        for ckey in tuple(self.buffer.keys()):
            if not self.buffer[ckey]:
                self.buffer.pop(ckey)
        return bool(self.buffer)

    #@Logging
    def reset(self):
        self.imágenes.clear()
        self.buffer.clear()

    #@Logging
    def keymake(self, archivo):
        if not isinstance(archivo, Archivo):
            carpeta = Carpeta(os.path.dirname(archivo))
            archivo = Archivo(carpeta, os.path.basename(archivo))
        return archivo

    #@Logging
    def unset(self, archivo):
        archivo = self.keymake(archivo)
        if archivo in self.imágenes:
            del self.imágenes[archivo]
        ckey = archivo.carpeta.dirección
        akey = archivo.nombre
        if ckey in self.buffer and akey in self.buffer[ckey]:
            del self.buffer[ckey][akey]

    #@Logging
    def pop(self, archivo, default=None):
        archivo = self.keymake(archivo)
        datos = self.imágenes.get(archivo, default)
        self.unset(archivo)
        return datos



class CacheFingerprint:

    def __init__(self):
        self._cache = {}
        self._activo = False

    #@Logging
    def activar(self, activar):
        self._cache.clear()
        self._activo = activar

    #@Logging
    def get(self, archivo):
        if self._activo:
            return self._cache.get(archivo)

    #@Logging
    def set(self, archivo, response):
        if self._activo:
            self._cache[archivo] = response



class Fingerprint:

    # FP inspirado en el algoritmo de detección de duplicados de Rob Kudla
    # http://www.ostertag.name/HowTo/findimagedupes.pl
    # http://www.jhnc.org/findimagedupes
    cmdset = (
        # comun
        "convert '{{}}' ",    # Imagen
        "-sample {0}x{0}! ",  # Tamaño inicial (default 160x160)
        "-modulate 100,0 ",   # Escala de grises (para fingerprint)
        "-blur 3x99 ",        # Reducción de ruido
        "-normalize ",        # Extender la intensidad tanto como sea posible
        "-equalize ",         # Aumentar el contraste tanto como sea posible
        # fingerprint
        "-sample {1}x{1} ",   # Reducción (default 16x16 = 256 pixeles)
        "-threshold 50% ",    # Dos colores (default 256 bits)
        "mono:-",             # Bi-level bitmap in least-significant-byte first order
        # histogram
        "-colorspace Gray ",  # Espacio de color
        "-colorspace RGB ",   # Espacio de color
        "-depth {1} ",        # Bits de profundidad (default 2**2bit = 4 valores)
        "-format '%c' ",      # Comment meta-data property (información)
        "histogram:info:-",   # Información de histograma
    )
    histdata = re.compile('\s*(\d+):.*?#([0-9a-f]{6}).*', re.IGNORECASE)

    def __init__(self, getcfg):
        self.archivo = getcfg('fingerprint')
        self.errores = set()
        self.datos = {}
        self.cache = CacheFingerprint()

    def __repr__(self):
        return '<{}:{}>'.format(self.__class__.__name__, id(self))

    #@Logging
    def set(self, size, hdepth):
        self.size = int(size)
        self.hdepth = int(hdepth)
        self.startup()

    #@Logging
    def startup(self):
        self.fpsize = int(self.size / 10)
        f = lambda s: (int(i) for i in s)
        fp = ''.join(compress(self.cmdset, f('11111111100000')))
        hg = ''.join(compress(self.cmdset, f('11011100010111')))
        hc = ''.join(compress(self.cmdset, f('11011100001111')))
        self.fp_cmd = fp.format(self.size, self.fpsize)
        self.hg_cmd = hg.format(self.size, self.hdepth)
        self.hc_cmd = hc.format(self.size, self.hdepth)
        n = 2 ** self.hdepth - 1
        incrementos = [0] + [255 / n] * n
        saturacion = tuple(map(round, accumulate(incrementos)))
        colores = product(saturacion, saturacion, saturacion)
        h = '{:02x}{:02x}{:02x}'
        # self.h*_key = {'color_hexadecimal': posicion_en_el_histograma}
        self.hg_key = {h.format(x, x, x): i for i, x in enumerate(saturacion)}
        self.hc_key = {h.format(x, y, z): i for i, (x, y, z) in enumerate(colores)}

    #@Logging
    def popen(self, cmd):
        out, err = Popen(cmd, stdout=PIPE, stderr=PIPE, shell=True).communicate()
        if err:
            raise OSError(err.decode('utf8', 'replace'))
        return out

    #@Logging
    def gen_fp(self, dirección):
        "Huella (fingerprint) del archivo"
        return self.popen(self.fp_cmd.format(dirección))

    #@Logging
    def fp_to_bits(self, fp):
        "fp: bytes -> bits"
        # reversed: "least-significant-byte first order"
        return ''.join(''.join(reversed('{:08b}'.format(i))) for i in fp)

    #@Logging
    def bits_to_fp(self, bits):
        "fp: bits -> bytes"
        # reversed: "least-significant-byte first order"
        nbytes = int(self.fpsize ** 2 / 8)
        ints = []
        for offset in range(nbytes):
            i = offset * 8
            f = i + 8
            b = ''.join(reversed(bits[i:f]))
            ints.append(int(b, 2))
        return bytes(ints)

    #@Logging
    def bits_rot90(self, bits):
        # bits     rot90
        # -----    -----
        # 1 2 3    3 6 9
        # 4 5 6 => 2 5 8
        # 7 8 9    1 4 7
        columnas = reversed(range(self.fpsize))
        return ''.join(''.join(bits[i::self.fpsize]) for i in columnas)

    #@Logging
    def gen_fps(self, dirección):
        "Calcula el fp para la imagen rotada a 0, 90, 180 y 270 grados"
        # reversed: rota 180 grados
        fp_0 = self.gen_fp(dirección)
        bits_0 = self.fp_to_bits(fp_0)
        bits_90 = self.bits_rot90(bits_0)
        fp_180 = self.bits_to_fp(''.join(reversed(bits_0)))
        fp_270 = self.bits_to_fp(''.join(reversed(bits_90)))
        return (fp_0, self.bits_to_fp(bits_90), fp_180, fp_270)

    #@Logging
    def diff_fp(self, fp1, fp2):
        "Diferencia entre huellas"
        npixel = self.fpsize ** 2
        fp1_xor_fp2 = ''.join(bin(i ^ j) for i, j in zip(fp1, fp2))
        return fp1_xor_fp2.count('1') / npixel

    #@Logging
    def gen_hist(self, dirección, color=True):
        "Histograma simplificado del archivo"
        if color:
            cmd = self.hc_cmd
            keys = self.hc_key
        else:
            cmd = self.hg_cmd
            keys = self.hg_key
        info = self.popen(cmd.format(dirección))
        lines = info.decode().strip().split('\n')
        hist = [0] * len(keys)
        for line in lines:
            vals = self.histdata.match(line)
            if vals:
                count, key = vals.groups()
                hist[keys[key.lower()]] = int(count)
        return hist

    #@Logging
    def diff_hist(self, hist1, hist2):
        "Diferencia entre histograma"
        npixel = self.size ** 2
        return sum(abs(i - j) for i, j in zip(hist1, hist2)) / npixel

    #@Logging
    def get_sign(self, archivo):
        "Devuelve una cadena del archivo: 'bytes:segundos.milesimas'"
        return '{}:{:.3f}'.format(archivo.tamaño, archivo.mtepoch)

    #@Logging(timer=True)
    def get(self, archivo, getfp, rot, gethist, color):

        response = self.cache.get(archivo)
        if response:
            return response

        if archivo not in self.errores:
            sign = self.get_sign(archivo)
            ckey = archivo.carpeta.dirección
            akey = archivo.nombre
            try:
                datos = self.datos[self.size][ckey][akey]
                if sign != datos['sign']:
                    datos.clear()
                    datos['sign'] = sign
            except:
                sizedata = self.datos.setdefault(self.size, {})
                datos = sizedata.setdefault(ckey, {}).setdefault(akey, {})
                datos['sign'] = sign

            errores = False
            if getfp:
                try:
                    fps = datos['fps']
                except:
                    try:
                        fps = self.gen_fps(archivo.dirección)
                    except:
                        fps = []
                        errores = True
                        var = (archivo.dirección, sys.exc_info()[1])
                        log.debug('Fingerprint %s: %s', *var)
                        self.errores.add(archivo)
                    else:
                        datos['fps'] = fps
                fps = fps if rot else fps[0:1]
            else:
                fps = []
            if gethist:
                hclase = 'color' if color else 'gris'
                try:
                    hist = datos['hist'][self.hdepth][hclase]
                except:
                    try:
                        hist = self.gen_hist(archivo.dirección, color)
                    except:
                        hist = None
                        errores = True
                        var = (archivo.dirección, sys.exc_info()[1])
                        log.debug('Histogram %s: %s', *var)
                        self.errores.add(archivo)
                    else:
                        hd = datos.setdefault('hist', {}).setdefault(self.hdepth, {})
                        hd[hclase] = hist
            else:
                hist = None

            response = (fps, hist, errores)
            if not errores:
                self.cache.set(archivo, response)
            return response
        else:
            return ([], None, True)

    #@Logging
    def software_version(self):
        head = self.popen('identify --version').split(b'\n')[0]
        try:
            ver = re.search(b'(\d+\.\d+\.\d+)', head).group(1)
        except:
            ver = b'unknown'
        return ver

    #@Logging
    def cargar(self):
        try:
            log.info('Cargando fingerprint')
            with open(self.archivo.path, mode='rb') as fichero:
                zdatos, crc32, version = pickle.load(fichero)
            ver = version == self.software_version()
            crc = crc32 == zlib.crc32(zdatos)
            if ver and crc:
                datos = zlib.decompress(zdatos)
                self.datos = pickle.loads(datos)
            else:
                problema = 'CRC32' if crc else 'Versión de ImageMagick'
                var = (problema, self.archivo)
                log.warning('%s inválido para %s (será sobrescrito)', *var)
                self.datos.clear()
        except:
            exctype, value = sys.exc_info()[:2]
            log.debug('ERROR en fingerprint. %s: %s', exctype.__name__, value)
            self.datos.clear()

    #@Logging
    def guardar(self):
        try:
            log.info('Guardando fingerprint')
            datos = pickle.dumps(self.datos)
            zdatos = zlib.compress(datos)
            crc32 = zlib.crc32(zdatos)
            ver = self.software_version()
            tamaño = unidad_a_multiplo(len(datos))
            ratio = len(zdatos) / len(datos)
            log.debug('Datos de fingerprint %s (compresión %.3f)', tamaño, ratio)
            with open(self.archivo.path, mode='wb') as fichero:
                pickle.dump((zdatos, crc32, ver), fichero)
        except:
            log.exception('Archivo %s', self.archivo)

    #@Logging
    def unset(self, archivo):
        try:
            ckey = archivo.carpeta.dirección
            akey = archivo.nombre
            self.datos[ckey].pop(akey)
            if not self.datos[ckey]:
                self.datos.pop(ckey)
        except:
            pass
        log.debug('Descartando fingerprint de %', archivo.dirección)

    #@Logging
    def reset(self):
        self.datos.clear()
        self.errores.clear()



class Direcciones:
    "Almacena la lista de carpetas, la lista negra y la ultima carpeta"

    def __init__(self, getcfg):
        self.archivo = getcfg('historial')
        self.carpetas = []
        self.cultima = ''
        self.lisneg = set()
        self.cargar()

    #@Logging
    def add_carpeta(self, nombre, dirección):
        self.carpetas.append([nombre, dirección])
        self.cultima = dirección

    #@Logging
    def remove_carpeta(self, dirección=None, index=None):
        if index is None:
            for index, (_nombre, _dirección) in enumerate(self.carpetas):
                if dirección == _dirección:
                    break
            else:
                raise KeyError('{} no está presente'.format(dirección))
        self.carpetas.pop(index)

    #@Logging
    def add_lisneg(self, dirección):
        self.lisneg.add(dirección)

    #@Logging
    def remove_lisneg(self, dirección):
        self.lisneg.remove(dirección)

    #@Logging
    def reset(self):
        self.carpetas = []
        self.cultima = ''
        self.lisneg.clear()

    #@Logging
    def cargar(self):
        try:
            log.info('Cargando historial')
            with open(self.archivo.path, mode='rb') as fichero:
                zdatos, crc32 = pickle.load(fichero)
            if crc32 == zlib.crc32(zdatos):
                datos = zlib.decompress(zdatos)
                self.carpetas, self.cultima, lisneg = pickle.loads(datos)
                self.lisneg = set(lisneg)
            else:
                log.warning('CRC32 inválido para %s (será sobrescrito)', self.archivo)
        except:
            exctype, value = sys.exc_info()[:2]
            log.debug('ERROR en historial. %s: %s', exctype.__name__, value)

    #@Logging
    def guardar(self):
        try:
            self.lisneg = list(set(self.lisneg))  # elimina repetidos
            log.info('Guardando historial')
            datos = pickle.dumps((self.carpetas, self.cultima, self.lisneg))
            zdatos = zlib.compress(datos)
            crc32 = zlib.crc32(zdatos)
            tamaño = unidad_a_multiplo(len(datos))
            ratio = len(zdatos) / len(datos)
            log.debug('Datos de historial %s (compresión %.3f)', tamaño, ratio)
            with open(self.archivo.path, mode='wb') as fichero:
                pickle.dump((zdatos, crc32), fichero)
        except:
            log.exception('Archivo %s', self.archivo)



class Configuración:

    def __init__(self, version, getcfg, widgets):
        self.version = version
        self.archivo = getcfg('configuracion')
        self.widgets = widgets
        self.reset()
        self.cargar()

    #@Logging
    def reset(self):
        self.valores = {
            'carpetas_tamaño_cero':         True,
            'carpetas_activar_panel':       True,
            'carpetas_panel_ancho':         150,    # pixel
            'carpetas_watchdog':            1000,   # milisegundos

            'imagen': {
                'exts':                     ['jpg', 'jpeg', 'png', 'gif', 'tiff', 'bmp'],
                'tmin':                     0,      # bytes
                'tmax':                     6e6,    # bytes
                'extpc':                    False,  # extensión por contenido
            },
            'imagen_watchdog':              1000,   # milisegundos
            'imagen_borseg':                0,      # 0:Normal, 1~12:Seguro
            'imagen_calidad':               2,      # 0:Nearest, 1:Tiles, 2:Bilinear, 3:Hyper
            'imagen_zoom_min':              60,     # pixel
            'imagen_zoom_max':              6000,   # pixel
            'imagen_zoom_factor':           1.15,
            'imagen_mostrar_tamaño':        True,
            'imagen_mostrar_fecha':         True,
            'imagen_formato_fecha':         "%d/%m/%Y   %H:%M:%S",
            'imagen_scrolled_correccion':   True,
            'imagen_aplicar_orientación':   False,

            'avances_multiples':            10,

            'ventana_tamaño':               (900, 500),
            'ventana_posición':             (-1,  -1),
            'ventana_maximized':            False,
            'ventana_fullscreen':           False,
            'ventana_decoración':           True,
            'ventana_usar_convert':         True,
            'ventana_iniciar_estado':       0,      # 0:Recordar, 1:Maximizado, 2:Fullscreen
            'ventana_salir_preguntar':      1,      # 0:Nunca, 1:Si hay cambios, 2:Siempre
            'ventana_cargar_guardar_frm':   True,
            'ventana_cargar_guardar_fph':   True,
            'ventana_cargar_guardar_keymap':True,

            'fingerprint_mostrar_tamaño':   True,
            'fingerprint_imagen_ancho':     128,
            'fingerprint_imagen_alto':      64,
            'fingerprint_size':             160,
            'fingerprint_hdepth':           2,

            'banderas_multiples':           True,
            'banderas_boton_ancho':         100,
           #'banderas_color_grupos':        Gdk.RGBA(0.00, 0.60, 1.00),
           #'banderas_color_borrar':        Gdk.RGBA(0.90, 0.20, 0.20),
           #'banderas_color_unset':         Gdk.RGBA(0.18, 0.18, 0.18),
           #'banderas_color_set':           Gdk.RGBA(0.90, 0.90, 0.90),
            'banderas_color_grupos':        Gdk.RGBA(0.00, 0.00, 1.00),
            'banderas_color_borrar':        Gdk.RGBA(1.00, 0.00, 0.00),
            'banderas_color_unset':         Gdk.RGBA(0.82, 0.82, 0.82),
            'banderas_color_set':           Gdk.RGBA(0.08, 0.08, 0.08),
        }
        self.software = {}
        self.temporal = {}
        self.set_software()

    #@Logging
    def set_software(self):
        try:
            for software in ('gimp', 'mat', 'shred', 'srm', 'convert', 'identify'):
                instalado = os.system('which {}'.format(software)) == 0
                self.software[software] = instalado
        except:
            log.exception('Comprobando software disponible')
            self.software = {}

    #@Logging
    def set_colores(self, resultado):
        for key in self.valores:
            if 'color' in key:
                self.valores[key] = color(self.valores[key], resultado)

    #@Logging
    def cargar(self):
        log.info('Cargando configuración')
        try:
            imagen = self.valores['imagen']
            with open(self.archivo.path, mode='r', encoding='utf-8') as fichero:
                datos = json.load(fichero)
            if self.version < datos.get('version', ''):
                log.warning('La configuración corresponde a una versión posterior')
            self.valores.update(datos)
            imagen.update(datos.get('imagen', {}))
            self.valores['imagen'] = imagen  # por si faltan valores
            self.set_colores('RGBA')
        except:
            exctype, value = sys.exc_info()[:2]
            log.debug('ERROR en configuración: %s: %s', exctype.__name__, value)
            self.reset()

    #@Logging
    def guardar(self):
        log.info('Guardando configuración')
        self.set_colores('html')
        self.valores['version'] = self.version
        if not (self.valores['ventana_maximized'] or self.valores['ventana_fullscreen']):
            self.valores['ventana_tamaño'] = self.widgets['window'].get_size()
            self.valores['ventana_posición'] = self.widgets['window'].get_position()
        with open(self.archivo.path, mode='w', encoding='utf-8') as fichero:
            json.dump(self.valores, fichero, ensure_ascii=False, indent=4, sort_keys=True)

    #@Logging
    def pop(self, key):
        if key in self.valores:
            return self.valores.pop(key)
        elif key in self.software:
            return self.software.pop(key)
        elif key in self.temporal:
            return self.temporal.pop(key)
        else:
            raise KeyError(key)

    #@Logging
    def __getitem__(self, keys):
        if isinstance(keys, tuple):
            uno = False
        else:
            uno = True
            keys = (keys,)
        valores = []
        for key in keys:
            if key in self.valores:
                valores.append(self.valores[key])
            elif key in self.software:
                valores.append(self.software[key])
            elif key in self.temporal:
                valores.append(self.temporal[key])
            else:
                raise KeyError(key)
        return valores[0] if uno else valores

    #@Logging
    def __setitem__(self, key, value):
        if key in self.valores:
            self.valores[key] = value
        elif key in self.software:
            self.software[key] = value
        else:
            self.temporal[key] = value

    #@Logging
    def get(self, key, default=None):
        try:
            return self[key]
        except KeyError:
            return default

    @property
    def borseg(self):
        return self['imagen_borseg', 'shred', 'srm']

