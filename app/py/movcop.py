#!/usr/bin/env python3
#-*- coding: utf-8 -*-

# Copyright (C) 2013 Cristian Hernán Schmidt
#
# This file is part of SCHVisor.
#
# SCHVisor is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# SCHVisor is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with SCHVisor. If not, see <http://www.gnu.org/licenses/>.

import os
import re
import logging

from datetime import datetime
from gi.repository import Gtk, Gdk

from debug import Log
from utils import normalizar, try_except, List, Dict

PATH, NAME = os.path.split(os.path.abspath(__file__))
BASE, EXT = os.path.splitext(NAME)
GUI = os.path.join(PATH, '..', 'ui', '{}.ui'.format(BASE))
CSS = os.path.join(PATH, '..', 'css', '{}.css'.format(BASE))

PATRONES = re.compile(r'\{(?:(BASE)|(\d+)(?::(\d+))?|(MD5)|(EXT)|(.+?))\}', re.IGNORECASE)
ICON_POS_SEC = Gtk.EntryIconPosition.SECONDARY

AYUDA = ('{{«num1»[:«num2»]}}: se reemplaza por un número de num1 dígitos, '
'empezando a contar desde num2 o desde 1 (uno).\n'
'Ejemplo "{{3:2}}" representa 002, 003, 004, 005, etc.\n\n'
'{{MD5}}: se reemplaza por la suma de verificación MD5 del archivo.\n\n'
'{{«formato de fecha»}}: se reemplaza por la fecha de ultima modificación del '
'archivo (los formatos de datetime), siendo: %Y=año (4 digitos), %y=año '
'(2 digitos), %m=mes, %d=día, %H=hora, %M=minuto, %S=segundo, etc.\n'
'Ejemplo "{{%Y-%m-%d %H:%M:%S}}" representa {:%Y-%m-%d %H:%M:%S}.\n\n'
'{{EXT}}: se reemplaza por la extensión de la imagen (con punto).\n\n'
'{{BASE}}: se reemplaza por el nombre actual (sin extensión).\n'
'Ejemplo, considerando un archivo llamado "test.jpg", "{{BASE}}{{EXT}}" '
'representa "test.jpg".\n\n'
'NOTA: Si el patrón no contiene al menos un {{num}}, {{MD5}} o {{fecha}} y se '
'esta renombrando mas de una imagen (en el grupo) no será aceptado. '
'No se distinguen mayúsculas en las palabras claves mencionadas aquí, '
'excepto en los formatos de datetime.')



def _format(texto):
    return texto.replace('{', '{{').replace('}', '}}')



class MovCop:

    log = Log(logging.getLogger(__name__), sub=1)

    def __init__(self, parent, titulo, carpetas, mba=None, banderas=None, desmarcar=None):
        self.parent = parent
        self.builder = Gtk.Builder()
        self.builder.add_from_file(GUI)
        self.builder.connect_signals(self)
        objects = (
            'dialog',
            'box',
            'label_titulo',
            'button_aceptar',
            'buttonbox_atajo',
            'checkbutton_setext',
            'checkbutton_igrupo',
            'checkbutton_desmarcar',
            'radiobutton_preguntar',
            'radiobutton_ignorar',
            'radiobutton_sobrescribir',
        )
        self.widgets = {obj: self.builder.get_object(obj) for obj in objects}

        if os.path.isfile(CSS):
            screen = Gdk.Screen.get_default()
            provider = Gtk.CssProvider()
            provider.load_from_path(CSS)
            priority = Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION
            context = Gtk.StyleContext()
            context.add_provider_for_screen(screen, provider, priority)

        if isinstance(banderas, dict):
            self.banderas = True
            if len(banderas) > 1:
                self.widgets['checkbutton_igrupo'].show()
                self.widgets['checkbutton_desmarcar'].show()
                self.widgets['buttonbox_atajo'].show()
        else:
            banderas = {1: banderas or 1}
            self.banderas = False
            if desmarcar is None:
                plural = 'imágenes' if 'imagenes' in normalizar(titulo) else 'imagen'
                # mba = multiples banderas por archivo permitidas
                plural = ('s', 'ones', plural) if mba else ('', 'ón', plural)  
                label = 'Borrar la{0} marcaci{1} de la{0} {2}'.format(*plural)
                self.widgets['checkbutton_desmarcar'].set_label(label)
                self.widgets['checkbutton_desmarcar'].show()
            elif desmarcar:
                if isinstance(desmarcar, (tuple, list)):
                    label, tooltip = desmarcar
                else:
                    label, tooltip = desmarcar, ''
                self.widgets['checkbutton_desmarcar'].set_label(label)
                self.widgets['checkbutton_desmarcar'].set_tooltip_text(tooltip)
                self.widgets['checkbutton_desmarcar'].show()

        self.liststore = Gtk.ListStore(str, str)  # nombre, dirección
        self.imágenes = banderas
        self.acciones = {False:'mover', True:'copiar'}
        self.entry = {}
        self.combobox = {}
        self.checkbutton = {}
        self.togglebutton = {}

        vbox = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
        self.widgets['box'].pack_start(vbox, expand=True, fill=True, padding=0)

        for b in sorted(banderas):
            n = banderas[b]
            tooltip = 'Con {} im{}'.format(n, 'agen' if n == 1 else 'ágenes')
            
            box = Gtk.Box()
            box.set_spacing(10)
            if self.banderas or n > 1:
                box.set_tooltip_text(tooltip)
            vbox.pack_start(box, expand=False, fill=True, padding=0)

            if self.banderas:
                label = Gtk.Label('Grupo {}'.format(b))
                box.pack_start(label, expand=False, fill=True, padding=0)

            self.combobox[b] = Gtk.ComboBox.new_with_model(self.liststore)
            self.combobox[b].set_tooltip_text('Elija la carpeta destino')
            self.combobox[b].connect('changed', self.on_combobox_changed, b)
            renderer = Gtk.CellRendererText()
            self.combobox[b].pack_start(renderer, True)
            self.combobox[b].add_attribute(renderer, "text", 0)
            box.pack_start(self.combobox[b], expand=True, fill=True, padding=0)

            self.entry[b] = Gtk.Entry()
            self.entry[b].set_icon_from_stock(ICON_POS_SEC, 'gtk-info')
            self.entry[b].set_icon_tooltip_text(ICON_POS_SEC, 'No se cambiará el nombre')
            self.entry[b].set_placeholder_text('Escriba el patrón para el nombre')
            self.entry[b].connect('icon-press', self.on_entry_icon_press, b)
            self.entry[b].connect('focus-out-event', self.on_entry_focus_out_event, b)
            box.pack_start(self.entry[b], expand=True, fill=True, padding=0)

            self.checkbutton[b] = Gtk.CheckButton('MD5')
            self.checkbutton[b].set_tooltip_text('Usar la suma de verificación MD5')
            #self.checkbutton[b].set_property("draw_indicator", False)
            self.checkbutton[b].set_use_action_appearance(False)
            self.checkbutton[b].connect('toggled', self.on_checkbutton_toggled, b)
            box.pack_start(self.checkbutton[b], expand=False, fill=True, padding=0)

            self.togglebutton[b] = Gtk.ToggleButton('Mover')
            self.togglebutton[b].set_size_request(58, -1)
            self.togglebutton[b].set_relief(Gtk.ReliefStyle.NONE)
            self.togglebutton[b].set_use_action_appearance(False)
            self.togglebutton[b].connect('toggled', self.on_togglebutton_toggled, b)
            box.pack_start(self.togglebutton[b], expand=False, fill=True, padding=0)

        self.otra_carpeta = 'Elejir otra...'

        for nombre, dirección in carpetas:
            self.liststore.append([nombre, dirección])
        self.liststore.append([self.otra_carpeta, ''])

        self.carpetas = Dict()
        self.nombres = Dict()
        self.offsets = Dict()
        self.errores = List()
        self.cache = Dict()

        self.widgets['label_titulo'].set_label(titulo)
        self.widgets['dialog'].set_transient_for(self.parent)
        self.widgets['dialog'].show_all()


    @try_except
    def on_combobox_changed(self, widget, b):
        iterdor = widget.get_active_iter()
        if iterdor != None:
            modelo = widget.get_model()
            dirección = modelo[iterdor][1]
            if dirección:
                self.carpetas[b] = dirección
            else:
                from dialogs import v_pregunta_carpeta
                título = 'Elija una carpeta'
                dirección = v_pregunta_carpeta(self.widgets['dialog'], título)
                if dirección:
                    self.carpetas[b] = dirección
                    modelo[iterdor][0] = '«{}»'.format(os.path.basename(dirección))
                    modelo[iterdor][1] = dirección
                    self.liststore.append([self.otra_carpeta, ''])
                else:
                    self.carpetas.remove(b)
                    widget.set_active(-1)


    def validar(self, widget, b):
        patron = widget.get_text()
        if patron:
            secuenciable = False
            offsets = []
            nombre = ''
            pos = 0
            for item in PATRONES.finditer(patron):
                base, num, offset, md5, ext, fecha = item.groups()
                nombre += _format(patron[pos:item.start()])
                pos = item.end()
                try:
                    if base:
                        nombre += '{BASE}'
                        secuenciable = True
                    elif num:
                        nombre += '{{:0>{}}}'.format(num)
                        offsets.append(int(offset or 1))
                        secuenciable = True
                    elif md5:
                        nombre += '{MD5}'
                        secuenciable = True
                    elif ext:
                        nombre += '{EXT}'
                    else:
                        prueba = '{:{f}}'.format(datetime.now(), f=fecha)
                        if prueba != fecha:
                            nombre += '{{FECHA:{}}}'.format(fecha)
                            secuenciable = True
                        else:
                            nombre += _format(patron[item.start():pos])
                except:
                    no_procesado = patron[item.start():pos]
                    self.log.exception('Patrón con error %s', no_procesado)
                    nombre += _format(no_procesado)

            nombre += _format(patron[pos:])

            if (secuenciable or self.imágenes[b] == 1) and not nombre.count('/'):
                self.nombres[b] = nombre
                self.offsets[b] = offsets
                self.errores.remove(b)
                icon = Gtk.STOCK_APPLY
                tooltip = ''
            else:
                self.errores.append(b)
                icon = Gtk.STOCK_DIALOG_ERROR
                tooltip = 'Error en el patrón'
        else:
            self.nombres.remove(b)
            self.offsets.remove(b)
            self.errores.remove(b)
            icon = Gtk.STOCK_INFO
            tooltip = 'No se cambiará el nombre'
        widget.set_icon_from_stock(ICON_POS_SEC, icon)
        widget.set_icon_tooltip_text(ICON_POS_SEC, tooltip)
        self.widgets['button_aceptar'].set_sensitive(False if self.errores else True)


    @try_except
    def on_entry_icon_press(self, widget, icon_position, event, b):
        self.validar(widget, b)


    @try_except
    def on_entry_focus_out_event(self, widget, event, b):
        self.validar(widget, b)


    @try_except
    def on_checkbutton_toggled(self, widget, b):
        entry = self.entry[b]
        if widget.get_active():
            self.cache[b] = (
                self.nombres.get(b),
                self.offsets.get(b),
                self.errores.pull(b),
                entry.get_icon_stock(ICON_POS_SEC),
                entry.get_icon_tooltip_text(ICON_POS_SEC),
            )
            nombre = '{MD5}{ext}'
            icon = Gtk.STOCK_APPLY
            tooltip = ''
            sensitive = False
            self.nombres[b] = nombre
            self.offsets[b] = []
        else:
            nombre, offsets, error, icon, tooltip = self.cache.pop(b)
            sensitive = True
            if nombre:
                self.nombres[b] = nombre
            else:
                del self.nombres[b]
            if error:
                self.errores.append(b)
        entry.set_text(nombre or '')
        entry.set_sensitive(sensitive)
        entry.set_icon_from_stock(ICON_POS_SEC, icon)
        entry.set_icon_tooltip_text(ICON_POS_SEC, tooltip)
        self.validar(entry, b)


    @try_except
    def on_togglebutton_toggled(self, widget, b):
        widget.set_label(self.acciones[widget.get_active()].capitalize())


    @try_except
    def on_button_mover_todos_clicked(self, widget):
        for w in self.togglebutton.values():
            w.set_active(False)


    @try_except
    def on_button_copiar_todos_clicked(self, widget):
        for w in self.togglebutton.values():
            w.set_active(True)


    @property
    def política(self):
        if self.widgets['radiobutton_preguntar'].get_active():
            return 'preguntar'
        elif self.widgets['radiobutton_ignorar'].get_active():
            return 'ignorar'
        else:
            return 'sobrescribir'


    @try_except
    def on_button_ayuda_clicked(self, widget):
        mensaje = 'Patrones de renombrado:'
        aclaración = AYUDA.format(datetime.now())
        from dialogs import v_información
        v_información(self.widgets['dialog'], mensaje, aclaración)


    def get_datos(self):
        setext = self.widgets['checkbutton_setext'].get_active()
        banderas = list(self.nombres) + list(self.offsets) + list(self.carpetas)
        datos = {}
        for b in set(banderas):
            n = self.nombres.get(b) or ''
            if setext and n and not n.endswith('{EXT}'):
                n += '{EXT}'
            o = self.offsets.get(b) or []
            c = self.carpetas.get(b) or None
            a = self.acciones[self.togglebutton[b].get_active()]
            if n or c:
                datos[b] = {'nombre': n, 'offsets': o, 'carpeta': c, 'acción': a}
        if not self.banderas:
            datos = datos.get(1, {})
        return datos


    def ejecutar(self):
        response = self.widgets['dialog'].run()
        self.widgets['dialog'].hide_on_delete()
        if response == Gtk.ResponseType.OK:
            datos = self.get_datos()
            política = self.política
            desmarcar = self.widgets['checkbutton_desmarcar'].get_active()
            igrupo = self.widgets['checkbutton_igrupo'].get_active()
            #       Dict   str       bool       bool
            return (datos, política, desmarcar, igrupo)
        else:
            return (None, None, None, None)

