#!/usr/bin/env python3
#-*- coding: utf-8 -*-

# Copyright (C) 2013 Cristian Hernán Schmidt
#
# This file is part of SCHVisor.
#
# SCHVisor is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# SCHVisor is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with SCHVisor. If not, see <http://www.gnu.org/licenses/>.

import os
import sys
import shutil
import filecmp
import logging
import functools
import collections

from datetime import datetime
from subprocess import Popen, PIPE
from gi.repository import Gtk, Gdk, GdkPixbuf

from model import Carpeta
from debug import Log, Logging
from utils import (FLAGS, WaitTask, try_except, fborrar, frenombrar, md5sum,
    maskkey_to_hotkey, hotkey_to_maskkey, unidad_a_multiplo, normalizar)

PATH, NAME = os.path.split(os.path.abspath(__file__))
BASE, EXT = os.path.splitext(NAME)
GUI = os.path.join(PATH, '..', 'ui', '{}.ui'.format(BASE))
CSS = os.path.join(PATH, '..', 'css', '{}.css'.format(BASE))

log = Log(logging.getLogger(__name__), sub=1)

FANCHO = 0.80
FALTO = 0.70



class ListaNegra(WaitTask):

    log = log
    wait_task = WaitTask.wait_task  # decorador

    def __init__(self, parent, lisneg, carpetas, borseg=None):
        self.parent = parent
        self.builder = Gtk.Builder()
        self.builder.add_from_file(GUI)
        self.builder.connect_signals(self)
        objects = (
            'dialog',
            'label_titulo',
            'scrolledwindow',
            'button_accion_1',
            'button_accion_2',
            'button_accion_3',
            'button_salir_accion',
        )
        self.widgets = {obj: self.builder.get_object(obj) for obj in objects}

        if os.path.isfile(CSS):
            screen = Gdk.Screen.get_default()
            provider = Gtk.CssProvider()
            provider.load_from_path(CSS)
            priority = Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION
            context = Gtk.StyleContext()
            context.add_provider_for_screen(screen, provider, priority)

        self.widgets['treeview'] = Gtk.TreeView()
        self.widgets['treeview'].set_rubber_banding(True)
        self.widgets['treeview'].set_headers_clickable(False)
        selection = self.widgets['treeview'].get_selection()
        selection.set_mode(Gtk.SelectionMode.MULTIPLE)
        self.widgets['treeview_selection'] = selection
        self.widgets['scrolledwindow'].add(self.widgets['treeview'])

        store = Gtk.ListStore(str, bool, str)  # dirección, inexistente, comentarios
        self.widgets['liststore'] = store
        self.widgets['treeview'].set_model(store)
        texto = Gtk.CellRendererText()
        dirección = Gtk.TreeViewColumn("Dirección", texto, text=0, strikethrough=1)
        self.widgets['treeview'].append_column(dirección)
        self.widgets['treeview'].set_headers_visible(False)
        self.widgets['treeview'].set_search_column(0)
        self.widgets['treeview'].set_tooltip_column(2)

        self.widgets['label_titulo'].set_text('Lista Negra')
        self.widgets['button_accion_1'].set_label('Eliminar')
        self.widgets['button_accion_2'].set_label('M/C/R')
        self.widgets['button_accion_3'].set_label('Borrar')
        tooltip1 = 'Eliminar archivos seleccionados de la lista negra'
        tooltip2 = 'Mover/copiar/renombrar del disco los archivos seleccionados'
        tooltip3 = 'Borrar del disco los archivos seleccionados'
        self.widgets['button_accion_1'].set_tooltip_text(tooltip1)
        self.widgets['button_accion_2'].set_tooltip_text(tooltip2)
        self.widgets['button_accion_3'].set_tooltip_text(tooltip3)
        self.widgets['button_salir_accion'].set_label('Salir y Actualizar')

        self.lisneg = lisneg
        self.borseg = borseg
        self.carpetas = tuple(carpetas)  # porque es un generador
        self.movcop_destino = None

        self.set_lista_negra()

        if parent:
            ancho = parent.get_allocated_width()
            alto = parent.get_allocated_height()
            size = round(FANCHO * ancho, -1), round(FALTO * alto, -1)
            self.widgets['dialog'].set_default_size(*size)
            self.widgets['dialog'].set_transient_for(parent)
        self.widgets['dialog'].show_all()


    @try_except
    def on_button_todo_clicked(self, widget):
        self.widgets['treeview_selection'].select_all()


    @try_except
    def on_button_nada_clicked(self, widget):
        self.widgets['treeview_selection'].unselect_all()


    def treeview_get_selected(self, existe=None):
        modelo, paths = self.widgets['treeview_selection'].get_selected_rows()
        iteradores = [modelo.get_iter(p) for p in paths]
        if existe:
            iteradores = [i for i in iteradores if os.path.exists(modelo[i][0])]
        return (modelo, iteradores, len(iteradores))


    def set_lista_negra(self):
        self.widgets['liststore'].clear()
        for archivo in sorted(self.lisneg):
            inexistente = not os.path.exists(archivo)
            if inexistente:
                comentarios = 'BORRADO'
            else:
                tamaño = unidad_a_multiplo(os.path.getsize(archivo))
                comentarios = 'Tamaño: {}'.format(tamaño)
            self.widgets['liststore'].append([archivo, inexistente, comentarios])


    @try_except
    def on_button_accion_1_clicked(self, widget):  # Eliminar
        modelo, iteradores, n = self.treeview_get_selected()
        if not n:
            return
        from dialogs import v_pregunta_si_no
        mjs = '¿Desea eliminar de la lista {0} archivo{1} seleccionado{1}?'
        mjs = mjs.format(*(('el', '') if n == 1 else ('los', 's')))
        aclaración = 'Estos cambios serán guardados en las configuraciones.'
        respuesta = v_pregunta_si_no(self.widgets['dialog'], mjs, aclaración)
        if respuesta:
            for iterador in iteradores:
                self.lisneg.remove(modelo[iterador][0])
                modelo.remove(iterador)


    @try_except
    def on_button_accion_2_clicked(self, widget):  # Mover/Copiar/Renombrar
        modelo, iteradores, n = self.treeview_get_selected(existe=True)
        if not n:
            return
        plural = 'la imagen' if n == 1 else 'las imágenes'
        título = 'Elija la carpeta destino y/o el nuevo nombre para {}'.format(plural)
        desmarcar = 'Eliminar de la lista negra {}'.format(plural)
        from movcop import MovCop
        dialog = MovCop(self.widgets['dialog'], título, self.carpetas, desmarcar=desmarcar)
        movcop_var = dialog.ejecutar()  # patron, política, desmarcar, igrupo
        if movcop_var[0]:
            if '{MD5}' in movcop_var[0]['nombre']:
                key, cancel = self.accion_2_md5sum(modelo, iteradores)
                md5 = self.wait_env[key].pop('data')['md5']
                if cancel:
                    return
            else:
                md5 = {}
            key, cancel = self.accion_2(modelo, iteradores, md5, *movcop_var)
            self.wait_env[key].pop('data')
            self.set_lista_negra()


    @wait_task('Calculando suma MD5')
    def accion_2_md5sum(self, key, modelo, iteradores):
        procesar = [modelo[it][0] for it in iteradores]
        self.wait_env[key]['data'] = {'procesar': procesar, 'md5': {}}


    #@Logging
    def accion_2_md5sum_thread(self, key):
        data = self.wait_env[key]['data']
        n = len(data['procesar'])
        for i, dirección in enumerate(data['procesar'], 1):
            data['md5'][dirección] = md5sum(dirección)
            yield i / n


    @wait_task('Moviendo/copiando/renombrando')
    def accion_2(self, key, modelo, iteradores, md5, patron, política, desmarcar, igrupo):
        data = {
            'copiar': {},
            'eliminar': set(),
            'renombrar': {},
        }
        for i, iterador in enumerate(iteradores, 1):
            dirección = modelo[iterador][0]
            if os.path.exists(dirección):
                carpeta, nombre = os.path.split(dirección)
                números = (i + offset for offset in patron['offsets'])
                parámetros = {
                    'BASE': os.path.splitext(nombre)[0],
                    'MD5': md5.get(dirección),
                    'FECHA': datetime.fromtimestamp(os.path.getmtime(dirección)),
                    'EXT': os.path.splitext(nombre)[1].lstrip('.'),
                }
                _nombre = patron['nombre'].format(*números, **parámetros) or nombre
                _carpeta = patron['carpeta'] or carpeta
                _dirección = os.path.join(_carpeta, _nombre)

                if _dirección != dirección:
                    if os.path.exists(_dirección):
                        if política == 'ignorar':
                            continue
                        elif política == 'sobrescribir':
                            data['eliminar'].add(_dirección)
                        else:
                            iguales = filecmp.cmp(dirección, _dirección)

                            if self.movcop_destino:
                                _carpeta = self.movcop_destino
                                _dirección = os.path.join(_carpeta, _nombre)
                                if os.path.exists(_dirección):
                                    preguntar = True
                                else:
                                    preguntar = False
                            else:
                                preguntar = True

                            if preguntar:
                                var = (self.widgets['dialog'], _carpeta, _nombre, 
                                    iguales, self.movcop_destino)
                                from dialogs import v_pregunta_archivo
                                _nombre, _carpeta, mismo = v_pregunta_archivo(*var)

                                if not (_nombre or _carpeta):
                                    continue

                                _dirección = os.path.join(_carpeta, _nombre)
                                self.movcop_destino = _carpeta if mismo else None

                    if patron['acción'] == 'mover':
                        data['renombrar'][dirección] = _dirección
                        self.lisneg.remove(dirección)
                        self.lisneg.add(_dirección)
                        modelo[iterador][0] = _dirección
                        dirección = _dirección

                    elif patron['acción'] == 'copiar':
                        data['copiar'][dirección] = _dirección

                    else:
                        raise ValueError('acción erronea "{}"'.format(patron['acción']))
                else:
                    continue
                if desmarcar:
                    self.lisneg.remove(dirección)
                    modelo.remove(iterador)

        if data['eliminar'] or data['copiar'] or data['renombrar']:
            self.wait_env[key]['data'] = data
        else:
            return True  # stop


    def accion_2_thread(self, key):
        data = self.wait_env[key]['data']
        n = len(data['eliminar']) + len(data['copiar']) + len(data['renombrar'])
        i = 0

        for i, d in enumerate(data['eliminar'], i + 1):
            método, comando = fborrar(d, self.borseg)
            self.log.info('Se eliminó [%s%s]: %s', método, comando, d)
            yield i / n

        for i, d in enumerate(data['copiar'], i + 1):
            _d = data['copiar'][d]
            shutil.copy2(d, _d)
            self.log.info('Se copio: %s -> %s', d, _d)
            yield i / n

        for i, d in enumerate(data['renombrar'], i + 1):
            _d = data['renombrar'][d]
            método, comando = frenombrar(d, _d, self.borseg)
            self.log.info('Se renombró [%s%s]: %s -> %s', método, comando, d, _d)
            yield i / n


    @try_except
    def on_button_accion_3_clicked(self, widget):  # Borrar
        modelo, iteradores, n = self.treeview_get_selected()
        if not n:
            return
        from dialogs import v_pregunta_si_no
        mjs = '¿Desea eliminar del disco y de la lista negra {0} archivo{1} seleccionado{1}?'
        mjs = mjs.format(*(('el', '') if n == 1 else ('los', 's')))
        aclaración = 'Los cambios en la lista negra serán guardados en las configuraciones.'
        respuesta = v_pregunta_si_no(self.widgets['dialog'], mjs, aclaración)
        if respuesta:
            key, cancel = self.accion_3(modelo, iteradores, n)
            self.wait_env[key].pop('data')
            self.set_lista_negra()


    @wait_task('Borrando archivos')
    def accion_3(self, key, modelo, iteradores, n):
        self.wait_env[key]['data'] = {
            'n': n,
            'modelo': modelo,
            'iteradores': iteradores,
        }


    def accion_3_thread(self, key):
        data = self.wait_env[key]['data']
        for i, iterador in enumerate(data['iteradores'], 1):
            dirección = data['modelo'][iterador][0]
            if os.path.exists(dirección):
                método, comando = fborrar(dirección, self.borseg)
                self.log.info('Se eliminó [%s%s]: %s', método, comando, dirección)
            self.lisneg.remove(dirección)
            data['modelo'].remove(iterador)
            yield i / data['n']


    def ejecutar(self):
        response = self.widgets['dialog'].run()
        self.widgets['dialog'].hide_on_delete()
        actulizar_carpetas = bool(response == 2)  # Salir y actualizar carpetas
        # La lista negra es un diccionario (mutable) ya está actualizado
        return actulizar_carpetas



class ListaModificados(WaitTask):

    log = log
    wait_task = WaitTask.wait_task  # decorador

    def __init__(self, parent, carpetas, base, convert, borseg):
        self.accion_3_destino = None
        
        self.parent = parent
        self.builder = Gtk.Builder()
        self.builder.add_from_file(GUI)
        self.builder.connect_signals(self)
        objects = (
            'dialog',
            'label_titulo',
            'scrolledwindow',
            'button_accion_1',
            'button_accion_2',
            'button_accion_3',
            'button_salir_accion',
        )
        self.widgets = {obj: self.builder.get_object(obj) for obj in objects}

        self.widgets['treeview'] = Gtk.TreeView()
        self.widgets['treeview'].set_rubber_banding(True)
        self.widgets['treeview'].set_headers_clickable(False)
        selection = self.widgets['treeview'].get_selection()
        selection.set_mode(Gtk.SelectionMode.MULTIPLE)
        self.widgets['treeview_selection'] = selection
        self.widgets['scrolledwindow'].add(self.widgets['treeview'])

        if os.path.isfile(CSS):
            screen = Gdk.Screen.get_default()
            provider = Gtk.CssProvider()
            provider.load_from_path(CSS)
            priority = Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION
            context = Gtk.StyleContext()
            context.add_provider_for_screen(screen, provider, priority)

        self.nuevas_carpetas = set()
        self.convert = convert
        self.borseg = borseg
        self.cfg, self.carpetas, self.formatos, self.banderas = base

        # coltype: ad, cn, an, ancho, alto, giro, rh, rv, formato
        self.coltype = (str, str, str, int, int, int, bool, bool, str)
        store = Gtk.ListStore(*self.coltype)
        self.widgets['liststore'] = store
        self.widgets['treeview'].set_model(store)

        self.otra_carpeta = 'Elejir otra...'
        self.cstore = Gtk.ListStore(str, str)
        for nombre, dirección in carpetas:
            self.cstore.append([nombre, dirección])
        self.cstore.append([self.otra_carpeta, ''])

        if convert:
            fmt = []
            p = Popen('convert -list format'.split(), stdout=PIPE)
            line = p.stdout.readline()
            ext_width = line.find(b'Format') + 6
            ext_write = line.find(b'Mode') + 1
            line = p.stdout.readline()  # linea de guiones "---"
            for line in p.stdout:
                if line == b'\n':
                    break
                ext = line[0:ext_width].decode().strip().lower()
                if ext:
                    if line[ext_write] == ord(b'w'):
                        fmt.append(ext)
        else:
            fmt = []
            for f in GdkPixbuf.Pixbuf.get_formats():
                if f.is_writable():
                    fmt.extend(f.get_extensions())
        self.fstore = Gtk.ListStore(str)
        for f in fmt:
            self.fstore.append([f])

        columnas = (
            # nombre     pos  tipo
            ('Carpeta',   1, 'combo' ),
            ('Nombre',    2, 'text'  ),
            ('Ancho',     3, 'text'  ),
            ('Alto',      4, 'text'  ),
            ('Giro',      5, 'text'  ),
            ('Reflejo H', 6, 'toggle'),
            ('Reflejo V', 7, 'toggle'),
            ('Formato',   8, 'combo' ),
        )
        models = {
            'Carpeta': self.cstore,
            'Formato': self.fstore,
        }
        CRText = Gtk.CellRendererText
        CRCombo = Gtk.CellRendererCombo
        CRToggle = Gtk.CellRendererToggle
        config = {
            # tipo      señal     vínculo   Renderer   propiedades
            'text':   ('edited',  'text',   CRText,   [("editable",    True)]),
            'toggle': ('toggled', 'active', CRToggle, [("activatable", True)]),
            'combo':  ('edited',  'text',   CRCombo,  [("editable",    True),
                                                       ("text-column", 0),
                                                       ("has-entry",   False)]),
        }
        for nombre, pos, tipo in columnas:
            señal, vínculo, Renderer, propiedades = config[tipo]
            renderer = Renderer()
            for n, v in propiedades:
                renderer.set_property(n, v)
            if tipo == 'combo':
                renderer.set_property("model", models[nombre])
            renderer.connect(señal, self.on_column_changed, pos)
            columna = Gtk.TreeViewColumn(nombre, renderer, **{vínculo: pos})
            columna.set_spacing(5)
            if nombre in ('Carpeta', 'Nombre'):
                columna.set_expand(True)
            self.widgets['treeview'].append_column(columna)

        self.widgets['treeview'].set_headers_visible(True)
        self.widgets['treeview'].set_search_column(0)
        self.widgets['treeview'].set_tooltip_column(0)

        self.widgets['label_titulo'].set_text('Imágenes modificadas')
        self.widgets['button_accion_1'].set_label('Marcar')
        self.widgets['button_accion_2'].set_label('Modificar')
        self.widgets['button_accion_3'].set_label('Guardar')
        tooltip1 = 'Marcar las imágenes seleccionadas con un grupo'
        tooltip2 = 'Modificar los formatos de las imágenes seleccionadas'
        tooltip3 = 'Guardar al disco las imágenes seleccionadas'
        self.widgets['button_accion_1'].set_tooltip_text(tooltip1)
        self.widgets['button_accion_2'].set_tooltip_text(tooltip2)
        self.widgets['button_accion_3'].set_tooltip_text(tooltip3)
        self.widgets['button_salir_accion'].set_label('Salir y Guardar')

        self.datos = []
        self.cache = []  # última carpeta válida elegida
        for archivo in sorted(self.formatos.buscar_cambios(), key=normalizar):
            fimagen = self.formatos.get(archivo)
            ancho, alto, giro, rh, rv = fimagen['ancho', 'alto', 'giro', 'rh', 'rv']
            d = (archivo.dirección, archivo.carpeta.nombre, archivo.nombre, 
                ancho or 0, alto or 0, giro or 0, rh, rv, archivo.formato)
            self.widgets['liststore'].append(d)
            self.datos.append(archivo)
            self.cache.append(archivo.carpeta.nombre)

        if parent:
            ancho = parent.get_allocated_width()
            alto = parent.get_allocated_height()
            size = round(FANCHO * ancho, -1), round(FALTO * alto, -1)
            self.widgets['dialog'].set_default_size(*size)
            self.widgets['dialog'].set_transient_for(parent)
        self.widgets['dialog'].show_all()


    @try_except
    def on_button_todo_clicked(self, widget):
        self.widgets['treeview_selection'].select_all()


    @try_except
    def on_button_nada_clicked(self, widget):
        self.widgets['treeview_selection'].unselect_all()


    @try_except
    def on_column_changed(self, widget, index, *args):
        if len(args) == 1:
            valor = not widget.get_active()
            pos = args[0]
        else:
            valor, pos = args
        if pos == 1:
            if valor == self.otra_carpeta:
                from dialogs import v_pregunta_carpeta
                título = 'Elija una carpeta'
                dirección = v_pregunta_carpeta(self.widgets['dialog'], título)
                if dirección:
                    valor = '«{}»'.format(os.path.basename(dirección))
                    i = len(self.cstore) - 1
                    self.cstore[i][0] = valor
                    self.cstore[i][1] = dirección
                    self.cstore.append([self.otra_carpeta, ''])
                else:
                    valor = self.cache[int(index)]
            else:
                self.cache[int(index)] = valor
        self.widgets['liststore'][index][pos] = self.coltype[pos](valor)


    def treeview_get_selected(self):
        modelo, paths = self.widgets['treeview_selection'].get_selected_rows()
        iteradores = [modelo.get_iter(p) for p in paths]
        return (modelo, iteradores, len(paths))


    @try_except
    def on_button_accion_1_clicked(self, widget):  # Marcar
        modelo, iteradores, n = self.treeview_get_selected()
        if not n:
            return
        título = 'Marcar'
        mjs = '¿Desea marcar con uno o varios grupos\n{0} archivo{1} seleccionado{1}?'
        mjs = mjs.format(*(('el', '') if n == 1 else ('los', 's')))
        banderas = {i:0 for i in range(1, 11)}
        from dialogs import plugin_check, v_pregunta_elegir_banderas
        check = plugin_check('Eliminar de la lista de formatos modificados',
            tooltip='Se eliminarán los formatos')
        respuesta = v_pregunta_elegir_banderas(self.widgets['dialog'], 
            título, mjs, banderas, widget=check, borrar_excluye=True)
        if respuesta:
            eliminar = check.get()
            for iterador in iteradores:
                i = modelo[iterador].path.get_indices()[0]
                if eliminar:
                    archivo = self.datos.pop(i)
                    self.formatos.unset(archivo)
                    self.cache.pop(i)
                    modelo.remove(iterador)
                else:
                    archivo = self.datos[i]
                self.banderas.set(archivo, respuesta)


    @try_except
    def on_button_accion_2_clicked(self, widget):  # Modificar
        modelo, iteradores, n = self.treeview_get_selected()
        if not n:
            return
        mjs = 'Formato para {0} archivo{1} seleccionado{1}'
        mjs = mjs.format(*(('el', '') if n == 1 else ('los', 's')))
        from dialogs import v_pregunta_formatos
        respuesta = v_pregunta_formatos(self.widgets['dialog'], mjs)
        if respuesta:
            if isinstance(respuesta, dict):
                mapa = {'ancho':3, 'alto':4, 'giro':5, 'rh':6, 'rv':7}
                for iterador in iteradores:
                    i = modelo[iterador].path.get_indices()[0]
                    archivo = self.datos[i]
                    self.formatos.set(archivo, **respuesta)
                    for var, valor in respuesta.items():
                        pos = mapa[var]
                        modelo[iterador][pos] = valor
            else:
                for iterador in iteradores:
                    i = modelo[iterador].path.get_indices()[0]
                    archivo = self.datos.pop(i)
                    self.formatos.unset(archivo)
                    self.cache.pop(i)
                    modelo.remove(iterador)


    def carpeta_dirección(self, nombre):
        for n, d in self.cstore:
            if n == nombre:
                return d


    @try_except
    def on_button_accion_3_clicked(self, widget):  # Guardar
        modelo, iteradores, n = self.treeview_get_selected()
        if not n:
            return
        mjs = '¿Desea guardar {0} archivo{1} seleccionado{1}?'
        mjs = mjs.format(*(('el', '') if n == 1 else ('los', 's')))
        from dialogs import plugin_pis, v_pregunta_si_no
        pis = plugin_pis('m', left=55)
        respuesta = v_pregunta_si_no(self.widgets['dialog'], mjs, widget=pis)
        if respuesta:
            política = pis.get()
            key, cancel = self.accion_3(modelo, iteradores, política)
            self.wait_env[key].pop('data')


    def aplicar_ext(self, nombre, ext):
        return '{}.{}'.format(os.path.splitext(nombre)[0], ext.lstrip(os.extsep))


    @wait_task('Guardando archivos')
    def accion_3(self, key, modelo, iteradores, política):
        data = {
            'guardar': {},
            'eliminar': [],
        }
        for iterador in iteradores:
            i = modelo[iterador].path.get_indices()[0]
            archivo = self.datos.pop(i)
            ad, cn, an, ancho, alto, giro, rh, rv, ext = modelo[iterador]
            giro = giro % 360
            _nombre = self.aplicar_ext(an, ext)
            _carpeta = self.carpeta_dirección(cn)
            __carpeta = _carpeta
            _dirección = os.path.join(__carpeta, _nombre)

            if ad != _dirección and os.path.exists(_dirección):
                if política == 'ignorar':
                    continue
                elif política == 'sobrescribir':
                    data['eliminar'].append(_dirección)
                else:
                    iguales = filecmp.cmp(ad, _dirección)

                    if self.accion_3_destino:
                        _carpeta = self.accion_3_destino
                        _dirección = os.path.join(_carpeta, _nombre)
                        if os.path.exists(_dirección):
                            preguntar = True
                        else:
                            preguntar = False
                    else:
                        preguntar = True

                    if preguntar:
                        var = (self.widgets['dialog'], _carpeta, _nombre, 
                            iguales, self.accion_3_destino)
                        from dialogs import v_pregunta_archivo
                        _nombre, _carpeta, mismo = v_pregunta_archivo(*var)
                        if not (_nombre or _carpeta):
                            continue
                        if _carpeta:
                            __carpeta = _carpeta
                        _dirección = os.path.join(__carpeta, _nombre)
                        self.accion_3_destino = _carpeta if mismo else None
            try:
                carpeta = self.carpetas[__carpeta]
            except KeyError:
                carpeta = Carpeta(__carpeta, self.cfg['imagen'])
                self.nuevas_carpetas.add(carpeta)

            data['guardar'][archivo] = (i, carpeta, _nombre, _dirección, modelo, iterador)
        self.wait_env[key]['data'] = data


    def accion_3_thread(self, key):
        data = self.wait_env[key]['data']
        n = len(data['eliminar']) + len(data['guardar'])
        i = 0

        for i, d in enumerate(data['eliminar'], i + 1):
            método, comando = fborrar(d, self.borseg)
            self.log.info('Se eliminó [%s%s]: %s', método, comando, d)
            yield i / n

        for i, archivo in enumerate(data['guardar'], i + 1):
            index, carpeta, nombre, dirección, modelo, iterador = data['guardar'][archivo]
            ad, cn, an, ancho, alto, giro, rh, rv, ext = modelo[iterador]
            ext = ext.lstrip(os.extsep)
            try:
                if self.convert:
                    cmd = "convert '{}'".format(ad)
                    giro = (-giro) % 360  # Distinto que GdkPixbuf
                    if all((ancho, alto)):
                        cmd += " -resize {}x{}".format(ancho, alto)
                    if giro:
                        cmd += " -rotate {}".format(giro)
                    if rh:
                        cmd += " -flop"
                    if rv:
                        cmd += " -flip"
                    cmd += " {}:'{}'".format(ext, dirección)
                    Popen(cmd, shell=True).wait()
                else:
                    _pf = GdkPixbuf.Pixbuf.new_from_file(ad)
                    if self.cfg['imagen_aplicar_orientación']:
                        _pf = _pf.apply_embedded_orientation()
                    rgb = GdkPixbuf.Colorspace.RGB
                    pf = GdkPixbuf.Pixbuf.new(rgb, True, 8, ancho, alto)
                    c = GdkPixbuf.InterpType(self.cfg['imagen_calidad'])
                    f = ancho / _pf.get_width()
                    _pf.scale(pf, 0, 0, ancho, alto, 0, 0, f, f, c)
                    giro = round(giro / 90) * 90
                    if giro:
                        pf = pf.rotate_simple(GdkPixbuf.PixbufRotation(giro))
                    if rh:
                        pf = pf.flip(True)
                    if rv:
                        pf = pf.flip(False)
                    pf.savev(dirección, ext, [], [])
            except:
                self.log.exception('Archivo %s', dirección)
                exctype, value = sys.exc_info()[:2]
                error = '{}: {}  '.format(exctype.__name__, value)
                from dialogs import v_información
                mjs = ('Ocurrio un error al guardar el archivo, '
                    'vea el archivo de logs para mas detalles. \n '
                    '{} '.format(error))
                v_información(self.widgets['dialog'], 'ERROR', mjs)
            else:
                self.formatos.unset(archivo)
                banderas = self.banderas.pop(archivo)

                if ad != dirección:
                    método, comando = fborrar(ad, self.borseg)
                    log = ' [{}{}]: {} -> {}'.format(método, comando, ad, dirección)
                else:
                    log = ': {}'.format(ad)
                self.log.info('Se modificó%s', log)

                if archivo.carpeta != carpeta:
                    archivo.carpeta.eliminar(archivo)
                    carpeta.agregar(archivo)

                if archivo.nombre != nombre:
                    archivo.nombre = nombre
                    carpeta.ordenar()

                if banderas:
                    self.banderas.set(archivo, banderas, unicos=True)

                modelo.remove(iterador)
                self.cache.pop(index)
            yield i / n


    def ejecutar(self):
        response = self.widgets['dialog'].run()
        self.widgets['dialog'].hide_on_delete()
        if response == 2 and len(self.widgets['liststore']):
            mjs = 'Solo se guardarán los tamaño, giro y reflejo.'
            from dialogs import v_información
            v_información(self.parent, mjs)
            for i, var in enumerate(self.widgets['liststore']):
                ad, cn, an, ancho, alto, giro, rh, rv, ext = var
                archivo = self.datos[i]
                fimagen = self.formatos.get(archivo)
                d = (ancho, alto, round((giro % 360) / 90) * 90, rh, rv)
                fimagen['ancho', 'alto', 'giro', 'rh', 'rv'] = d
            nuevas_carpetas = self.nuevas_carpetas
        else:
            nuevas_carpetas = None
        return nuevas_carpetas



class ListaSimilares:

    log = log

    def __init__(self, parent, archivos, carpetas, banderas, fmovcop, fborrar, pix):
        self.carpetas = carpetas
        self.banderas = banderas
        self.fmovcop = fmovcop  # \ Funciones de viewer.Visor con @wait_task
        self.fborrar = fborrar  # /
        self.parent = parent
        self.builder = Gtk.Builder()
        self.builder.add_from_file(GUI)
        self.builder.connect_signals(self)
        objects = (
            'dialog',
            'label_titulo',
            'scrolledwindow',
            'button_accion_1',
            'button_accion_2',
            'button_accion_3',
            'button_salir_accion'
        )
        self.widgets = {obj: self.builder.get_object(obj) for obj in objects}

        if os.path.isfile(CSS):
            screen = Gdk.Screen.get_default()
            provider = Gtk.CssProvider()
            provider.load_from_path(CSS)
            priority = Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION
            context = Gtk.StyleContext()
            context.add_provider_for_screen(screen, provider, priority)

        vbox = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
        viewport = Gtk.Viewport()

        ncol = max(map(len, archivos.values()))
        pixgen = GdkPixbuf.Pixbuf.new_from_file_at_scale
        pixgen2 = GdkPixbuf.Pixbuf.new_from_file

        n = 0
        self.errores = {}
        self.archivos = {}
        self.iconview = []
        self.liststore = []
        self.separator = []

        load_icon = Gtk.IconTheme.get_default().load_icon
        size = round(max(min(pix[0], pix[1]) * 0.63, 32))
        self.pix_err = load_icon(Gtk.STOCK_DIALOG_ERROR, size, 0)

        for archivos in archivos.values():
            liststore = Gtk.ListStore(GdkPixbuf.Pixbuf, str, str, int)
            self.liststore.append(liststore)
            for archivo in archivos:
                label = ''
                tooltip = ''
                try:
                    pixbuf = pixgen(archivo.dirección, pix[0], pix[1], True)
                except:
                    try:
                        p = pixgen2(archivo.dirección)
                        _ancho = p.get_width()
                        _alto = p.get_height()
                        _rel = _ancho / _alto
                        f_ancho = _ancho / pix[0]
                        f_alto = _alto / pix[1]
                        if f_ancho > f_alto:
                            ancho = pix[0]
                            alto = round(ancho / _rel)
                        else:
                            alto = pix[1]
                            ancho = round(alto * _rel)
                        cs = GdkPixbuf.Colorspace.RGB
                        pixbuf = GdkPixbuf.Pixbuf.new(cs, True, 8, ancho, alto)
                        c = GdkPixbuf.InterpType(1)
                        f = ancho / _ancho
                        p.scale(pixbuf, 0, 0, ancho, alto, 0, 0, f, f, c)
                    except:
                        exctype, value = sys.exc_info()[:2]
                        var = (archivo.dirección, exctype.__name__, value)
                        self.log.debug('ERROR con "%s" %s: %s', *var)
                        self.errores[n] = archivo
                        label += '<b>ERROR</b>\n'
                        tooltip += 'ERROR: {}\n'.format(value)
                        pixbuf = self.pix_err

                label += archivo.nombre
                tooltip += archivo.dirección
                if pix[2]:
                    label += '\n<i>{}</i>'.format(unidad_a_multiplo(archivo.tamaño, 2))
                liststore.append([pixbuf, label, tooltip, n])
                self.archivos[n] = archivo
                n += 1
            iconview = Gtk.IconView(model=liststore)
            iconview.set_pixbuf_column(0)
            iconview.set_markup_column(1)
            iconview.set_tooltip_column(2)
            iconview.set_hexpand(True)
            iconview.set_columns(ncol)
            iconview.set_item_width(pix[0])
            iconview.set_margin(5)
            iconview.set_row_spacing(0)
            iconview.set_selection_mode(Gtk.SelectionMode.MULTIPLE)
            self.iconview.append(iconview)
            vbox.pack_start(iconview, expand=False, fill=False, padding=0)
            separator = Gtk.Separator()
            separator.set_margin_top(1)
            separator.set_margin_bottom(1)
            self.separator.append(separator)
            vbox.pack_start(separator, expand=False, fill=False, padding=0)

        viewport.add(vbox)
        self.widgets['scrolledwindow'].add(viewport)

        self.widgets['label_titulo'].set_text('Imágenes similares')
        self.widgets['button_accion_1'].set_label('Marcar')
        self.widgets['button_accion_2'].set_label('Mover')
        self.widgets['button_accion_3'].set_label('Borrar')
        tooltip1 = 'Marcar las imágenes seleccionadas'
        tooltip2 = 'Mover las imágenes seleccionadas'
        tooltip3 = 'Borrar las imágenes seleccionadas'
        self.widgets['button_accion_1'].set_tooltip_text(tooltip1)
        self.widgets['button_accion_2'].set_tooltip_text(tooltip2)
        self.widgets['button_accion_3'].set_tooltip_text(tooltip3)

        if parent:
            ancho = parent.get_allocated_width()
            alto = parent.get_allocated_height()
            size = round(FANCHO * ancho, -1), round(FALTO * alto, -1)
            self.widgets['dialog'].set_default_size(*size)
            self.widgets['dialog'].set_transient_for(parent)
        self.widgets['dialog'].show_all()
        self.widgets['button_salir_accion'].hide()


    @try_except
    def on_button_todo_clicked(self, widget):
        for iconview in self.iconview:
            iconview.select_all()


    @try_except
    def on_button_nada_clicked(self, widget):
        for iconview in self.iconview:
            iconview.unselect_all()


    #@Logging
    def get_selected(self, eliminar=False):
        datos = list(zip(self.iconview, self.liststore, self.separator))
        archivos = []
        for iconview, liststore, separator in datos:
            for path in iconview.get_selected_items():
                iterador = liststore.get_iter(path)
                n = liststore.get_value(iterador, 3)
                if eliminar:
                    archivo = self.archivos.pop(n)
                    liststore.remove(iterador)
                else:
                    archivo = self.archivos[n]
                archivos.append(archivo)
            if not len(liststore):
                iconview.destroy()
                separator.destroy()
                self.iconview.remove(iconview)
                self.liststore.remove(liststore)
                self.separator.remove(separator)
        return archivos


    #@Logging
    def nselected(self):
        return sum(len(i.get_selected_items()) for i in self.iconview)


    @try_except
    def on_button_accion_1_clicked(self, widget):  # Marcar
        n = self.nselected()
        if not n:
            return
        título = 'Marcar'
        mjs = '¿Desea marcar con uno o varios grupos\n{0} archivo{1} seleccionado{1}?'
        mjs = mjs.format(*(('el', '') if n == 1 else ('los', 's')))
        banderas = {i:0 for i in range(1, 11)}
        from dialogs import plugin_check, v_pregunta_elegir_banderas
        check = plugin_check('Eliminar de la lista de semejantes',
            tooltip='No se eliminan los archivos del disco')
        respuesta = v_pregunta_elegir_banderas(self.widgets['dialog'], 
            título, mjs, banderas, widget=check, borrar_excluye=True)
        if respuesta:
            eliminar = check.get()
            archivos = self.get_selected(eliminar)
            for archivo in archivos:
                self.banderas.set(archivo, respuesta)


    @try_except
    def on_button_accion_2_clicked(self, widget):  # Mover
        n = self.nselected()
        if not n:
            return
        plural = ('', 'agen') if n == 1 else ('s', 'ágenes')
        grupos = self.banderas.get_activas(range(1, 10))
        título = 'Elija la carpeta destino y/o el nuevo nombre para la{} im{}'
        título = título.format(*plural)
        desmarcar = ('Eliminar de la lista de semejantes',
            'No se eliminan los archivos del disco')
        from movcop import MovCop
        var = {'banderas': n, 'desmarcar': desmarcar}
        dialog = MovCop(self.widgets['dialog'], título, self.carpetas, **var)
        patrones, política, desmarcar, igrupo = dialog.ejecutar()
        if patrones:
            eliminar = desmarcar or patrones.get('acción') == 'mover'
            archivos = self.get_selected(eliminar)
            self.fmovcop(patrones, política, desmarcar, igrupo, archivos=archivos)


    @try_except
    def on_button_accion_3_clicked(self, widget):  # Borrar
        n = self.nselected()
        if not n:
            return
        plural = ('', 'agen') if n == 1 else ('s', 'ágenes')
        mjs = '¿Desea borrar la{0} im{1} seleccionada{0}?'
        mjs = mjs.format(*plural)
        aclaración = 'Esta operación borra la{0} im{1} del disco.'
        aclaración = aclaración.format(*plural)
        from dialogs import v_pregunta_si_no
        respuesta = v_pregunta_si_no(self.widgets['dialog'], mjs, aclaración)
        if respuesta:
            archivos = self.get_selected(eliminar=True)
            self.fborrar(True, archivos)


    def ejecutar(self):
        response = self.widgets['dialog'].run()
        self.widgets['dialog'].hide_on_delete()



class ListaCombinacionesTeclas:

    log = log

    def __init__(self, parent, keymaps):
        self.parent = parent
        self.builder = Gtk.Builder()
        self.builder.add_from_file(GUI)
        self.builder.connect_signals(self)
        objects = (
            'dialog',
            'label_titulo',
            'scrolledwindow',
            'buttonbox',
            'button_salir_accion',
            'button_reset',
        )
        self.widgets = {obj: self.builder.get_object(obj) for obj in objects}

        if os.path.isfile(CSS):
            screen = Gdk.Screen.get_default()
            provider = Gtk.CssProvider()
            provider.load_from_path(CSS)
            priority = Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION
            context = Gtk.StyleContext()
            context.add_provider_for_screen(screen, provider, priority)

        # keymaps = {area: {'acción': (mask, key)}}
        # mask_key = (mask, key)

        self.treestore = Gtk.TreeStore(str, str, str)  # acción, hotkey1, hotkey2
        for area, acción_hotkeys in keymaps.items():
            it = self.treestore.append(None, (area.capitalize(), '', ''))
            for acción, hotkeys in sorted(acción_hotkeys.items()):
                if hotkeys:
                    hotkeys.sort()
                    hk1, hk2 = hotkeys if len(hotkeys) > 1 else (hotkeys[0], '')
                else:
                    hk1, hk2 = '', ''
                self.treestore.append(it, (acción.capitalize(), hk1, hk2))

        self.treeview = Gtk.TreeView()
        self.treeview.set_model(self.treestore)
        self.treeview.set_headers_clickable(False)
        self.treeview.set_headers_visible(True)
        self.treeview.set_show_expanders(False)
        self.treeview.set_level_indentation(26)
        self.treeview.set_search_column(0)
        self.treeview.set_hscroll_policy(Gtk.ScrollablePolicy.NATURAL)
        self.treeview.set_vscroll_policy(Gtk.ScrollablePolicy.NATURAL)
        self.treeview.connect('button-press-event', self.on_button_press_event)
        self.treeview.expand_all()

        self.columnas = collections.OrderedDict((
            # título     i width expand
            ("Acción",  (0, 220,  None)),
            ("Tecla 1", (1, None, True)),
            ("Tecla 2", (2, None, True)),
        ))
        for título, (pos, width, expand) in self.columnas.items():
            renderer = Gtk.CellRendererText()
            column = Gtk.TreeViewColumn(título, renderer, text=pos)
            column.set_sizing(Gtk.TreeViewColumnSizing.FIXED)
            if width is not None:
                column.set_fixed_width(width)
            if expand is not None:
                column.set_expand(expand)
            self.treeview.append_column(column)

        self.widgets['scrolledwindow'].add(self.treeview)
        label = 'Combinaciones de teclas\nDoble click para editar'
        self.widgets['label_titulo'].set_text(label)
        
        if parent:
            ancho = parent.get_allocated_width()
            alto = parent.get_allocated_height()
            size = round(FANCHO * ancho, -1), round(FALTO * alto, -1)
            self.widgets['dialog'].set_default_size(*size)
            self.widgets['dialog'].set_transient_for(parent)
        self.widgets['dialog'].show_all()
        self.widgets['buttonbox'].hide()
        self.widgets['button_reset'].show()
        self.widgets['button_salir_accion'].set_label('Salir y Guardar')


    @try_except
    def on_button_press_event(self, widget, event):
        nclicks = event.get_click_count()[1]
        if event.button == Gdk.BUTTON_PRIMARY and nclicks == 2:
            path, column, *cell = widget.get_path_at_pos(event.x, event.y)
            if len(path.get_indices()) == 2:  # area no me interesa
                col = column.get_title()
                it = self.treestore.get_iter(path)
                k = self.columnas[col][0]
                from dialogs import v_pregunta_tecla
                event = v_pregunta_tecla(self.widgets['dialog'], col)
                if event:
                    mask = event.state & FLAGS
                    key = event.keyval
                    hotkey = maskkey_to_hotkey((mask, key))
                    if hotkey not in self.treestore[it]:
                        for area in self.treestore:  # Eliminar si ya existe
                            for acción in area.iterchildren():
                                if acción[1] == hotkey:
                                    acción[1] = ''
                                if acción[2] == hotkey:
                                    acción[2] = ''
                        self.treestore[it][k] = hotkey
                        return True  # stop event


    @try_except
    def on_button_todo_clicked(self, widget):
        pass

    @try_except
    def on_button_nada_clicked(self, widget):
        pass

    @try_except
    def on_button_accion_1_clicked(self, widget):
        pass

    @try_except
    def on_button_accion_2_clicked(self, widget):
        pass

    @try_except
    def on_button_accion_3_clicked(self, widget):
        pass


    def ejecutar(self):
        response = self.widgets['dialog'].run()
        self.widgets['dialog'].hide_on_delete()

        if response == 1:  # button_reset
            return 'Reset'

        elif response == 2:  # button_salir_accion
            keymaps = {}
            for iterarea in self.treestore:
                area = iterarea[0].lower()
                for acción_hotkeys in iterarea.iterchildren():
                    acción, hotkey1, hotkey2 = acción_hotkeys[:]
                    hotkeys = (hotkey1, hotkey2)
                    keymaps.setdefault(area, {})[acción.lower()] = hotkeys
            return keymaps

