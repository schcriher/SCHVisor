#!/usr/bin/env python3
#-*- coding: utf-8 -*-
#
# Copyright (C) 2013 Cristian Hernán Schmidt
#
# This file is part of SCHVisor.
#
# SCHVisor is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# SCHVisor is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with SCHVisor. If not, see <http://www.gnu.org/licenses/>.

import os
import re
import sys
import imghdr
import shutil
import hashlib
import functools
import threading, _thread
import subprocess
import unicodedata

from subprocess import Popen, PIPE
from gi.repository import Gdk, GdkPixbuf, GObject

NO_PALABRAS_RE = re.compile('[^\w]', re.IGNORECASE)
TAMAÑO_RE = re.compile('[^\d. bkmg+\-*/]+')  # se usa eval



NO_MASK = 0
CONTROL = int(Gdk.ModifierType.CONTROL_MASK)
SHIFT = int(Gdk.ModifierType.SHIFT_MASK)
SUPER = int(Gdk.ModifierType.SUPER_MASK)
FLAGS = CONTROL | SHIFT | SUPER

mask_name = {
    CONTROL: 'CONTROL',
    SHIFT:   'SHIFT',
    SUPER:   'SUPER',
}
name_mask = {n: i for i, n in mask_name.items()}


def maskkey_to_hotkey(maskkey):
    mask, key = maskkey
    secuencia = []
    for m in mask_name:
        if m & mask:
            secuencia.append('<{}>'.format(mask_name[m]))
    name = Gdk.keyval_name(key)
    if name:
        secuencia.append(name)
    return ' + '.join(secuencia)


def hotkey_to_maskkey(hotkey):
    elementos = hotkey.split(' + ')
    mask = 0
    for m in elementos[:-1]:
        mask |= name_mask[m.strip('<>')]
    key = Gdk.keyval_from_name(elementos[-1])
    return (mask, key)



def factorial(n):
    productorio = 1
    for i in range(1, n + 1):
        productorio *= i
    return productorio



def ncombinaciones(n, m):
    return round(factorial(n) / (factorial(n - m) * factorial(m)))



def try_except(método):
    @functools.wraps(método)
    def _try_except(self, *args, **kwargs):
        if hasattr(self, 'sleepdog'):
            self.sleepdog = True
        try:
            return método(self, *args, **kwargs)
        except:
            mensaje = 'Error ejecutando el método "{}"'.format(método.__name__)
            self.log.exception(mensaje)
            exctype, value = sys.exc_info()[:2]
            mensaje += '\n\n{}: {} '.format(exctype.__name__, value)
            from dialogs import v_información
            parent = self.widgets.get('dialog') or self.widgets.get('window')
            v_información(parent, 'ERROR', mensaje)
        finally:
            if hasattr(self, 'sleepdog'):
                self.sleepdog = False
    return _try_except



class GeneratorTask:
    # http://unpythonic.blogspot.com/2007/08/using-threads-in-pygtk.html

    def __init__(self, key, generator, progress, complete):
        self.key = key
        self.generator = generator  # funsion_thread
        self.progress = progress    # wait_progress
        self.complete = complete    # funsion_complete

    def _start(self):
        self._stopped = False
        for fraction in self.generator(self.key):
            GObject.idle_add(self.progress, self.key, fraction)
            if self._stopped:
                _thread.exit()
        if self.complete is not None:
            GObject.idle_add(self.complete, self.key)

    def start(self):
        self.thread = threading.Thread(target=self._start)
        self.thread.start()

    def stop(self):
        self._stopped = True

    def is_alive(self):
        return self.thread.is_alive()

    def join(self):
        return self.thread.join()



def modulo_clase_nombre(clase, método):
    modulo = método.__module__
    clase = clase.__class__.__name__
    nombre = método.__name__
    return '{}.{}.{}'.format(modulo, clase, nombre)



def rgetattr(obj, path):
    items = str(path).split('.')
    for item in items:
        obj = getattr(obj, item)
    return obj



class WaitTask:

    wait_env = {}  # datos de las ventana de espera

    def wait_progress(self, key, fraction):
        dialog = self.wait_env[key].get('dialog')
        if dialog:
            dialog.fraction = fraction  # el dialog decide cada cuanto mostrar
            if fraction == 1:
                dialog.response(0)

    def wait_task(mensaje):
        def decorador(método):
            @functools.wraps(método)
            def _task(self, *args, **kwargs):
                key = modulo_clase_nombre(self, método)
                env = self.wait_env[key] = {}

                response = método(self, key, *args, **kwargs)
                if response is True:
                    return  # stop

                from dialogs import WaitDialog
                parent = self.widgets.get('dialog') or self.widgets.get('window')
                env['dialog'] = WaitDialog(parent, mensaje, env)

                generator = getattr(self, '{}_thread'.format(método.__name__))
                progress = self.wait_progress
                complete = '{}_complete'.format(método.__name__)
                complete = getattr(self, complete) if hasattr(self, complete) else None

                env['task'] = GeneratorTask(key, generator, progress, complete)
                env['task'].start()
                env['dialog'].run()

                cancel = env['dialog'].cancel
                env.pop('task').join()
                env.pop('dialog').destroy()

                return key, cancel
            return _task
        return decorador



class Sentinela:

    def __init__(self, archivo):
        self.archivo = archivo
        self.fecha_a = archivo.mfecha()  # \ últimas
        self.fecha_b = self.fecha_a      # / dos fechas
        self.cambió = False

    def get_actualizar(self):
        # No da positivo si el archivo está cambiando
        self.fecha_a, self.fecha_b = self.fecha_b, self.archivo.mfecha()
        if self.fecha_a == self.fecha_b:
            # Estable
            if self.cambió:
                # Ahora es seguro leerlo
                self.cambió = False
                return True
        else:
            self.cambió = True



borseg_posibles = {
    # Rápido
     1: 'shred -zun0 "{0}"',
     2: 'shred -zun1 "{0}"',
     3: 'shred -zun2 "{0}"',
     4: 'shred -zun38 "{0}"',
     # Lento
     5: 'srm -zllf "{0}"',
     6: 'srm -zll "{0}"',
     7: 'srm -zl "{0}"',
     8: 'srm -z "{0}"',
     # Paranoico
     9: 'shred -zn0 "{0}"; srm -zllf "{0}"',
    10: 'shred -zn1 "{0}"; srm -zll "{0}"',
    11: 'shred -zn2 "{0}"; srm -zl "{0}"',
    12: 'shred -zn38 "{0}"; srm -z "{0}"',
}

borseg_tipos = {
    1: 'rápido',
    2: 'lento',
    3: 'paranoico',
}

def borseg_comando(borseg, shred, srm):
    i = (borseg - 1) // 4 + 1
    posibles = []
    if shred:
        posibles.append(1)
    if srm:
        posibles.append(2)
    if shred and srm:
        posibles.append(3)
    if i in posibles:
        return borseg_tipos[i], borseg_posibles[borseg]


def fborrar(dirección, borseg):
    if borseg[0]:
        método, comando = borseg_comando(*borseg)
        subprocess.Popen(comando.format(dirección), shell=True).wait()
    else:
        método, comando = 'normal', ''
        os.remove(dirección)
    if comando:
        comando = ':{}'.format(comando.replace('"{0}"', '<f>'))
    return (método, comando)


def frenombrar(old, new, borseg):
    if borseg[0]:
        shutil.copy2(old, new)
        método, comando = fborrar(old, borseg)
    else:
        método, comando = 'normal', ''
        shutil.move(old, new)
    return (método, comando)



def elimina_caracteres_diacriticos(cadena):
    """Elimina los caracteres Mark y Nonspacing (category 'Mn') de la cadena.
    """
    nfkd = unicodedata.normalize('NFKD', cadena)
    return ''.join(c for c in nfkd if unicodedata.category(c) != 'Mn')



def normalizar(obj):
    """Normalizador para ordenar ficheros, pone en minisculas y
       elimina los caracteres Mark y Nonspacing (category 'Mn')
    """
    if hasattr(obj, 'dirección'):
        obj = obj.dirección
    return elimina_caracteres_diacriticos(obj.lower())



def multiplo_a_unidad(valor):
    "Conversor de tamaños de archivos"
    valor = TAMAÑO_RE.sub('', str(valor).lower())
    for u, v in (('b',''), ('k','*1e3'), ('m','*1e6'), ('g','*1e9')):
        valor = valor.replace(u, v)
    return round(eval(valor))



def unidad_a_multiplo(valor, digit=1):
    "Conversor de tamaños de archivos"
    múltiplo = 1000
    if valor < múltiplo:
        prefijo = 'B'
    else:
        prefijos = ('KB', 'MB', 'GB')
        for prefijo in prefijos:
            valor /= múltiplo
            if valor < múltiplo * 0.86:
                break
    return '{0:.{digit}f} {1}'.format(valor, prefijo, digit=digit)



def md5sum(archivo):
    "Calcula la suma MD5 de un archivo"
    md5 = hashlib.md5()
    with open(archivo, mode='br') as fichero:
        for buffer in iter(functools.partial(fichero.read, 128), b''):
            md5.update(buffer)
    return md5.hexdigest()



def descomponer_color_html(obj, max):
    offset = int(len(obj) / 3)
    i1 = 1 * offset
    i2 = 2 * offset
    r, g, b = obj[:i1], obj[i1:i2], obj[i2:]
    factor = max / int('f' * offset, 16)
    redondeo = int if max > 1 else lambda i: i
    rgb = (redondeo(int(v, 16) * factor) for v in (r, g, b))
    return rgb



def color(obj, resultado):

    if resultado.startswith('html'):  # Ver si hay que incluir la transparencia
        if resultado[-1].isdigit():
            n = int(resultado[-1])
            if n not in (1, 2, 3, 4):
                raise ValueError('"{}" incorrecto'.format(resultado))
        else:
            n = 2
        f = int('f' * n, 16)
        if isinstance(obj, (Gdk.RGBA, Gdk.Color)):
            factor = f if isinstance(obj, Gdk.RGBA) else f / 65535
            rgb = map(round, (v * factor for v in (obj.red, obj.green, obj.blue)))
            html = '#{:0{n}x}{:0{n}x}{:0{n}x}'.format(*rgb, n=n)
        elif isinstance(obj, str) and obj.startswith('#') and len(obj) in (4, 7, 10, 13):
            if (len(obj) - 1 / 3) == n:
                html = obj
            else:
                rgb = map(int, descomponer_color_html(obj[1:], int('f' * n, 16)))
                html = '#{:0{n}x}{:0{n}x}{:0{n}x}'.format(*rgb, n=n)
        return html.upper()

    elif resultado == 'RGBA':
        if isinstance(obj, Gdk.RGBA):
            return obj
        elif isinstance(obj, Gdk.Color):
            factor = 1 / 65535
            rgb = (v * factor for v in (obj.red, obj.green, obj.blue))
            return Gdk.RGBA(*rgb)
        elif isinstance(obj, str) and obj.startswith('#'):
            return Gdk.RGBA(*descomponer_color_html(obj[1:], 1))

    raise TypeError('No se puede transformar, {} a "{}"'.format(obj, resultado))



class List(list):

    def remove(self, x):
        """Remueve el valor indicado. Si no existe no hace nada. No devuelve nada."""
        try:
            super().remove(x)
        except ValueError:
            pass

    def pull(self, x, default=None):
        """Remueve el valor indicado devolviendolo, si no existe se devuelve default."""
        try:
            super().remove(x)
            return x
        except ValueError:
            return default



class Dict(dict):

    def remove(self, x):
        """Remueve el valor indicado. Si no existe no hace nada. No devuelve nada."""
        try:
            self.pop(x)
        except KeyError:
            pass

    def pull(self, x, default=None):
        """Remueve el valor indicado devolviendolo, si no existe se devuelve default."""
        try:
            return super().pop(x)
        except KeyError:
            return default



def posiciones(cadena, letra, offset=0):
    """Devuelve todas las posiciones de la "letra" en la "cadena".
       "offset" es un entero que modifica todas las posiciones.
    """
    for i, c in enumerate(cadena):
        if c == letra:
            yield i + offset



def texdiff(a, b, insensible=True, acentos=False, solo_palabras=False):
    """Devuelve el "tanto por uno" de la diferencia entre "a" y "b".
       Una letra mal está mas penalizada en una palabra corta que en una larga.

       insensible=True      No se distingue entre minúsculas y mayúsculas.
       acentos=False        No se distingue entre palabras con y sin acento.
       solo_palabras=False  Se analiza solo los caracteres considerados palabra
                            en las expresiones regulares ('\w' del paquete re)

       USO:
           fracción = diferencia(a, b)

       DONDE:
           0 <= fracción <= 1
           0.0 Cero diferencia, "a" y "b" son iguales
           0.5 Media diferencia, ejemplo: las mismas letras en otro orden
           1.0 Completa diferencia, ninguna letra coincide

       DISEÑO: Schmidt Cristian Hernán <schcriher@gmail.com>
    """
    if insensible:
        a = a.lower()
        b = b.lower()

    if not acentos:
        a = elimina_caracteres_diacriticos(a)
        b = elimina_caracteres_diacriticos(b)

    if solo_palabras:
        a = NO_PALABRAS_RE.sub('', a)
        b = NO_PALABRAS_RE.sub('', b)

    n = len(a + b)
    letras = set(a + b)

    diff_cantidad = 0
    diff_posicion = 0

    for letra in letras:
        count1 = a.count(letra)
        count2 = b.count(letra)
        diff_cantidad += abs(count1 - count2)

        pos1 = set(posiciones(a, letra))
        pos2 = set(posiciones(b, letra))
        diff_posicion += len(pos1.symmetric_difference(pos2))

    return (diff_cantidad + diff_posicion) / (2 * n)



extcmd = "identify -format '%m' '{}[0]'"

def identify_format(dirección):
    "Obtiene el formato del archivo por análisis de contenido"
    p = Popen(extcmd.format(dirección), stdout=PIPE, stderr=PIPE, shell=True)
    ext, err = p.communicate()
    if err:
        raise OSError(err.decode('utf8', 'replace'))
    return ext.strip().decode().lower()


def get_ext(dirección, por_contenido=True):
    if por_contenido:
        try:
            ext = identify_format(dirección)
        except:
            try:
                ext = imghdr.what(dirección)
            except:
                ext = None
    else:
        ext = None
    if ext:
        ext = '{}{}'.format(os.extsep, ext)
    else:
        nombre = os.path.split(dirección)[1]
        ext = os.path.splitext(nombre)[1].lower().replace('jpg', 'jpeg')
    return ext

