#!/usr/bin/env python3
#-*- coding: utf-8 -*-

# Copyright (C) 2013 Cristian Hernán Schmidt
#
# This file is part of SCHVisor.
#
# SCHVisor is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# SCHVisor is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with SCHVisor. If not, see <http://www.gnu.org/licenses/>.

import os
import sys
import logging

from subprocess import Popen, PIPE
from gi.repository import Gtk, Gdk

from debug import Log
from utils import (borseg_comando, borseg_posibles, try_except,
    multiplo_a_unidad, unidad_a_multiplo)

PATH, NAME = os.path.split(os.path.abspath(__file__))
BASE, EXT = os.path.splitext(NAME)
GUI = os.path.join(PATH, '..', 'ui', '{}.ui'.format(BASE))
CSS = os.path.join(PATH, '..', 'css', '{}.css'.format(BASE))



def imagen_exts(valores):
    if isinstance(valores, str):
        lista = set(valores.lower().replace('.', ' ').strip().split())
        if 'jpg' in lista or 'jpeg' in lista:
            lista.add('jpeg')
            lista.add('jpg')
        return sorted(lista)
    else:
        lista = set(valores)
        if 'jpg' in valores:
            lista.remove('jpg')
            lista.add('jpeg')
        return ' '.join(sorted(lista))



class Config:

    log = Log(logging.getLogger(__name__), sub=1)

    objetos = (
        'checkbutton_ventana_cargar_guardar_frm',
        'checkbutton_ventana_cargar_guardar_fph',
        'checkbutton_ventana_cargar_guardar_keymap',
        'checkbutton_ventana_decoración',
        'checkbutton_ventana_usar_convert',
        'combobox_ventana_iniciar_estado',
        'combobox_ventana_salir_preguntar',

        'checkbutton_carpetas_tamaño_cero',
        'checkbutton_carpetas_activar_panel',
        'spinbutton_avances_multiples',
        'spinbutton_carpetas_panel_ancho',
        'spinbutton_carpetas_watchdog',
        'spinbutton_imagen_watchdog',

        'checkbutton_imagen_mostrar_fecha',
        'checkbutton_imagen_mostrar_tamaño',
        'checkbutton_imagen_extpc',  #\
        'entry_imagen_tmin',         # \
        'entry_imagen_tmax',         #  > key = 'imagen'
        'entry_imagen_exts',         # /
        'entry_imagen_formato_fecha',
        'checkbutton_imagen_aplicar_orientación',
        'checkbutton_imagen_scrolled_correccion',
        'combobox_imagen_borseg',
        'spinbutton_imagen_zoom_min',
        'spinbutton_imagen_zoom_max',
        'spinbutton_imagen_zoom_factor',
        'combobox_imagen_calidad',

        'checkbutton_fingerprint_mostrar_tamaño',
        'spinbutton_fingerprint_imagen_ancho',
        'spinbutton_fingerprint_imagen_alto',
        'spinbutton_fingerprint_size',
        'spinbutton_fingerprint_hdepth',

        'checkbutton_banderas_multiples',
        'spinbutton_banderas_boton_ancho',
        'colorbutton_banderas_color_grupos',
        'colorbutton_banderas_color_borrar',
        'colorbutton_banderas_color_set',
        'colorbutton_banderas_color_unset',
    )
    métodos = {        # get/set    # tipo
        'checkbutton':  'active',   # bool
        'combobox':     'active',   # int
        'entry':        'text',     # str
        'colorbutton':  'rgba',     # Gdk.Color
        'spinbutton':   'value',    # float
    }
    adaptaciones = {
        'imagen_tmin':          {'set': unidad_a_multiplo, 'get': multiplo_a_unidad},
        'imagen_tmax':          {'set': unidad_a_multiplo, 'get': multiplo_a_unidad},
        'imagen_exts':          {'set': imagen_exts,       'get': imagen_exts},
        'imagen_watchdog':      {'set': int,               'get': int},
        'carpetas_watchdog':    {'set': int,               'get': int},
        'avances_multiples':    {'set': int,               'get': int},
        'fingerprint_size':     {'set': int,               'get': int},
        'fingerprint_hdepth':   {'set': int,               'get': int},
        'carpetas_panel_ancho': {'set': int,               'get': int},
        'banderas_boton_ancho': {'set': int,               'get': int},
    }

    def __init__(self, parent, cfg):
        self.iniciando = True
        self.parent = parent
        self.builder = Gtk.Builder()
        self.builder.add_from_file(GUI)
        self.builder.connect_signals(self)
        objects = (
            'dialog',
            'label_carpeta',
            'label_version',
            'grid_general_varios',
            'grid_general_fingerprint',
            'box_imagen_borseg',
            'liststore_imagen_borseg',
        )
        self.widgets = {o: self.builder.get_object(o) for o in objects}
        self.widgets.update({o: self.builder.get_object(o) for o in self.objetos})

        if os.path.isfile(CSS):
            screen = Gdk.Screen.get_default()
            provider = Gtk.CssProvider()
            provider.load_from_path(CSS)
            priority = Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION
            context = Gtk.StyleContext()
            context.add_provider_for_screen(screen, provider, priority)

        self.cfg = cfg
        self.actualizar_carpeta = False

        grid = Gtk.Grid()
        grid.set_column_spacing(15)
        grid.set_column_homogeneous(True)
        grid.set_margin_left(5)

        ncol = 3
        software = sorted(cfg.software)
        for left in range(ncol):
            for top, soft in enumerate(software[left::ncol]):
                p = Popen(['which', soft], stdout=PIPE).communicate()
                which = p[0].decode().strip()
                check = Gtk.CheckButton(which or soft)
                activo = bool(which)
                cfg.software[soft] = activo
                check.set_active(activo)
                check.set_sensitive(activo)
                no = '' if activo else 'no '
                check.set_tooltip_text('Software {}instalado'.format(no))
                check.connect("toggled", self.on_software_toggled, activo)
                grid.attach(check, left, top, 1, 1)
        self.widgets['grid_general_varios'].attach(grid, 1, 1, 1, 1)

        shred, srm = cfg.get('shred'), cfg.get('srm')
        if shred or srm:
            ids = []
            nombres = []
            if shred:
                ids.extend(range(1, 5))
                nombres.extend('Rápido nivel {}'.format(i) for i in range(1, 5))
            if srm:
                ids.extend(range(5, 9))
                nombres.extend('Lento nivel {}'.format(i) for i in range(1, 5))
            if shred and srm:
                ids.extend(range(9, 13))
                nombres.extend('Paranoico nivel {}'.format(i) for i in range(1, 5))
            for i, nivel in zip(ids, nombres):
                self.widgets['liststore_imagen_borseg'].append([i, nivel])

        self.datos('set')
        
        tooltip = 'Software no disponible ({})'

        if not (shred or srm):
            tip = tooltip.format('shred y/o srm')
            self.widgets['combobox_imagen_borseg'].set_active(0)
            self.widgets['box_imagen_borseg'].set_sensitive(False)
            self.widgets['box_imagen_borseg'].set_tooltip_text(tip)

        if not cfg.get('convert'):
            tip = tooltip.format('convert')
            self.widgets['grid_general_fingerprint'].set_sensitive(False)
            self.widgets['grid_general_fingerprint'].set_tooltip_text(tip)
            for children in self.widgets['grid_general_fingerprint'].get_children():
                children.set_tooltip_text('')
                for child in children.get_children():
                    child.set_tooltip_text('')
            self.widgets['checkbutton_ventana_usar_convert'].set_active(False)
            self.widgets['checkbutton_ventana_usar_convert'].set_sensitive(False)
            self.widgets['checkbutton_ventana_usar_convert'].set_tooltip_text(tip)

        carpeta = cfg.archivo.base.carpeta
        cfg_dir = cfg.archivo.base.cfg_dir
        self.widgets['label_carpeta'].set_text(carpeta)
        self.widgets['label_carpeta'].set_tooltip_text('Fijado en: {}'.format(cfg_dir))
        self.widgets['label_version'].set_text('Versión {}'.format(cfg.version))
        self.widgets['dialog'].set_transient_for(self.parent)
        self.widgets['dialog'].show_all()
        self.iniciando = False


    @try_except
    def on_button_carpeta_clicked(self, widget):
        nborseg = self.widgets['combobox_imagen_borseg'].get_active()
        shred, srm = self.cfg['shred', 'srm']
        borseg = (nborseg, shred, srm)
        carpeta = self.cfg.archivo.base.carpeta
        from dialogs import v_pregunta_carpeta
        título = 'Elija otra carpeta para las configuraciones'
        dirección = v_pregunta_carpeta(self.widgets['dialog'], título, carpeta)
        if dirección:
            self.cfg.archivo.base.set_carpeta(dirección, borseg)
            self.widgets['label_carpeta'].set_text(dirección)
 

    @try_except
    def on_software_toggled(self, widget, activo):
        "Mantiene el estado original, son de solo lectura"
        widget.set_active(activo)


    @try_except
    def on_combobox_imagen_borseg_changed(self, widget):
        cmdrmeq = '[equivalente a] rm -f "{0}"'
        comando = borseg_posibles.get(widget.get_active(), cmdrmeq)
        widget.set_tooltip_text('Comando: {}'.format(comando))


    def datos(self, flujo):
        if flujo == 'set':
            img = self.cfg['imagen']
            self.cfg['imagen_exts'] = img['exts']
            self.cfg['imagen_tmin'] = img['tmin']
            self.cfg['imagen_tmax'] = img['tmax']
            self.cfg['imagen_extpc'] = img['extpc']

        for objeto in self.objetos:
            tipo, var = objeto.split('_', 1)
            nombre = '{}_{}'.format(flujo, self.métodos[tipo])
            método = getattr(self.widgets[objeto], nombre)
            adaptar = self.adaptaciones.get(var, {}).get(flujo, lambda o: o)
            try:
                if flujo == 'set':
                    método(adaptar(self.cfg[var]))
                else:
                    self.cfg[var] = adaptar(método())
            except:
                self.log.exception('%s valores', flujo)
                exctype, value = sys.exc_info()[:2]
                error = '{}: {}  '.format(exctype.__name__, value)
                from dialogs import v_información
                acción = 'SET_GUI(cfg[{}])' if flujo == 'set' else 'cfg[{}] = GET_GUI()'
                mensaje = 'Operación: {}\n{}'.format(acción.format(var), error)
                parent = self.parent if flujo == 'set' else self.widgets['dialog']
                v_información(parent, 'ERROR', mensaje)

        if flujo == 'get':
            img = {
                'exts': self.cfg.pop('imagen_exts'),
                'tmin': self.cfg.pop('imagen_tmin'),
                'tmax': self.cfg.pop('imagen_tmax'),
                'extpc': self.cfg.pop('imagen_extpc'),
            }
            self.cfg['imagen'].clear()
            self.cfg['imagen'].update(img)  # mantiene el objeto original


    @try_except
    def on_checkbutton_banderas_multiples_toggled(self, widget):
        if not self.iniciando and not widget.get_active():
            from dialogs import v_pregunta_si_no
            pregunta = '¿Desea limitar a una marca por archivo?'
            aclaración = "Esto dejará una sola marca (la mas baja) por archivo."
            respuesta = v_pregunta_si_no(self.widgets['dialog'], pregunta, aclaración)
            if not respuesta:
                widget.set_active(True)


    def on_checkbutton_imagen_extpc_toggled(self, widget):
        if widget.get_active():
            from dialogs import v_pregunta_si_no
            pregunta = '¿Desea activar el reconocimiento por contenido?'
            aclaración = "Hará mucho mas lenta la carga de carpetas al inicio."
            respuesta = v_pregunta_si_no(self.widgets['dialog'], pregunta, aclaración)
            if not respuesta:
                widget.set_active(False)


    def actualizar_carpeta_callback(self, *args):
        if not self.iniciando:
            self.actualizar_carpeta = True


    def ejecutar(self):
        response = self.widgets['dialog'].run()
        self.widgets['dialog'].hide_on_delete()

        if response == 1:
            from dialogs import v_pregunta_si_no
            pregunta = ('¿Desea eliminar todas las configuraciones, formatos, '
                'keymap y fingerprint?')
            respuesta = v_pregunta_si_no(self.parent, pregunta)
            return 'Borrar' if respuesta else None

        elif response == Gtk.ResponseType.ACCEPT:  # -3
            self.datos('get')
            return 'Actualizar' if self.actualizar_carpeta else True

        else:
            return None

