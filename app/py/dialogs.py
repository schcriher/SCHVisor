#!/usr/bin/env python3
#-*- coding: utf-8 -*-

# Copyright (C) 2013 Cristian Hernán Schmidt
#
# This file is part of SCHVisor.
#
# SCHVisor is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# SCHVisor is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with SCHVisor. If not, see <http://www.gnu.org/licenses/>.

import os
import math

from gi.repository import Gtk, Gdk, GObject

from model import Carpeta, Archivo

PATH, NAME = os.path.split(os.path.abspath(__file__))
BASE, EXT = os.path.splitext(NAME)
GUI = os.path.join(PATH, '..', 'ui', '{}.ui'.format(BASE))
CSS = os.path.join(PATH, '..', 'css', '{}.css'.format(BASE))

FLAGS = Gtk.DialogFlags.MODAL | Gtk.DialogFlags.DESTROY_WITH_PARENT

# Gtk.Grid.attach(child, left, top, width, height)
# Gtk.Box.pack_start(child, expand=True, fill=True, padding=0)



def set_style_context(context):
    provider = Gtk.CssProvider()
    provider.load_from_path(CSS)
    priority = Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION
    context.add_provider(provider, priority)



def plugin_pis(orientation='h', spacing=4, top=0, bottom=0, left=6, right=6):
    "Widget de consulta: Preguntar-Ignorar-Sobrescribir"
    cfg = {'expand': True, 'fill': True, 'padding': 0}
    
    ori = 'HORIZONTAL' if orientation == 'h' else 'VERTICAL'
    box = Gtk.Box(orientation=getattr(Gtk.Orientation, ori))
    box.set_spacing(spacing)
    box.set_margin_top(top)
    box.set_margin_bottom(bottom)
    box.set_margin_left(left)
    box.set_margin_right(right)

    info = Gtk.Label('Cuando se encuentren nombres iguales:')
    box.pack_start(info, **cfg)

    rb_ori = 'VERTICAL' if orientation == 'v' else 'HORIZONTAL'
    rb_box = Gtk.Box(orientation=getattr(Gtk.Orientation, rb_ori))
    rb_box.set_spacing(spacing)
    p = Gtk.RadioButton("Preguntar")
    i = Gtk.RadioButton.new_with_label_from_widget(p, "Ignorar")
    s = Gtk.RadioButton.new_with_label_from_widget(p, "Sobrescribir")
    rb_box.pack_start(p, **cfg)
    rb_box.pack_start(i, **cfg)
    rb_box.pack_start(s, **cfg)
    box.pack_start(rb_box, **cfg)

    def respuesta():
        "Respuesta de PIS"
        if p.get_active():
            return 'preguntar'
        elif i.get_active():
            return 'ignorar'
        else:
            return 'sobrescribir'
    box.get = respuesta

    return box



def plugin_check(mensaje, estado=False, tooltip=None, 
        top=6, bottom=6, left=0, right=0):

    "Widget de consulta: check"
    cfg = {'expand': True, 'fill': True, 'padding': 0}

    box = Gtk.Box()
    box.set_margin_top(top)
    box.set_margin_bottom(bottom)
    box.set_margin_left(left)
    box.set_margin_right(right)

    check = Gtk.CheckButton(mensaje)
    check.set_active(estado)
    if tooltip:
        check.set_tooltip_text(tooltip)
    box.pack_start(check, **cfg)

    def respuesta():
        return check.get_active()
    box.get = respuesta

    return box



# ---------------------------------------------------------------------------- #



def v_información(padre, mensaje, aclaración=None):
    ventana = Gtk.MessageDialog(
        parent = padre,
        flags = FLAGS,
        message_type = Gtk.MessageType.INFO, 
        buttons = Gtk.ButtonsType.OK, 
        message_format = mensaje,
    )
    if aclaración:
        ventana.format_secondary_text(aclaración)
    if os.path.isfile(CSS):
        set_style_context(ventana.get_style_context())
    respuesta = ventana.run()
    ventana.destroy()



def v_pregunta_si_no(padre, mensaje, aclaración=None, widget=None):
    ventana = Gtk.MessageDialog(
        parent = padre,
        flags = FLAGS,
        message_type = Gtk.MessageType.QUESTION, 
        buttons = Gtk.ButtonsType.OK_CANCEL, 
        message_format = mensaje,
    )
    if aclaración:
        ventana.format_secondary_text(aclaración)
    if os.path.isfile(CSS):
        set_style_context(ventana.get_style_context())
    if widget:
        ventana.get_content_area().add(widget)
        ventana.show_all()
    respuesta = ventana.run()
    acción = bool(respuesta == Gtk.ResponseType.OK)
    ventana.destroy()
    return acción



def v_pregunta_carpeta(padre, título, 
        current=None, multiple=False, crear=False, ocultos=False):

    action = 'CREATE_FOLDER' if crear else 'SELECT_FOLDER'
    ventana = Gtk.FileChooserDialog(
        parent = padre,
        title = título, 
        action = getattr(Gtk.FileChooserAction, action),
        buttons = (
            Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL,
            Gtk.STOCK_OPEN, Gtk.ResponseType.OK,
        ),
        flags = FLAGS,
    )
    if current:
        ventana.set_current_folder(current)
    ventana.set_select_multiple(bool(multiple))
    ventana.set_show_hidden(bool(ocultos))
    if os.path.isfile(CSS):
        set_style_context(ventana.get_style_context())
    respuesta = ventana.run()
    if respuesta == Gtk.ResponseType.OK:
        carpeta = ventana.get_filenames() if multiple else ventana.get_filename()
    else:
        carpeta = None
    ventana.destroy()
    return carpeta



# ---------------------------------------------------------------------------- #



class TeclaDialog(Gtk.Dialog):

    def __init__(self, padre, col):
        Gtk.Dialog.__init__(self, 
            parent = padre,
            title = 'Definiendo {}'.format(col),
            flags = FLAGS,
        )
        self.key_sin_efectos = (
            Gdk.KEY_Caps_Lock,
            Gdk.KEY_Num_Lock,
            Gdk.KEY_space,
            Gdk.KEY_Control_L,
            Gdk.KEY_Control_R,
            Gdk.KEY_Alt_L,
            Gdk.KEY_Alt_R,
            Gdk.KEY_Shift_L,
            Gdk.KEY_Shift_R,
            Gdk.KEY_Shift_Lock,
            Gdk.KEY_Super_L,
            Gdk.KEY_Super_R,
            Gdk.KEY_Scroll_Lock,
            Gdk.KEY_ISO_Lock,
            Gdk.KEY_ISO_Level2_Latch,
            Gdk.KEY_ISO_Level3_Latch,
            Gdk.KEY_ISO_Level3_Lock,
            Gdk.KEY_ISO_Level3_Shift,
            Gdk.KEY_ISO_Level5_Latch,
            Gdk.KEY_ISO_Level5_Lock,
            Gdk.KEY_ISO_Level5_Shift,
        )
        self.event = None
        self.set_border_width(26)
        self.set_events(Gdk.EventType.KEY_PRESS)
        self.connect('key-press-event', self.on_window_key_press_event)
        self.label = Gtk.Label('Presione una tecla...\n(Enter para cancelar)')
        self.label.set_justify(Gtk.Justification.CENTER)
        self.get_content_area().add(self.label)
        self.show_all()

    def on_window_key_press_event(self, widget, event):
        if event.keyval in self.key_sin_efectos:
            return True  # stop event
        if not event.keyval == Gdk.KEY_Return:
            self.event = event
        self.destroy()

    def run(self):
        response = super().run()



def v_pregunta_tecla(padre, col):
    ventana = TeclaDialog(padre, col)
    if os.path.isfile(CSS):
        set_style_context(ventana.get_style_context())
    ventana.run()
    ventana.destroy()
    return ventana.event



# ---------------------------------------------------------------------------- #



class ArchivoDialog(Gtk.Dialog):

    def __init__(self, padre, carpeta, nombre,
            iguales=False, destino=None, acción=None, ext_lower=False):

        Gtk.Dialog.__init__(self, 
            parent = padre,
            title = 'Nombre de archivo',
            flags = FLAGS,
            buttons = (
                Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL,
                Gtk.STOCK_OK,     Gtk.ResponseType.OK,
            ),
        )
        if acción is None:
            acción = 'mover/copiar/renombrar'
        self.nombre = nombre
        ext = os.path.splitext(nombre)[1]
        self.ext = ext.lower() if ext_lower else ext
        self.carpeta = carpeta
        self._carpeta = None
        self.iguales = iguales
        self.destino = destino  # ultimo destino elegido

        self.set_border_width(10)
        self.set_default_size(round(0.6 * padre.get_allocated_width(), -2), -1)
        self.box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)

        label = 'Escriba el nuevo nombre para el archivo y/o elija otra carpeta'
        label = Gtk.Label(label)
        label.set_margin_bottom(5)
        self.box.pack_start(label, True, True, 0)
        
        if iguales:
            mod = ('<b>', acción, 'igual', '</b>')
        else:
            mod = ('', acción, 'distinto', '')
        markup = '{}El archivo que quiere {} es {} al existente{}'.format(*mod)
        label = Gtk.Label()
        label.set_markup(markup)
        label.set_margin_bottom(15)
        self.box.pack_start(label, True, True, 0)
        
        hbox = Gtk.Box()
        # NOMBRE
        icon_pos_sec = Gtk.EntryIconPosition.SECONDARY
        self.entry = Gtk.Entry()
        self.entry.set_text(nombre)
        self.entry.set_placeholder_text('Actual: "{}"'.format(nombre))
        self.entry.set_icon_from_stock(icon_pos_sec, Gtk.STOCK_REFRESH)
        self.entry.set_icon_tooltip_text(icon_pos_sec, 'Nombre original')
        self.entry.connect('icon-press', self.nombre_original)
        self.entry.connect('changed', self.validar)
        hbox.pack_start(self.entry, True, True, 0)
        # CARPETA
        boton = Gtk.Button("Carpeta")
        boton.connect("clicked", self.on_carpeta_clicked)
        boton.set_margin_left(5)
        hbox.pack_start(boton, False, False, 1)
        self.box.pack_start(hbox, False, False, 0)

        label = 'Agregar la extensión del archivo automaticamente'
        tooltip = 'Si no está la extensión adecuada se la agrega'
        self.check_ext = Gtk.CheckButton(label)
        self.check_ext.set_tooltip_text(tooltip)
        self.check_ext.set_active(True)
        self.check_ext.set_margin_top(15)
        self.box.pack_start(self.check_ext, False, False, 0)

        label = 'Usar esta misma carpeta si no hay colición de nombres'
        tooltip = 'Para los demas archivos que tengan este problema'
        self.check_car = Gtk.CheckButton(label)
        self.check_car.set_tooltip_text(tooltip)
        self.check_car.set_sensitive(False)
        self.box.pack_start(self.check_car, False, False, 0)

        self.box.set_margin_bottom(5)
        self.get_content_area().add(self.box)
        self.show_all()
        self.validar()

    def nombre_original(self, *widgets):
        self.entry.set_text(self.nombre)

    def on_carpeta_clicked(self, widget):
        self._carpeta = v_pregunta_carpeta(self, 'Elija la carpeta destino', 
            self.destino or self.carpeta)
        tooltip = 'Destino: {}'.format(self._carpeta) if self._carpeta else ''
        self.entry.set_tooltip_text(tooltip)
        self.check_car.set_sensitive(bool(self._carpeta))
        self.validar()

    def validar(self, *widgets):
        nombre = self.entry.get_text()
        is_ext = nombre.lower().endswith(self.ext.lower())
        if self.check_ext.get_active() and not is_ext:
            nombre += self.ext
        carpeta = self._carpeta or self.carpeta
        dirección = os.path.join(carpeta, nombre)
        self._nombre =  None if os.path.exists(dirección) else nombre
        for button in self.get_action_area().get_children():
            if button.get_label() == 'gtk-ok':
                button.set_sensitive(bool(self._nombre))

    def run(self):
        response = super().run()
        if response == Gtk.ResponseType.OK:
            return (self._nombre, self._carpeta, self.check_car.get_active())
        else:
            return (None, None, None)



def v_pregunta_archivo(padre, carpeta, nombre,
        iguales=False, destino=None, acción=None, ext_lower=False):

    var = (padre, carpeta, nombre, iguales, destino, acción, ext_lower)
    ventana = ArchivoDialog(*var)
    if os.path.isfile(CSS):
        set_style_context(ventana.get_style_context())
    respuesta = ventana.run()
    ventana.destroy()
    return respuesta



# ---------------------------------------------------------------------------- #



class WaitDialog(Gtk.Dialog):

    def __init__(self, parent, mensaje, env):
        Gtk.Dialog.__init__(self, 
            parent = parent,
            title = 'Espere',
            flags = FLAGS,
        )
        self.n = None
        self.env = env
        self.cancel = False
        self.fraction = 0

        self.set_border_width(10)
        self.set_default_size(230, -1)
        self.set_resizable(False)
        self.set_deletable(False)
        self.set_destroy_with_parent(True)
        self.connect('key-press-event', self.on_key_press_event)

        vbox = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
        vbox.set_spacing(10)

        box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
        self.label = Gtk.Label(mensaje)
        self.label.set_justify(Gtk.Justification.CENTER)
        box.pack_start(self.label, expand=False, fill=False, padding=0)
        self.sublabel = Gtk.Label()
        self.sublabel.set_justify(Gtk.Justification.CENTER)
        self.sublabel.set_no_show_all(True)
        box.pack_start(self.sublabel, expand=False, fill=False, padding=0)
        vbox.pack_start(box, expand=True, fill=True, padding=0)

        self.bar = Gtk.ProgressBar()
        vbox.pack_start(self.bar, expand=True, fill=True, padding=0)

        btbox = Gtk.ButtonBox()
        self.btcancel = Gtk.Button.new_from_stock(Gtk.STOCK_CANCEL)
        self.btcancel.connect("clicked", self.on_cancel_botton_clicked)
        btbox.pack_start(self.btcancel, expand=True, fill=True, padding=0)
        vbox.pack_start(btbox, expand=True, fill=True, padding=0)

        self.get_content_area().add(vbox)
        self.show_all()
        self.update_id = GObject.timeout_add(250, self.update)

    def update(self):
        # Si el proceso es muy rápido la GUI se congestiona
        self.bar.set_fraction(self.fraction)

        if 'n' in self.env:
            self.n = self.env.pop('n')
            self.bar.set_show_text(True)

        if 'label' in self.env:
            self.label.set_text(self.env.pop('label'))

        if 'sublabel' in self.env:
            self.sublabel.set_text(self.env.pop('sublabel'))
            self.sublabel.show()

        if self.n:
            i = round(self.fraction * self.n)
            self.bar.set_text('{}/{}'.format(i, self.n))

        return True

    def on_key_press_event(self, widget, event):
        return True  # inhibe la cancelación con KEY_Escape

    def on_cancel_botton_clicked(self, *args):
        pregunta = '¿Realmente desea cancelar?'
        aclaración = 'La operación en curso continuará.'
        cancel = v_pregunta_si_no(self, pregunta, aclaración)
        if cancel:
            self.btcancel.set_sensitive(False)
            self.cancel = True
            self.env['task'].stop()
            self.sublabel.set_text('Cancelando...')
            self.sublabel.show()
            GObject.source_remove(self.update_id)
            GObject.timeout_add(200, self.cancelando)

    def cancelando(self):
        if self.env['task'].is_alive():
            return True  # continua esperando
        else:
            self.response(0)



# ---------------------------------------------------------------------------- #



class SalirDialog(Gtk.Dialog):

    def __init__(self, padre, pregunta):
        Gtk.Dialog.__init__(self, 
            parent = padre,
            title = 'Salir',
            flags = FLAGS,
            buttons = (
                Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL,
                Gtk.STOCK_QUIT, Gtk.ResponseType.CLOSE,
            )
        )
        self.set_border_width(15)

        label = Gtk.Label()
        label.set_markup(pregunta)
        label.set_justify(Gtk.Justification.CENTER)
        label.set_margin_bottom(10)

        self.get_content_area().add(label)
        self.show_all()

    def run(self):
        response = super().run()
        return response



def v_pregunta_salir(padre, pregunta):
    ventana = SalirDialog(padre, pregunta)
    if os.path.isfile(CSS):
        set_style_context(ventana.get_style_context())
    respuesta = ventana.run()
    ventana.destroy()
    return respuesta == Gtk.ResponseType.CLOSE



# ---------------------------------------------------------------------------- #



class IrADialog(Gtk.Dialog):

    def __init__(self, padre, rango):
        Gtk.Dialog.__init__(self, 
            parent = padre,
            title = 'Ir a la imagen: número ({}) o nombre'.format(rango),
            flags = FLAGS,
            buttons = (
                Gtk.STOCK_OK,     Gtk.ResponseType.OK,
                Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL,
            )
        )
        self.set_default_response(Gtk.ResponseType.OK)
        self.set_border_width(10)

        self.entry = Gtk.Entry()
        self.get_content_area().add(self.entry)

        ok, cancel = self.get_action_area().get_children()
        ok.set_margin_left(10)
        ok.set_margin_right(20)
        cancel.set_margin_left(20)
        cancel.set_margin_right(10)

        self.show_all()

    def run(self):
        response = super().run()
        if response == Gtk.ResponseType.OK:
            return self.entry.get_text() or None
        else:
            return None



def v_pregunta_ir_a(padre, rango):
    ventana = IrADialog(padre, rango)
    if os.path.isfile(CSS):
        set_style_context(ventana.get_style_context())
    respuesta = ventana.run()
    ventana.destroy()
    return respuesta



# ---------------------------------------------------------------------------- #



class ElegirBanderasDialog(Gtk.Dialog):

    def __init__(self, padre, título, mensaje, banderas, 
            default=False, mb=True, widget=None, borrar_excluye=False):

        Gtk.Dialog.__init__(self, 
            parent = padre,
            title = título,
            flags = FLAGS,
            buttons = (
                Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL,
                Gtk.STOCK_OK,     Gtk.ResponseType.OK,
            ),
        )
        self.set_border_width(10)
        self.check = {}
        self.mb = mb
        self.check_lock = False
        self.borrar_excluye = borrar_excluye

        grid = Gtk.Grid()
        grid.set_column_spacing(10)
        grid.set_margin_bottom(10)

        mensaje = Gtk.Label(mensaje)
        mensaje.set_margin_bottom(15)
        mensaje.set_justify(Gtk.Justification.CENTER)
        grid.attach(mensaje, 0, 0, 2, 1)

        if len(banderas) > 4:
            hbox = Gtk.Box(spacing=6)
            hbox.set_margin_bottom(10)
            todo = Gtk.Button("Todo")
            todo.connect("clicked", self.on_botton_clicked, True)
            hbox.pack_start(todo, True, True, 0)
            nada = Gtk.Button("Nada")
            nada.connect("clicked", self.on_botton_clicked, False)
            hbox.pack_start(nada, True, True, 0)
            grid.attach(hbox, 0, 1, 2, 1)
            siguiente = 2
        else:
            siguiente = 1

        l = list(banderas)
        n2 = len(l) + 1
        n1 = math.floor(n2 / 2)
        for columna, ini, fin in ((0, 0, n1), (1, n1, n2)):
            for i, b in enumerate(l[ini:fin], siguiente):
                n = banderas[b]
                if n:
                    nlabel = ' ({})'.format(n)
                    imagen = 'imagen' if n == 1 else 'imágenes'
                    tooltip = 'Con {} {}'.format(n, imagen)
                else:
                    nlabel = ''
                    tooltip = ''
                label = 'Borrado{}' if b == 10 else 'Grupo {}{{}}'.format(b)
                self.check[b] = Gtk.CheckButton(label.format(nlabel))
                self.check[b].set_active(default)
                self.check[b].set_tooltip_text(tooltip)
                self.check[b].connect("toggled", self.on_check_toggled, b)
                grid.attach(self.check[b], columna, i, 1, 1)

        if widget:
            fila = siguiente + n1 + 1
            grid.attach(widget, 0, fila, 2, 1)

        self.get_content_area().add(grid)
        self.show_all()

    def on_botton_clicked(self, widget, estado):
        for b, check in self.check.items():
            if not estado or not self.borrar_excluye or b != 10:
                check.set_active(estado)

    def on_check_toggled(self, widget, b):
        if self.borrar_excluye and not self.check_lock:
            self.check_lock = True
            if b == 10 and widget.get_active():
                for w in self.check.values():
                    w.set_active(False)
                self.check[10].set_active(True)
            else:
                self.check[10].set_active(False)
            self.check_lock = False

    def run(self):
        response = super().run()
        if response == Gtk.ResponseType.OK:
            return [b for b, widget in self.check.items() if widget.get_active()]



def v_pregunta_elegir_banderas(padre, título, mensaje, banderas, 
        default=False, mb=True, widget=None, borrar_excluye=False):

    var = (padre, título, mensaje, banderas, default, mb, widget, borrar_excluye)
    ventana = ElegirBanderasDialog(*var)
    if os.path.isfile(CSS):
        set_style_context(ventana.get_style_context())
    respuesta = ventana.run()
    ventana.destroy()
    return respuesta



# ---------------------------------------------------------------------------- #



class FormatosDialog(Gtk.Dialog):

    def __init__(self, padre, mensaje, ini=None):
        Gtk.Dialog.__init__(self, 
            parent = padre,
            title = 'Formatos',
            flags = FLAGS,
            buttons = (
                'Borrar',         3,
                Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL,
                Gtk.STOCK_OK,     Gtk.ResponseType.OK,
            ),
        )
        self.set_border_width(10)

        vbox = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
        vbox.set_margin_top(2)
        vbox.set_margin_bottom(16)
        vbox.set_margin_left(10)
        vbox.set_margin_right(10)

        mensaje = Gtk.Label(mensaje)
        mensaje.set_margin_bottom(16)
        mensaje.set_justify(Gtk.Justification.CENTER)
        vbox.pack_start(mensaje, expand=True, fill=True, padding=0)

        grid = Gtk.Grid()
        grid.set_column_spacing(10)

        label = Gtk.Label('Tamaño')
        tooltip = '(0 tamaño original, -1 no se modifica)'
        ancho = Gtk.SpinButton()
        ancho.set_adjustment(Gtk.Adjustment(0, -1, 10000, 10, 100, 0))
        ancho.set_tooltip_text('Ancho en pixeles {}'.format(tooltip))
        xsep = Gtk.Label('X')
        alto = Gtk.SpinButton()
        alto.set_adjustment(Gtk.Adjustment(0, -1, 10000, 10, 100, 0))
        alto.set_tooltip_text('Alto en pixeles {}'.format(tooltip))
        grid.attach(label, left=0, top=0, width=1, height=1)
        grid.attach(ancho, left=1, top=0, width=1, height=1)
        grid.attach(xsep,  left=2, top=0, width=1, height=1)
        grid.attach(alto,  left=3, top=0, width=1, height=1)

        label = Gtk.Label('Giro')
        store = Gtk.ListStore(int, str)
        for g in (0, 90, 180, 270):
            store.append([g, '{} grados (sentido antihorario)'.format(g)])
        render = Gtk.CellRendererText()
        giro = Gtk.ComboBox.new_with_model(store)
        giro.pack_start(render, True)
        giro.add_attribute(render, "text", 1)
        giro.set_id_column(0)
        grid.attach(label, left=0, top=1, width=1, height=1)
        grid.attach(giro,  left=1, top=1, width=3, height=1)

        label = Gtk.Label('Reflejo')
        rh = Gtk.CheckButton('Horizontal')
        rh.set_hexpand(True)
        rv = Gtk.CheckButton('Vertical')
        rv.set_hexpand(True)
        grid.attach(label, left=0, top=2, width=1, height=1)
        grid.attach(rh,    left=1, top=2, width=1, height=1)
        grid.attach(rv,    left=3, top=2, width=1, height=1)

        vbox.pack_start(grid, expand=True, fill=True, padding=0)

        self.objects = {
            'ancho': ancho,
            'alto':  alto,
            'giro':  giro,
            'rh':    rh,
            'rv':    rv,
        }
        ini = ini or {'ancho':-1, 'alto':-1, 'giro':0, 'rh':False, 'rv':False}
        ancho.set_value(ini['ancho'])
        alto.set_value(ini['alto'])
        giro.set_active(ini['giro'] / 90)
        rh.set_active(ini['rh'])
        rv.set_active(ini['rv'])

        self.get_content_area().add(vbox)
        self.show_all()


    def run(self):
        response = super().run()
        if response == Gtk.ResponseType.OK:
            datos = {}
            for var, obj in self.objects.items():
                if var in ('ancho', 'alto'):
                    valor = obj.get_value_as_int()
                elif var == 'giro':
                    valor = obj.get_active() * 90
                else:
                    valor = obj.get_active()
                if var not in ('ancho', 'alto') or valor != -1:
                    datos[var] = valor
            return datos
        elif response == 3:
            return True



def v_pregunta_formatos(padre, mensaje):
    ventana = FormatosDialog(padre, mensaje)
    if os.path.isfile(CSS):
        set_style_context(ventana.get_style_context())
    respuesta = ventana.run()
    ventana.destroy()
    return respuesta



# ---------------------------------------------------------------------------- #



class SimilaresDialog(Gtk.Dialog):

    def __init__(self, parent, objetivo, hdepth):
        Gtk.Dialog.__init__(self, 
            parent = parent,
            title = 'Seteos',
            flags = FLAGS,
            buttons = (
                Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL,
                Gtk.STOCK_OK,     Gtk.ResponseType.OK,
            ),
        )
        self.set_border_width(10)

        vbox = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
        vbox.set_spacing(5)
        vbox.set_margin_bottom(10)

        mensaje = Gtk.Label()
        mensaje.set_markup('<b>Comparador de imágenes</b>')
        mensaje.set_margin_bottom(10)
        vbox.pack_start(mensaje, expand=True, fill=True, padding=0)

        grid = Gtk.Grid()
        grid.set_column_spacing(10)

        label = Gtk.Label('Semejanza')
        grid.attach(label, left=0, top=0, width=1, height=1)

        self.nivel = Gtk.SpinButton()
        self.nivel.set_adjustment(Gtk.Adjustment(90, 0, 100, 1, 10, 0))
        self.nivel.set_tooltip_text('Porcentaje de similitud')
        self.nivel.set_hexpand(True)
        self.nivel.set_snap_to_ticks(True)
        self.nivel.set_value(90)
        grid.attach(self.nivel, left=1, top=0, width=1, height=1)

        label = Gtk.Label('Objetivo')
        grid.attach(label, left=0, top=1, width=1, height=1)

        # Aca por la activacion del combox
        self.archivo_todos = Gtk.CheckButton('Buscar archivo en todas las carpetas')

        self.store = Gtk.ListStore(str)
        for obj in ('Todos', 'Grupos...'):
            self.store.append([obj])
        render = Gtk.CellRendererText()
        self.objetivo = Gtk.ComboBox.new_with_model(model=self.store)
        self.objetivo.pack_start(render, True)
        self.objetivo.add_attribute(render, "text", 0)
        self.objetivo.set_hexpand(True)
        self.objetivo.connect('changed', self.on_objetivo_changed)
        if objetivo:
            self._objetivo = objetivo
            self._dirección = objetivo.dirección
            label = 'Carpeta' if isinstance(objetivo, Carpeta) else 'Archivo'
            self.store.append([label])
            self.objetivo.set_active(2)
            self.objetivo.set_tooltip_text(self._dirección)
        else:
            self.objetivo.set_active(0)
        grid.attach(self.objetivo, left=1, top=1, width=1, height=1)
        vbox.pack_start(grid, expand=True, fill=True, padding=0)

        grid = Gtk.Grid()
        grid.set_column_spacing(5)

        self.fps = Gtk.CheckButton('Huellas')
        self.fps.set_active(True)
        self.fps.set_tooltip_text('Incluir la comparación de las formas')
        grid.attach(self.fps, left=0, top=0, width=1, height=1)
        tooltip = 'Comprobar imágenes rotadas 90, 180 y 270 grados'
        self.rot = Gtk.CheckButton('Incluir rotaciones')
        self.rot.set_tooltip_text(tooltip)
        self.rot.set_active(True)
        grid.attach(self.rot, left=1, top=0, width=1, height=1)

        self.hist = Gtk.CheckButton('Histogramas')
        self.hist.set_tooltip_text('Incluir la comparación de los histogramas')
        grid.attach(self.hist, left=0, top=1, width=1, height=1)
        box = Gtk.Box()
        box.set_sensitive(False)
        box.set_spacing(5)
        self.gris = Gtk.RadioButton('Gris')
        self.gris.set_tooltip_text('{} grises'.format(2 ** hdepth))
        self.color = Gtk.RadioButton.new_with_label_from_widget(self.gris, 'Color')
        self.color.set_tooltip_text('{} colores'.format((2 ** hdepth) ** 3))
        box.pack_start(self.gris, expand=True, fill=True, padding=0)
        box.pack_start(self.color, expand=True, fill=True, padding=0)
        grid.attach(box, left=1, top=1, width=1, height=1)

        self.fps.connect('toggled', self.on_checkbutton_toggled, self.rot, self.hist)
        self.hist.connect('toggled', self.on_checkbutton_toggled, box, self.fps)
        vbox.pack_start(grid, expand=True, fill=True, padding=0)

        if isinstance(objetivo, Archivo):
            tooltip = 'Si no se busca solamente en la carpeta del archivo'
            self.archivo_todos.set_active(True)
            self.archivo_todos.set_tooltip_text(tooltip)
            vbox.pack_start(self.archivo_todos, expand=True, fill=True, padding=0)

        self.get_content_area().add(vbox)
        self.show_all()

    def on_checkbutton_toggled(self, widget, sub_widget, com_widget):
        sub_widget.set_sensitive(widget.get_active())
        if not (widget.get_active() or com_widget.get_active()):
            com_widget.set_active(True)

    def on_objetivo_changed(self, widget):
        index = widget.get_active()
        if index == 1:
            título = 'Marcas'
            mensaje = 'Elija el o los grupos a procesar'
            banderas = {i:0 for i in range(1, 10)}
            from dialogs import v_pregunta_elegir_banderas
            grupos = v_pregunta_elegir_banderas(self, título, mensaje, banderas)
            if grupos:
                g = ','.join(map(str, grupos))
                plural = '' if len(grupos) == 1 else 's'
                tooltip = 'Grupo{0} elegido{0}: {1}'.format(plural, g)
                widget.set_tooltip_text(tooltip)
                self.grupos = grupos
                self.store[1][0] = 'Grupo{}'.format(plural)
            else:
                widget.set_tooltip_text('')
                widget.set_active(0)
                self.grupos = []
            self.archivo_todos.set_sensitive(False)
        else:
            self.store[1][0] = 'Grupos...'
            self.grupos = []
            if index == 2:
                self.objetivo.set_tooltip_text(self._dirección)
                self.archivo_todos.set_sensitive(True)
            else:
                self.objetivo.set_tooltip_text('')
                self.archivo_todos.set_sensitive(False)


    def run(self):
        response = super().run()
        if response == Gtk.ResponseType.OK:
            fps = self.fps.get_active()
            rot = self.rot.get_active()
            hist = self.hist.get_active()
            color = self.color.get_active()
            nivel = self.nivel.get_value_as_int() / 100
            obj = self.objetivo.get_active()
            local = None
            if obj == 0:
                objetivo = 'Todos'
            elif obj == 1:
                objetivo = self.grupos
            else:
                objetivo = self._objetivo
                if isinstance(objetivo, Archivo):
                    local = not self.archivo_todos.get_active()
            return fps, rot, hist, color, nivel, objetivo, local



def v_pregunta_similares(padre, objetivo, hdepth):
    ventana = SimilaresDialog(padre, objetivo, hdepth)
    if os.path.isfile(CSS):
        set_style_context(ventana.get_style_context())
    respuesta = ventana.run()
    ventana.destroy()
    return respuesta



