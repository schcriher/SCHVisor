#!/usr/bin/env python3
#-*- coding: utf-8 -*-

# Copyright (C) 2013 Cristian Hernán Schmidt
#
# This file is part of SCHVisor.
#
# SCHVisor is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# SCHVisor is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with SCHVisor. If not, see <http://www.gnu.org/licenses/>.

import logging
import inspect
import functools

from datetime import datetime
from gi.repository import Gtk, Gdk, GdkPixbuf

from utils import modulo_clase_nombre

FULL = False
METODOS = set()
TIEMPOS = {}
LLAMADAS = 0
PROFUNDIDAD = -1
COLORES_LOG = False

GI_REPR = (Gtk.Widget, Gtk.Adjustment, Gtk.Action, Gtk.TreeStore, Gtk.ListStore,
           Gtk.TreeSelection, Gtk.TreePath, Gdk.EventKey, Gdk.EventButton, 
           GdkPixbuf.Pixbuf)


def _repr(valor):
    if isinstance(valor, GI_REPR) or inspect.isgenerator(valor):
        return '<{}>'.format(valor.__class__.__name__)
    else:
        return repr(valor)


def texto(valores, nombre_ini=''):
    textos = []
    ini, fin = '', ''
    if isinstance(valores, dict):
        for nombre, valor in valores.items():
            textos.append('{}={}'.format(nombre, _repr(valor)))
        if textos:
            ini, fin = '  kwargs:{', '}'
    elif isinstance(valores, (list, tuple)) and not nombre_ini:
        for valor in valores:
            textos.append(_repr(valor))
        if textos:
            ini, fin = '  args:(', ')'
    else:
        textos.append(_repr(valores))
    if nombre_ini:
        ini = '  {}:{}'.format(nombre_ini, ini[-1] if ini else '')
    return '{}{}{}'.format(ini, ', '.join(textos), fin)


def get_margen(char='·', sub_char='└', sub=0):
    global PROFUNDIDAD
    return char * PROFUNDIDAD + sub_char * sub


class Log:

    colores = {
        'sch':       '\033[1;35m{}\033[0m',
        'debug':     '\033[33m{}\033[0m',
        'info':      '\033[36m{}\033[0m',
        'warning':   '\033[1;31m{}\033[0m',
        'error':     '\033[41m\033[1;37m{}\033[0m',
        'exception': '\033[41m\033[1;37m{}\033[0m',
        'critical':  '\033[47m\033[1;31m{}\033[0m',
    } if COLORES_LOG else {}

    aleas = {
        'sch': 'debug',
    }

    def __init__(self, log, **kwargs):
        self.log = log
        self.kwargs = kwargs  # char, sub, sub_char
        self.nombre = None
        # No usar colores si algun log va a un archivo
        for handle in self.log.root.handlers:
            if isinstance(handle, logging.FileHandler):
                self.colores = {}
                break

    def extra(self):
        return {'m': get_margen(**self.kwargs)}  # actulizar dinámicamente

    def logger(self, mensaje, *args):
        mensaje = self.colores.get(self.nombre, '{}').format(mensaje)
        logger = getattr(self.log, self.aleas.get(self.nombre, self.nombre))
        logger(mensaje, *args, extra=self.extra())

    def __getattr__(self, nombre):
        self.nombre = nombre
        return self.logger


log = Log(logging.getLogger(__name__))


def Logging(*args, **kwargs):
    """Formas de uso del decorador
         @Logging
         @Logging('mensaje inicial')
         @Logging('mensaje inicial', 'mensaje final')
         @Logging(excluir={'key':valor})
         @Logging('mensaje inicial', excluir={'key':valor})
         @Logging('mensaje inicial', 'mensaje final', excluir={'key':valor})
         @Logging(timer=True)

         Si en el metodo llamado está presente alguna variable fijada en excluir
         con el valor asignado no se imprimen los log (inicio y final). En
         excluir key tiene la forma 'posicion_variable:nombre_variable:atributo'
         Para omitir alguno dejarlo vacio, ejemplo '0::' (comprueba si el primer
         parámetro pasado al método es igual al valor asigado para no incluir el
         log). Para pasar varios valores como posibles hacerlo con una tupla, si
         el valor necesario es una tupla (entra en conflicto con lo anterior)
         pasarla dentro de una tupla.
         El parámetro timer desactiva por defecto el loggueo, para activarlo hay
         que pasar el parámetro logger=True.
    """
    método = None
    mensaje_ini = []
    mensaje_fin = []
    if args:
        if inspect.isfunction(args[0]):
            método = args[0]
        else:
            mensaje_ini = args[0].split('\n')
            if len(args) > 1:
                mensaje_fin = args[1].split('\n')

    
    timer = kwargs.get('timer', False)
    logger = kwargs.get('logger', not timer)
    excluir = {}
    for k, v in kwargs.get('excluir', {}).items():
        excluir[k] = v if isinstance(v, tuple) else (v,)

    def decorador(método):
        global METODOS
        METODOS.add(método.__name__)

        @functools.wraps(método)
        def _Logging(*args, **kwargs):
            mcn = modulo_clase_nombre(args[0], método)

            global LLAMADAS
            global PROFUNDIDAD
            LLAMADAS += 1
            PROFUNDIDAD += 1

            for key, valor in excluir.items():
                pos, nom, atr = key.split(':')
                try:
                    v = getattr(args[int(pos)], atr) if atr else args[int(pos)]
                    if v in valor:
                        mostrar_log = False
                        break
                except (ValueError, IndexError):
                    pass
                try:
                    v = getattr(kwargs[nom], atr) if atr else kwargs[nom]
                    if v in valor:
                        mostrar_log = False
                        break
                except KeyError:
                    pass
            else:
                mostrar_log = True
                margen = get_margen()
                offset = - (len(margen) + len(str(LLAMADAS)) - 1)
                if FULL:
                    offset += 62
                    quien = mcn
                else:
                    offset += 52
                    quien = método.__name__
                p = '{}{{}} {:<{o}}{{}}'.format(LLAMADAS, quien, o=offset)

            mostrar_log = mostrar_log if logger else False

            if mostrar_log:
                for m in mensaje_ini:
                    log.debug(m)
                var = (texto(args), texto(kwargs))
                log.debug(p.format('i', '{}{}'.format(*var)))

            ini = datetime.now()
            retorno = método(*args, **kwargs)  # Ejecución del método
            fin = datetime.now()
            dt = fin - ini

            if timer:
                TIEMPOS.setdefault(mcn, []).append(dt.total_seconds())

            if mostrar_log:
                var = (str(dt), texto(retorno, 'return'))
                log.debug(p.format('f', '  {}{}'.format(*var)))
                for m in mensaje_fin:
                    log.debug(m)

            PROFUNDIDAD -= 1
            return retorno
        return _Logging

    if método:
        return decorador(método)  # decorador llamado sin argumentos
    else:
        return decorador          # decorador llamado con argumentos
