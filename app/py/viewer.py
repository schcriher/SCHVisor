#!/usr/bin/env python3
#-*- coding: utf-8 -*-
#
# Copyright (C) 2013 Cristian Hernán Schmidt
#
# This file is part of SCHVisor.
#
# SCHVisor is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# SCHVisor is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with SCHVisor. If not, see <http://www.gnu.org/licenses/>.

import os
import sys
import json
import html
import time
import filecmp
import logging
import functools
import subprocess
import collections

from copy import copy
from datetime import datetime
from itertools import combinations, product
from gi.repository import Gtk, Gdk, GdkPixbuf, GObject

from debug import Log, Logging
from utils import (FLAGS, NO_MASK, CONTROL, SHIFT, SUPER, WaitTask, Sentinela,
    ncombinaciones, borseg_tipos, try_except, fborrar, frenombrar, normalizar,
    md5sum, unidad_a_multiplo, color, texdiff, maskkey_to_hotkey, hotkey_to_maskkey)
from model import (Archivo, Carpeta, Carpetas, Banderas, Formatos, Fingerprint,
    Direcciones, Configuración)

GObject.threads_init()

PATH, NAME = os.path.split(os.path.abspath(__file__))
BASE, EXT = os.path.splitext(NAME)
GUI = os.path.join(PATH, '..', 'ui', '{}.ui'.format(BASE))
CSS = os.path.join(PATH, '..', 'css', '{}.css'.format(BASE))

log = Log(logging.getLogger(__name__), sub=1)



class ArchivoCfg:

    def __init__(self, base, nombre):
        self.base = base
        self.nombre = nombre

    def __repr__(self):
        return repr(self.path)

    def __str__(self):
        return self.path

    @property
    def path(self):
        return os.path.join(self.base.carpeta, self.nombre)



class GetCfg:

    log = log

    def __init__(self, carpeta, cfg_dir):
        self.logging = None
        self.carpeta = None
        self.cfg_dir = os.path.abspath(cfg_dir)
        self.archivos = {}
        self.set_carpeta(carpeta, None)

    def __repr__(self):
        return '<{}:{}>'.format(self.__class__.__name__, self.carpeta)

    def __call__(self, nombre):
        archivo = ArchivoCfg(self, nombre)
        self.archivos[nombre] = archivo
        if 'log' in nombre:
            self.logging = archivo
        return archivo

    def set_carpeta(self, carpeta, borseg):
        if not os.path.isdir(carpeta):
            os.makedirs(carpeta)

        for archivo in self.archivos.values():
            path = os.path.join(carpeta, archivo.nombre)
            método, comando = frenombrar(archivo.path, path, borseg)
            var = (método, comando, archivo.path, path)
            self.log.info('Se renombró [%s%s]: %s -> %s', *var)

        if self.carpeta:
            if not len(os.listdir(self.carpeta)):
                os.rmdir(self.carpeta)
                self.log.info('Se eliminó [normal]: %s', self.carpeta)

        self.carpeta = os.path.abspath(carpeta)
        with open(self.cfg_dir, mode='w', encoding='utf-8') as fid:
            fid.write(carpeta)

        if self.logging:
            self.set_logging()
        os.system('sync')

    def set_logging(self):
        for handle in logging.root.handlers:
            if isinstance(handle, logging.FileHandler):
                nombre = os.path.basename(handle.baseFilename)
                if nombre == self.logging.nombre:
                    handle.acquire()
                    handle.close()
                    handle.baseFilename = self.logging.path
                    handle.release()

    def borrar(self, borseg, nombre=None):
        archivos = [self.archivos[nombre]] if nombre else self.archivos.values()
        for archivo in archivos:
            try:
                método, comando = fborrar(archivo.path, borseg)
                self.log.info('Se eliminó [%s%s]: %s', método, comando, archivo.path)
            except OSError:
                pass



class Visor(WaitTask):

    log = log
    wait_task = WaitTask.wait_task  # decorador

    #@Logging
    def __init__(self, version, getcfg, ini):
        self.log.info('Inicio de SCHVisor %s', version)

        self.hotkey = True
        self.sleepdog = False
        self.adjustment = True
        self.panel_carpetas = None
        self.movcop_destino = None
        self.normalizar_destino = None

        builder = Gtk.Builder()
        builder.add_from_file(GUI)
        builder.connect_signals(self)
        objects = (
            'window',
            'grid',
            'label_banderas',
            'label_informacion',
            'button_bandera', 
            'button_bandera_image',
            #
            'imagen_scrolledwindow',
            'imagen_viewport',
            'imagen_adjustment_horizontal',
            'imagen_adjustment_vertical',
            'imagen',
            #
            'carpetas_scrolledwindow',
            'carpetas_treeview',
            'carpetas_selection',
            'carpetas_liststore',
            #
            'menu',
            'menu_contextual_imagen',
            'menu_contextual_carpetas',
            #
            'menuitem_limpiar',
            'menuitem_imagen_limpiar',
            'menuitem_carpetas_limpiar',
            'menuitem_imagen_abrir_gimp',
            'menuitem_similares',
            'menuitem_imagen_similares',
            'menuitem_carpetas_similares',
            #
            'actiongroup_menu_imagenes',
            'actiongroup_menu_contextual_imagen',
            'actiongroup_menu_contextual_carpetas_imagenes',
            #
            'action_movcop',
            'action_limpiar',
            'action_borrar',
            'action_normalizar',
            'action_olvidar',
            'action_freset',
            'action_modificados',
            'action_lisneg',
            'action_carpetas_quitar',
            'action_similares',
            'action_imagen_similares',
            'action_carpetas_similares',
        )
        self.widgets = {obj: builder.get_object(obj) for obj in objects}

        if os.path.isfile(CSS):
            screen = Gdk.Screen.get_default()
            provider = Gtk.CssProvider()
            provider.load_from_path(CSS)
            priority = Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION
            context = Gtk.StyleContext()
            context.add_provider_for_screen(screen, provider, priority)

        self.clipboard = Gtk.Clipboard.get(Gdk.SELECTION_CLIPBOARD)
        self.buffer = {}
        self.getcfg = getcfg

        self.cfg = Configuración(version, getcfg, self.widgets)

        self.keymap_archivo = getcfg('keymap')
        self.keymap_gen_mask_key_acción()
        self.keymap_gen_acción_método()
        if self.cfg['ventana_cargar_guardar_keymap']:
            self.keymap_cargar()

        self.paths = Direcciones(getcfg)

        self.banderas = Banderas()
        icon = (Gtk.STOCK_DELETE, Gtk.IconSize.MENU)
        self.bandera_borrar = Gtk.Image.new_from_icon_name(*icon)
        self.bandera_grupos = Gtk.Label()
        self.bandera_vacio = Gtk.Label()
        label_context = self.widgets['label_informacion'].get_style_context()
        self.label_color_normal = label_context.get_color(Gtk.StateFlags.NORMAL)

        self.formatos = Formatos(getcfg)
        if self.cfg['ventana_cargar_guardar_frm']:
            self.formatos.cargar()

        self.fph = Fingerprint(getcfg)
        if self.cfg['ventana_cargar_guardar_fph']:
            self.fph.cargar()

        self.widgets['window'].resize(*self.cfg['ventana_tamaño'])
        if (-1, -1) != tuple(self.cfg['ventana_posición']):
            self.widgets['window'].move(*self.cfg['ventana_posición'])

        if self.cfg['ventana_iniciar_estado'] == 1:
            self.maximize(True)
        elif self.cfg['ventana_iniciar_estado'] == 2:
            self.fullscreen(True)
        else:
            self.maximize(self.cfg['ventana_maximized'])
            self.fullscreen(self.cfg['ventana_fullscreen'])

        self.ca = None  # carpeta activa
        self._ica = None  # índice de la carpeta activa
        self.carpetas = Carpetas(self.dog)
        self.carpetas_activar_panel(self.cfg['carpetas_activar_panel'])
        ini = os.path.dirname(ini) if os.path.isfile(ini or '') else ini
        agregar = (os.path.basename(ini), ini) if ini else self.paths.carpetas
        self.agregar_carpetas(agregar, recordar=False)
        self.actualizar()
        self.widgets['window'].show_all()
        self.widgets['imagen'].clear()
        self.set_carpeta(setter=0)
        self.log.info('SCHVisor cargado con %s imágenes', len(self))


    #@Logging
    def __repr__(self):
        return '<{}>'.format(self.__class__.__name__)


    #@Logging
    def __len__(self):
        return sum(len(archivos) for archivos in self.carpetas)


    #@Logging
    def run(self):
        Gtk.main()


    #@Logging
    def archivos_eliminados(self, archivos):
        for archivo in archivos:
            self.banderas.unset(archivo)
            self.formatos.unset(archivo)


    #@Logging
    def dog(self, carpeta):
        if not self.sleepdog:
            ret = carpeta.actualizar()
            if ret:
                self.archivos_eliminados(ret[0])
                # No es necesario forzar, los cambios en una imagen 
                # no se perciben en la fecha de modificacion de su
                # carpeta, así se evita una duplicacion de trabajo,
                # sin embargo si se actualiza la información.
                self.set_imagen(forzar=False)
        return True


    #@Logging
    @wait_task('Actualizando carpetas')
    def actualizar_carpetas(self, key, forzar=False):
        self.wait_env[key]['data'] = {
            'carpetas': self.carpetas,
            'forzar': forzar,
            'eliminados': [],
            'nuevos': [],
        }


    #@Logging
    def actualizar_carpetas_thread(self, key):
        data = self.wait_env[key]['data']
        n = len(data['carpetas'])
        for i, carpeta in enumerate(data['carpetas'], 1):
            ret = carpeta.actualizar(data['forzar'])
            if ret:
                data['eliminados'].extend(ret[0])
                data['nuevos'].extend(ret[1])
            yield i / n


    #@Logging
    def actualizar_carpetas_complete(self, key):
        data = self.wait_env[key].pop('data')
        self.archivos_eliminados(data['eliminados'])


    #@Logging
    def actualizar(self):
        try:
            borseg = self.cfg['imagen_borseg']
            if borseg:
                tipo = borseg_tipos.get((borseg - 1) // 4 + 1)
                if tipo:
                    nivel = borseg % 4 or 4
                else:
                    tipo = nivel = '<ERROR>'
                title = ' [Borrado {} nivel {}]'.format(tipo, nivel)
            else:
                title = ''
            self.widgets['window'].set_title('SCHVisor{}'.format(title))

            self.keymap_gen_acción_método()

            b_unset = color(self.cfg['banderas_color_unset'], 'html')
            b_set = color(self.cfg['banderas_color_set'], 'html')
            self.banderas.colores = (b_unset, b_set)
            self.banderas.set_multiple(self.cfg['banderas_multiples'])

            self.carpetas.set_timeout(self.cfg['carpetas_watchdog'])
            self.decoración(activar=self.cfg['ventana_decoración'])

            if self.cfg['banderas_multiples']:
                ancho = self.cfg['banderas_boton_ancho']
            else:
                ancho = 32
            tamaños = (
                ('button_bandera',          ancho, -1),
                ('label_banderas',          self.cfg['carpetas_panel_ancho'] + 4, -1),
                ('carpetas_scrolledwindow', self.cfg['carpetas_panel_ancho'], -1),
            )
            for widget, ancho, alto in tamaños:
                self.widgets[widget].set_size_request(ancho, alto)

            # CORRECCION:
            #self.size = abs(int(size / 40) * 40) or 160
            #self.hdepth = abs(int(hdepth)) or 2
            self.fph.set(*self.cfg['fingerprint_size', 'fingerprint_hdepth'])

            if self.cfg['gimp']:
                self.widgets['menuitem_imagen_abrir_gimp'].show()
            else:
                self.widgets['menuitem_imagen_abrir_gimp'].hide()

            if self.cfg['mat']:
                self.widgets['menuitem_limpiar'].show()
                self.widgets['menuitem_imagen_limpiar'].show()
                self.widgets['menuitem_carpetas_limpiar'].show()
            else:
                self.widgets['menuitem_limpiar'].hide()
                self.widgets['menuitem_imagen_limpiar'].hide()
                self.widgets['menuitem_carpetas_limpiar'].hide()

            if self.cfg['convert']:
                self.widgets['menuitem_similares'].show()
                self.widgets['menuitem_imagen_similares'].show()
                self.widgets['menuitem_carpetas_similares'].show()
            else:
                self.widgets['menuitem_similares'].hide()
                self.widgets['menuitem_imagen_similares'].hide()
                self.widgets['menuitem_carpetas_similares'].hide()
        except:
            self.log.exception('Archivo %s', self.cfg.archivo)
            exctype, value = sys.exc_info()[:2]
            error = '{}: {}'.format(exctype.__name__, value)
            from dialogs import v_información
            mjs = ('Ocurrio un error al cargar la configuración, '
                'el archivo será eliminado, vea los logs para mas detalles. '
                'Se resetea la configuracion. \n{}'.format(error))
            v_información(self.widgets['window'], 'ERROR', mjs)
            # En la configuración no importa el borrado seguro
            # No contiene datos sensibles
            if os.path.exists(self.cfg.archivo.path):
                os.remove(self.cfg.archivo.path)
            self.cfg.reset()
            self.actualizar()


    #@Logging
    def salir(self):
        if not self.cfg['ventana_salir_preguntar']:  # Nunca
            salir = True
        else:
            marcas = len(self.banderas.get_activas())
            preg_siempre = self.cfg['ventana_salir_preguntar'] == 2
            if marcas or preg_siempre:
                pregunta = ''
                if marcas:
                    var = (marcas, 's' if marcas > 1 else '')
                    pregunta += 'Hay {} marca{} sin procesar.\n'.format(*var)
                pregunta += '¿Realmente desea salir?'
                from dialogs import v_pregunta_salir
                salir = v_pregunta_salir(self.widgets['window'], pregunta)
            else:
                salir = True
        if salir:
            try:
                self.carpetas.unset_all_watchdogs()
                self.cfg.guardar()
                if self.cfg['ventana_cargar_guardar_keymap']:
                    self.keymap_guardar()
                self.paths.guardar()
                if self.cfg['ventana_cargar_guardar_frm']:
                    self.formatos.guardar()
                if self.cfg['ventana_cargar_guardar_fph']:
                    self.fph.guardar()
            except:
                self.log.exception('Archivo %s', self.cfg.archivo)
                exctype, value = sys.exc_info()[:2]
                error = '{}: {}'.format(exctype.__name__, value)
                from dialogs import v_información
                mjs = ('Ocurrio un error al guardar las configuraciones y datos, '
                    'vea el archivo de logs para mas detalles. \n'
                    '{} '.format(error))
                v_información(self.widgets['window'], 'ERROR', mjs)
            os.system('sync')
            self.log.info('Finalización de SCHVisor')
            Gtk.main_quit()
        else:
            return not salir


    #@Logging
    @try_except
    def on_window_delete_event(self, widget, event):
        return self.salir()


    #@Logging
    @try_except
    def on_window_window_state_event(self, widget, event):
        state = event.new_window_state
        self.cfg['ventana_maximized'] = bool(state & Gdk.WindowState.MAXIMIZED)
        self.cfg['ventana_fullscreen'] = bool(state & Gdk.WindowState.FULLSCREEN)


    #@Logging
    def fullscreen(self, activar=None):
        if activar is None:
            activar = not self.cfg['ventana_fullscreen']
        if activar:
            self.widgets['window'].fullscreen()
        else:
            self.widgets['window'].unfullscreen()
        self.cfg['ventana_fullscreen'] = activar


    #@Logging
    def maximize(self, activar=None):
        if not self.cfg['ventana_fullscreen']:
            if activar is None:
                activar = not self.cfg['ventana_maximized']
            if activar:
                self.widgets['window'].maximize()
            else:
                self.widgets['window'].unmaximize()
            self.cfg['ventana_maximized'] = activar


    #@Logging
    def decoración(self, activar=None):
        if not self.cfg['ventana_fullscreen']:
            if activar is None:
                activar = not self.cfg['ventana_decoración']
            self.cfg['ventana_decoración'] = activar
            self.widgets['window'].set_decorated(activar)


    #@Logging
    def carpetas_activar_panel(self, activar=None):
        if activar is None:
            activar = not self.panel_carpetas

        ancho_w_antes = self.widgets['imagen_scrolledwindow'].get_allocated_width()
        ancho_s_antes = self.widgets['imagen_adjustment_horizontal'].get_page_size()
        diff_antes = ancho_w_antes - ancho_s_antes

        setter = self.widgets['grid'].child_set_property
        if activar:
            setter(self.widgets['imagen_scrolledwindow'], 'left-attach', 1)
            setter(self.widgets['imagen_scrolledwindow'], 'width', 1)
            self.widgets['carpetas_scrolledwindow'].show()
            self.panel_carpetas = True
        else:
            self.widgets['carpetas_scrolledwindow'].hide()
            setter(self.widgets['imagen_scrolledwindow'], 'left-attach', 0)
            setter(self.widgets['imagen_scrolledwindow'], 'width', 2)
            self.panel_carpetas = False
        self.widgets['grid'].resize_children()

        ancho_w_despues = self.widgets['imagen_scrolledwindow'].get_allocated_width()
        ancho_s_despues = self.widgets['imagen_adjustment_horizontal'].get_page_size()
        diff_despues = ancho_w_despues - ancho_s_despues
        if self.cfg['imagen_scrolled_correccion']:
            offset_diff = (diff_antes - diff_despues) / 2
        else:
            offset_diff = 0
        offset = (ancho_w_antes - ancho_w_despues) / 2 + offset_diff
        if self.hay_archivo_activo and offset != 0:
            ajuste = self.widgets['imagen_adjustment_horizontal'].get_value()
            self.adjustment = False
            self.widgets['imagen_adjustment_horizontal'].set_value(ajuste + offset)
            self.adjustment = True


    # ------------------------------------------------------------------------ #


    #@Logging
    @wait_task('Agregando carpetas')
    def agregar_carpetas(self, key, carpetas, recordar=True):
        if not carpetas:
            return True  # stop
        self.wait_env[key]['data'] = {
            'carpetas': carpetas, 
            'recordar': recordar,
            'errores': [],
        }


    #@Logging
    def agregar_carpetas_thread(self, key):
        data = self.wait_env[key]['data']
        n = len(data['carpetas'])
        for i, (nombre, obj) in enumerate(data['carpetas'], 1):
            try:
                if isinstance(obj, Carpeta):
                    carpeta = obj
                    carpeta._mfecha = None  # forzar la actualización
                else:
                    carpeta = Carpeta(obj, self.cfg['imagen'], self.paths.lisneg)
                    carpeta.listar()
                if carpeta.tamaño or self.cfg['carpetas_tamaño_cero']:
                    fila = [nombre, html.escape(carpeta.dirección)]
                    self.widgets['carpetas_liststore'].append(fila)
                    self.carpetas.agregar(carpeta)
                    if data['recordar']:
                        self.paths.add_carpeta(*fila)
            except:
                exctype, value = sys.exc_info()[:2]
                mjs = 'Error cargando la carpeta'
                mjs += ' ' if str(obj) in str(value) else ' "{}" '.format(obj)
                mjs += 'de la configuración. Será ignorada.'
                self.log.exception(mjs)
                data['errores'].append(str(obj))
            yield i / n
        yield 1


    #@Logging
    def agregar_carpetas_complete(self, key):
        data = self.wait_env[key].pop('data')
        n = len(data['errores'])
        if n:
            var = (n, '', 'fue') if n == 1 else (n, 's', 'fueron')
            mjs = 'Error cargando {0} carpeta{1}, {2} ignorada{1}. '.format(*var)
            mjs += 'Vea el archivo de logs para mas detalles.'
            from dialogs import v_información
            v_información(self.widgets['window'], 'ERROR', mjs)
            for dirección in data['errores']:
                self.paths.remove_carpeta(dirección)


    #@Logging
    def agregar_nuevas_carpetas(self, carpetas):
        n = len(carpetas)
        var = ('s', 'agregaron', n) if n > 1 else ('', 'agregó', n)
        from dialogs import v_pregunta_si_no
        respuesta = v_pregunta_si_no(self.widgets['window'],
            '¿Desea agregar la{0} carpeta{0} nueva{0}?'.format(var[0]),
            'En los destinos se {1} {2} carpeta{0}'.format(*var))
        if respuesta:
            agregar = [(c.nombre, c) for c in sorted(carpetas, key=normalizar)]
            self.agregar_carpetas(agregar)


    #@Logging
    def set_carpeta(self, offset=0, overflow='auto', setter=None):
        "Activa una carpeta"
        n = len(self.carpetas) - 1
        if n >= 0:
            if isinstance(setter, int):
                i = setter
            elif isinstance(setter, (str, Carpeta, Archivo)):
                carpeta = setter.carpeta if isinstance(setter, Archivo) else setter
                i = self.carpetas.index(carpeta)
            else:
                if offset == 'ini':
                    i = 0
                elif offset == 'fin':
                    i = n
                else:
                    modelo, iterador = self.widgets['carpetas_selection'].get_selected()
                    if iterador != None:
                        i = modelo[iterador].path.get_indices()[0] + int(offset)
                    else:
                        raise ValueError('offset=num debe haber una carpeta seleccionada')
            if not (0 <= i <= n):
                if overflow == 'ini':
                    i = 0
                elif overflow == 'fin':
                    i = n
                elif overflow == 'auto' and isinstance(setter, int):
                    i = 0 if setter <= 0 else n
                elif overflow == 'auto' and isinstance(offset, int):
                    i = 0 if offset <= 0 else n
                else:
                    e = '0 <= i({}) <= n({}), offset={}, overflow={}'
                    raise OverflowError(e.format(i, n, offset, overflow))
            if i != self.i:
                it = self.widgets['carpetas_liststore'][i].iter
                self.widgets['carpetas_selection'].select_iter(it)
                return True
        else:
            self.i = None


    #@Logging
    @try_except
    def on_carpetas_selection_changed(self, widget):
        modelo, iterador = widget.get_selected()
        self.i = modelo[iterador].path.get_indices()[0] if iterador != None else None
        self.set_imagen()


    @property
    def i(self):
        return self._ica


    @i.setter
    def i(self, i):
        if i in self.carpetas:
            i = self.carpetas.index(i)
        if isinstance(i, int) and 0 <= i < len(self.carpetas):
            self.ca = self.carpetas[i]
            self._ica = i
        else:
            self.ca = None
            self._ica = None


    @property
    def baa(self):
        "Devuelve una lista con las banderas del archivo activo"
        if self.hay_archivo_activo:
            return self.banderas.get(self.ca.aa, [], lista=True)
        else:
            return []


    @property
    def hay_archivo_activo(self):
        return bool(self.ca and self.ca.aa)


    @property
    def hay_carpetas(self):
        return bool(self.carpetas)


    #@Logging
    def set_imagen(self, offset=0, overflow='auto', setter=None, forzar=False):
        "Activa y cargar al buffer una imagen, ademas de actualizar la pantalla"
        if self.ca:
            self.ca.set(offset, overflow, setter)

        if self.hay_archivo_activo:
            cambió_archivo = self.ca.aa != self.buffer.get('archivo')
            cambió_mtepoch = self.ca.aa.mtepoch != self.buffer.get('mtepoch')

        if self.hay_archivo_activo and (cambió_archivo or cambió_mtepoch or forzar):
            try:
                pixbuf = GdkPixbuf.Pixbuf.new_from_file(self.ca.aa.dirección)
                if self.cfg['imagen_aplicar_orientación']:
                    pixbuf = pixbuf.apply_embedded_orientation()

                self.buffer['archivo'] = self.ca.aa
                self.buffer['mtepoch'] = self.ca.aa.mtepoch
                self.buffer['pixbuf'] = pixbuf
                self.buffer['ancho'] = pixbuf.get_width()
                self.buffer['alto'] = pixbuf.get_height()
                self.buffer['rel'] = self.buffer['ancho'] / self.buffer['alto']

                self.set_zoom()

            except:
                dirección = self.ca.aa.dirección
                exctype, value = sys.exc_info()[:2]
                mjs = 'Error cargando la imagen'
                mjs += '. ' if dirección in str(value) else ' "{}". '.format(dirección)
                mjs += 'Será agregada a la lista negra.'
                self.log.exception(mjs)
                mjs += '\n\n{}: {}'.format(exctype.__name__, value)
                from dialogs import v_información
                v_información(self.widgets['window'], 'ERROR', mjs)
                self.paths.add_lisneg(dirección)
                self.banderas.unset(self.ca.aa)
                self.formatos.unset(self.ca.aa)
                self.ca.eliminar(self.ca.aa)
                return self.set_imagen(offset, overflow, setter, forzar)

        elif not self.ca or not self.ca.aa:
            self.widgets['imagen'].clear()
            self.buffer['archivo'] = None
            self.buffer['mtepoch'] = None
            self.buffer['pixbuf'] = None
            self.buffer['ancho'] = None
            self.buffer['alto'] = None
            self.buffer['rel'] = None

        self.mostrar_información()
        self.mostrar_banderas()


    #@Logging
    def mostrar_información(self):
        if self.hay_archivo_activo:
            información = '{}/{}:   {}{}{}'.format(
                self.ca.i + 1,
                self.ca.tamaño,
                self.ca.aa.nombre,
                '   {}'.format(unidad_a_multiplo(self.ca.aa.tamaño))
                    if self.cfg['imagen_mostrar_tamaño'] else '',
                '   {}'.format(self.ca.aa.mfecha(self.cfg['imagen_formato_fecha']))
                    if self.cfg['imagen_mostrar_fecha'] else '',
            )
            tooltip = self.ca.aa.dirección
        elif not self.ca or not self.ca.aa:
            if self.carpetas:
                información = 'Sin imágenes en esta carpeta'
            else:
                información = 'Cargue una carpeta'
            tooltip = ''
        else:
            return False
        self.widgets['label_informacion'].set_text(información)
        self.widgets['label_informacion'].set_tooltip_text(tooltip)


    #@Logging
    def set_zoom(self, factor=None):
        # Datos con giro
        fimagen = self.formatos.get(self.ca.aa)
        if factor is 1:
            fimagen.reset()

        if fimagen.is_zoom():
            ancho = fimagen.ancho
            alto = fimagen.alto
            rel = fimagen.rel
        else:
            ancho_w = self.widgets['imagen_scrolledwindow'].get_allocated_width()
            alto_w = self.widgets['imagen_scrolledwindow'].get_allocated_height()
            self.log.sch('ancho_w=%s, alto_w=%s', ancho_w, alto_w)
            
            if fimagen.is_giro_90():
                ancho_w, alto_w = alto_w, ancho_w

            f_ancho = self.buffer['ancho'] / ancho_w
            f_alto = self.buffer['alto'] / alto_w
            rel = self.buffer['rel']
            if f_ancho > f_alto:
                ancho = ancho_w
                alto = round(ancho / rel)
            else:
                alto = alto_w
                ancho = round(alto * rel)

            if fimagen.is_giro_90():
                ancho, alto = alto, ancho
                rel = 1 / rel

        if isinstance(factor, float):
            ancho = round(ancho * factor)
            alto = round(ancho / rel)
            zmin = self.cfg['imagen_zoom_min']
            zmax = self.cfg['imagen_zoom_max']
            if not ((zmin <= ancho <= zmax) and (zmin <= alto <= zmax)):
                return False
            self.formatos.set(self.ca.aa, ancho, alto, rel)

        self.log.sch('ancho=%s, alto=%s', ancho, alto)

        if fimagen.is_giro_90():  # make_pixbuf necesita ancho y alto sin giro
            ancho, alto = alto, ancho
        pixbuf = self.make_pixbuf(ancho, alto, *fimagen['giro', 'rh', 'rv'])
        self.widgets['imagen'].set_from_pixbuf(pixbuf)
        self.set_alineación()


    #@Logging
    def make_pixbuf(self, ancho, alto, giro=None, rh=None, rv=None):
        "Crea pixbuf a base del buffer con las transformaciones (ancho y alto sin giro)"
        pixbuf = GdkPixbuf.Pixbuf.new(GdkPixbuf.Colorspace.RGB, True, 8, ancho, alto)
        c = GdkPixbuf.InterpType(self.cfg['imagen_calidad'])
        f = ancho / self.buffer['ancho']
        self.buffer['pixbuf'].scale(pixbuf, 0, 0, ancho, alto, 0, 0, f, f, c)

        if giro:
            pixbuf = pixbuf.rotate_simple(GdkPixbuf.PixbufRotation(giro))
        if rh:
            pixbuf = pixbuf.flip(True)
        if rv:
            pixbuf = pixbuf.flip(False)

        return pixbuf


    #@Logging
    def set_alineación(self, offset_h=0, offset_v=0):
        self.adjustment = False
        ancho_w = self.widgets['imagen_scrolledwindow'].get_allocated_width()
        alto_w = self.widgets['imagen_scrolledwindow'].get_allocated_height()
        pixbuf = self.widgets['imagen'].get_pixbuf()
        fimagen = self.formatos.get(self.ca.aa)

        ancho = pixbuf.get_width()
        alto = pixbuf.get_height()
        ajuste_h = fimagen.fh * ancho - ancho_w / 2 + offset_h
        ajuste_v = fimagen.fv * alto - alto_w / 2 + offset_v

        self.log.sch('ajuste_h=%s, ajuste_v=%s', ajuste_h, ajuste_v)
        self.log.sch('ancho-ancho_w=%s, alto-alto_w=%s', ancho-ancho_w, alto-alto_w)

        if offset_h:
            fh = (ajuste_h + ancho_w / 2) / ancho
            self.formatos.set(self.ca.aa, fh=fh)
        if offset_v:
            fv = (ajuste_v + alto_w / 2) / alto
            self.formatos.set(self.ca.aa, fv=fv)

        if self.cfg['imagen_scrolled_correccion']:
            self.widgets['imagen_scrolledwindow'].resize_children()
            diff_h = ancho_w - self.widgets['imagen_adjustment_horizontal'].get_page_size()
            diff_v = alto_w - self.widgets['imagen_adjustment_vertical'].get_page_size()
            ajuste_h += diff_h / 2
            ajuste_v += diff_v / 2
            self.log.sch('diff_h=%s, diff_v=%s', diff_h, diff_v)

        if ancho > ancho_w:
            self.widgets['imagen_adjustment_horizontal'].set_upper(ancho)
            self.widgets['imagen_adjustment_horizontal'].set_value(ajuste_h)
        if alto > alto_w:
            self.widgets['imagen_adjustment_vertical'].set_upper(alto)
            self.widgets['imagen_adjustment_vertical'].set_value(ajuste_v)
        self.adjustment = True


    #@Logging(excluir={'0::adjustment':False})
    def on_imagen_adjustment_horizontal_value_changed(self, widget):
        pixbuf = self.widgets['imagen'].get_pixbuf()
        if self.adjustment and pixbuf:
            ancho_w = self.widgets['imagen_scrolledwindow'].get_allocated_width()
            ancho = pixbuf.get_width()
            if ancho > ancho_w:
                fh = (widget.get_value() + ancho_w / 2) / ancho
                self.formatos.set(self.ca.aa, fh=fh)
                self.log.sch('fh=%s', fh)


    #@Logging(excluir={'0::adjustment':False})
    def on_imagen_adjustment_vertical_value_changed(self, widget):
        pixbuf = self.widgets['imagen'].get_pixbuf()
        if self.adjustment and pixbuf:
            alto_w = self.widgets['imagen_scrolledwindow'].get_allocated_height()
            alto = pixbuf.get_height()
            if alto > alto_w:
                fv = (widget.get_value() + alto_w / 2) / alto
                self.formatos.set(self.ca.aa, fv=fv)
                self.log.sch('fv=%s', fv)


    #@Logging
    def on_button_bandera_clicked(self, widget):
        self.banderas.unset(self.ca.aa, self.baa)
        self.mostrar_banderas()


    #@Logging
    def set_bandera(self, bandera):
        "Setea una bandera, pero si ya está la quita"
        if bandera in self.baa:
            self.banderas.unset(self.ca.aa, bandera)
        else:
            if bandera == 10:
                self.banderas.set(self.ca.aa, 10, unicos=True)
            else:
                self.banderas.unset(self.ca.aa, 10)
                self.banderas.set(self.ca.aa, bandera)
        self.mostrar_banderas()


    #@Logging
    def mostrar_banderas(self):
        if self.baa:
            if self.baa[0] == 10:
                color = self.cfg['banderas_color_borrar']
                boton = self.bandera_borrar
            else:
                color = self.cfg['banderas_color_grupos']
                banderas = ','.join(map(str, self.baa))
                boton = self.bandera_grupos
                boton.set_markup('<b>{}</b>'.format(banderas))
                boton.override_color(Gtk.StateType.NORMAL, color)
            sensitive = True
        else:
            color = self.label_color_normal
            boton = self.bandera_vacio
            sensitive = False
        
        self.widgets['label_informacion'].override_color(Gtk.StateType.NORMAL, color)
        self.widgets['label_banderas'].set_markup(self.banderas.estado)
        self.widgets['button_bandera'].set_image(boton)
        self.widgets['button_bandera'].set_sensitive(sensitive)


    #@Logging
    def on_carpetas_cellrenderertext_nombre_editing_started(self, widget, entry, index):
        self.hotkey = False

    #@Logging
    def on_carpetas_cellrenderertext_nombre_edited(self, widget, index, nombre):
        nombre = nombre or os.path.basename(self.widgets['carpetas_liststore'][index][1])
        self.widgets['carpetas_liststore'][index][0] = nombre
        self.paths.carpetas[int(index)][0] = nombre
        self.hotkey = True

    #@Logging
    def on_carpetas_cellrenderertext_nombre_editing_canceled(self, widget):
        self.hotkey = True


    #@Logging
    @try_except
    def on_window_key_press_event(self, widget, event):
        if self.hotkey:
            key = event.keyval
            name = Gdk.keyval_name(key)
            mask = event.state & FLAGS
            self.log.sch('TECLA: name=%s, id=%s, mask=%s', name, key, mask)

            posibles = (True, self.hay_carpetas, self.hay_archivo_activo)
            conjunto = zip(posibles, self.area_mask_key_acción.values(), self.acción_método)

            for buscar, acción, método in conjunto:
                if buscar:
                    try:
                        método[acción[mask][key]]()
                        return True  # stop event
                    except KeyError:
                        pass


    #@Logging
    def set_pixbuf(self, giro=None, reflejo_h=None, reflejo_v=None):
        pixbuf = self.widgets['imagen'].get_pixbuf()
        fimagen = self.formatos.get(self.ca.aa, setter=True)

        if giro and giro % 90 == 0:
            pixbuf = pixbuf.rotate_simple(GdkPixbuf.PixbufRotation(giro % 360))
            if not fimagen.rel:
                fimagen.rel = self.buffer['rel']
            fimagen.girar(giro)

        if reflejo_h:
            pixbuf = pixbuf.flip(True)
            fimagen.reflejo('h')

        if reflejo_v:
            pixbuf = pixbuf.flip(False)
            fimagen.reflejo('v')

        self.widgets['imagen'].set_from_pixbuf(pixbuf)
        self.set_alineación()


    #@Logging
    def ir_a(self):
        rango = '1-{}'.format(len(self.ca))
        from dialogs import v_pregunta_ir_a
        setter = v_pregunta_ir_a(self.widgets['window'], rango)
        if setter and setter.isdigit():
            self.set_imagen(setter=int(setter)-1)
        elif setter:
            diferencias = []
            for archivo in self.ca:
                if setter in (archivo.nombre, archivo.base):
                    self.set_imagen(setter=archivo)
                    return
                diferencias.append((texdiff(setter, archivo.base), archivo))
            self.log.sch('semejanzas=%s', diferencias)
            self.set_imagen(setter=min(diferencias)[1])


    #@Logging
    def key_escape(self):
        if self.cfg['ventana_fullscreen']:
            self.fullscreen(False)
        else:
            self.salir()


    #@Logging
    def reset_imagen_actual(self):
        self.formatos.unset(self.ca.aa)
        self.set_zoom()


    #@Logging
    def carpetas_disponibles(self, actual=True):
        if actual:
            filtro = lambda c: True
        else:
            filtro = lambda c: True if c != self.ca else False
        return ((c.nombre, c.dirección) for c in self.carpetas if filtro(c))


    # ------------------------------------------------------------------------ #


    #@Logging
    def movcop(self, para, actual, banderas, grupos=None, archivos=None):
        carpetas = self.carpetas_disponibles(actual=actual)
        titulo = 'Elija la carpeta destino y/o el nuevo nombre para {}'.format(para)
        mba = self.cfg['banderas_multiples']
        from movcop import MovCop
        dialog = MovCop(self.widgets['window'], titulo, carpetas, mba, banderas)
        patrones, política, desmarcar, igrupo = dialog.ejecutar()
        if not patrones:
            return True  # stop

        md5_archivos = []
        if grupos:
            for grupo in patrones:
                if '{MD5}' in patrones[grupo]['nombre']:
                    md5_archivos.extend(self.banderas.get(grupo, []))
        elif '{MD5}' in patrones['nombre']:
            md5_archivos.extend(archivos)

        if md5_archivos:
            key, cancel = self.movcop_md5sum(md5_archivos)
            md5 = self.wait_env[key].pop('data')['md5']
            if cancel:
                return
        else:
            md5 = {}

        var = (patrones, política, desmarcar, igrupo, grupos, archivos, md5)
        key, cancel = self.movcop_principal(*var)
        if cancel:
            self.actualizar()


    #@Logging
    @wait_task('Calculando sumas MD5')
    def movcop_md5sum(self, key, archivos):
        self.wait_env[key]['data'] = {
            'archivos': archivos,
            'md5': {},
        }


    #@Logging
    def movcop_md5sum_thread(self, key):
        data = self.wait_env[key]['data']
        n = len(data['archivos'])
        for i, archivo in enumerate(data['archivos'], 1):
            data['md5'][archivo] = md5sum(archivo.dirección)
            yield i / n


    #@Logging
    @wait_task('Moviendo/copiando/renombrando')
    def movcop_principal(self, key, *args):
        patrones, política, desmarcar, igrupo, grupos, archivos, md5 = args
        data = {
            'copiar': {},
            'nuevas': set(),
            'eliminar': set(),
            'renombrar': {},
        }
        i = 0
        if grupos:
            for grupo in grupos:
                if igrupo:
                    i = 0
                if grupo in patrones:
                    for archivo in copy(self.banderas.get(grupo, [])):
                        self.banderas.unset(archivo, grupo)
                        var = (i, patrones[grupo], archivo, política, md5)
                        resultado = self.movcop_archivo(*var)
                        if resultado:
                            copiar, eliminar, renombrar, nuevas = resultado
                            data['copiar'].update(copiar)
                            data['nuevas'].update(nuevas)
                            data['eliminar'].update(eliminar)
                            data['renombrar'].update(renombrar)
                            i += 1
                elif desmarcar:
                    self.banderas.unset(banderas=grupo)
        else:
            # Se procesa sobre un archivo o una carpeta
            for archivo in copy(archivos):
                if desmarcar:
                    self.banderas.unset(archivo)
                var = (i, patrones, archivo, política, md5)
                resultado = self.movcop_archivo(*var)
                if resultado:
                    copiar, eliminar, renombrar, nuevas = resultado
                    data['copiar'].update(copiar)
                    data['nuevas'].update(nuevas)
                    data['eliminar'].update(eliminar)
                    data['renombrar'].update(renombrar)
                    i += 1

        self.wait_env[key]['data'] = data


    #@Logging
    def movcop_archivo(self, i, patrón, archivo, política, md5):
        copiar = {}
        nuevas = set()
        eliminar = set()
        renombrar = {}
        números = (i + offset for offset in patrón['offsets'])
        parámetros = {
            'BASE': archivo.base,
            'MD5': md5.get(archivo.dirección),
            'FECHA': archivo.mfecha(),
            'EXT': archivo.ext,
        }
        nombre = patrón['nombre'].format(*números, **parámetros)
        try:
            carpeta = self.carpetas[patrón['carpeta']]
            carpeta = carpeta if carpeta != archivo.carpeta else None
        except KeyError:
            if patrón['carpeta']:
                carpeta = Carpeta(patrón['carpeta'], self.cfg['imagen'],
                    self.paths.lisneg)
                nuevas.add(carpeta)
            else:
                carpeta = None
        _carpeta = (carpeta or archivo.carpeta).dirección
        _nombre = nombre or archivo.nombre
        nueva_dirección = os.path.join(_carpeta, _nombre)

        if nueva_dirección != archivo.dirección:
            if os.path.exists(nueva_dirección):
                if política == 'ignorar':
                    return
                elif política == 'sobrescribir':
                    eliminar.add(nueva_dirección)
                else:
                    iguales = filecmp.cmp(archivo.dirección, nueva_dirección)

                    if self.movcop_destino:
                        _carpeta = self.movcop_destino.dirección
                        nueva_dirección = os.path.join(_carpeta, _nombre)
                        if os.path.exists(nueva_dirección):
                            preguntar = True
                            destino = self.movcop_destino
                        else:
                            preguntar = False
                            carpeta = self.movcop_destino
                    else:
                        preguntar = True
                        destino = None

                    if preguntar:
                        from dialogs import v_pregunta_archivo
                        var = (self.widgets['window'], _carpeta, _nombre, iguales, destino)
                        nombre, _carpeta, misma = v_pregunta_archivo(*var)
                        if _carpeta:
                            try:
                                carpeta = self.carpetas[_carpeta]
                                carpeta = carpeta if carpeta != archivo.carpeta else None
                            except KeyError:
                                if _carpeta:
                                    carpeta = Carpeta(_carpeta, self.cfg['imagen'],
                                        self.paths.lisneg)
                                    nuevas.add(carpeta)
                                else:
                                    carpeta = None

                        if not (nombre or carpeta):
                            return

                        self.movcop_destino = carpeta if misma else None

            if patrón['acción'] == 'copiar':
                copiar[archivo] = (carpeta, nombre)

            elif patrón['acción'] == 'mover':
                fimagen = self.formatos.pop(archivo, default=False)
                banderas = self.banderas.pop(archivo, default=False)
                renombrar[archivo] = (carpeta, nombre, fimagen, banderas)

            else:
                raise ValueError('acción erronea "{}"'.format(patrón['acción']))

            return (copiar, eliminar, renombrar, nuevas)


    #@Logging
    def movcop_principal_thread(self, key):
        data = self.wait_env[key]['data']
        borseg = self.cfg.borseg
        n = len(data['eliminar']) + len(data['copiar']) + len(data['renombrar'])
        i = 0

        for i, dirección in enumerate(data['eliminar'], i + 1):
            método, comando = fborrar(dirección, borseg)
            self.log.info('Se eliminó [%s%s]: %s', método, comando, dirección)
            yield i / n

        ordenar = set()  # eficiente para muchos archivos

        for i, archivo in enumerate(data['copiar'], i + 1):
            carpeta, nombre = data['copiar'][archivo]
            nuevo_archivo = archivo.copiar(carpeta, nombre, ordenar=False)
            ordenar.add(nuevo_archivo.carpeta)
            yield i / n

        for i, archivo in enumerate(data['renombrar'], i + 1):
            carpeta, nombre, fimagen, banderas = data['renombrar'][archivo]
            archivo.renombrar(carpeta, nombre, borseg, ordenar=False)
            ordenar.add(carpeta or archivo.carpeta)
            if fimagen:
                self.formatos.set(archivo, fimagen)
            if banderas:
                self.banderas.set(archivo, banderas, unicos=True)
            yield i / n

        for carpeta in ordenar:
            carpeta.ordenar()

        yield 1


    #@Logging
    def movcop_principal_complete(self, key):
        data = self.wait_env[key].pop('data')
        if data['nuevas']:
            self.agregar_nuevas_carpetas(data['nuevas'])
        self.set_imagen(forzar=True)


    #@Logging
    @wait_task('Normalizando archivos')
    def normalizar(self, key, archivos, política):
        data = {
            'renombrar': {},
            'eliminar': set(),
            'nuevas': set(),
        }
        for archivo in copy(archivos):
            ext = archivo.ext_format
            if ext != archivo.ext:
                banderas = self.banderas.pop(archivo, default=False)
                fimagen = self.formatos.pop(archivo, default=False)
                nombre = '{}{}'.format(archivo.base, ext)
                carpeta = None  # la misma, excepto que cambie
                _carpeta = archivo.carpeta.dirección
                dirección = os.path.join(_carpeta, nombre)

                if os.path.exists(dirección):
                    if política == 'ignorar':
                        continue
                    elif política == 'sobrescribir':
                        obj = self.carpetas.get_archivo(dirección)  # lento, se usa poco
                        obj.carpeta.eliminar(obj)
                        self.banderas.unset(obj)
                        self.formatos.unset(obj)
                        data['eliminar'].add(dirección)
                    else:
                        iguales = filecmp.cmp(archivo.dirección, dirección)
                        var = (self.widgets['window'], _carpeta, nombre, iguales, 
                            self.normalizar_destino, 'normalizar', True)
                        from dialogs import v_pregunta_archivo
                        nombre, _carpeta, misma = v_pregunta_archivo(*var)
                        self.normalizar_destino = _carpeta if misma else None
                        if _carpeta:
                            carpeta = self.carpetas.get(_carpeta)
                            if not carpeta:
                                carpeta = Carpeta(_carpeta, self.cfg['imagen'],
                                    self.paths.lisneg)
                                data['nuevas'].add(carpeta)
                data['renombrar'][archivo] = (carpeta, nombre, banderas, fimagen)
        
        if data['renombrar'] or data['eliminar'] or data['nuevas']:
            self.wait_env[key]['data'] = data
        else:
            return True  # stop


    #@Logging
    def normalizar_thread(self, key):
        data = self.wait_env[key]['data']
        borseg = self.cfg.borseg
        n = len(data['eliminar']) + len(data['renombrar'])
        i = 0

        for i, dirección in enumerate(data['eliminar'], i + 1):
            método, comando = fborrar(dirección, borseg)
            self.log.info('Se eliminó [%s%s]: %s', método, comando, dirección)
            yield i / n

        ordenar = set()  # eficiente para muchos archivos

        for i, archivo in enumerate(data['renombrar'], i + 1):
            carpeta, nombre, banderas, fimagen = data['renombrar'][archivo]
            archivo.renombrar(carpeta, nombre, borseg, ordenar=False)
            ordenar.add(carpeta or archivo.carpeta)
            if fimagen:
                self.formatos.set(archivo, fimagen)
            if banderas:
                self.banderas.set(archivo, banderas, unicos=True)
            yield i / n

        for carpeta in ordenar:
            carpeta.ordenar()

        yield 1


    #@Logging
    def normalizar_complete(self, key):
        data = self.wait_env[key].pop('data')
        if data['nuevas']:
            self.agregar_nuevas_carpetas(data['nuevas'])
        self.set_imagen(forzar=True)


    #@Logging
    @wait_task('Borrando archivos')
    def borrar(self, key, objetivo, archivos):
        if objetivo is True:
            borrar = True
        else:
            mjs = '¿Desea borrar {}?'.format(objetivo)
            aclaración = 'Esta operación borra las imágenes del disco.'
            from dialogs import v_pregunta_si_no
            borrar = v_pregunta_si_no(self.widgets['window'], mjs, aclaración)
        if not borrar:
            return True  # stop
        self.wait_env[key]['data'] = {'archivos': archivos}

    #@Logging
    def borrar_thread(self, key):
        data = self.wait_env[key]['data']
        borseg = self.cfg.borseg
        n = len(data['archivos'])
        for i, archivo in enumerate(copy(data['archivos']), 1):
            self.banderas.unset(archivo)
            self.formatos.unset(archivo)
            archivo.borrar(borseg)
            yield i / n

    #@Logging
    def borrar_complete(self, key):
        self.wait_env[key].pop('data')
        self.set_imagen(forzar=True)


    #@Logging
    @wait_task('Limpiando archivos')
    def limpiar(self, key, archivos):
        self.wait_env[key]['data'] = {'archivos': archivos}

    #@Logging
    def limpiar_thread(self, key):
        data = self.wait_env[key]['data']
        n = len(data['archivos'])
        for i, archivo in enumerate(copy(data['archivos']), 1):
            subprocess.call(['mat', archivo.dirección])
            yield i / n

    #@Logging
    def limpiar_complete(self, key):
        self.wait_env[key].pop('data')
        self.set_imagen(forzar=True)


    #@Logging
    @wait_task('Calculando huellas y comparando')
    def similares(self, key, objetivo=None):
        from dialogs import v_pregunta_similares
        respuesta = v_pregunta_similares(self.widgets['window'], objetivo, self.fph.hdepth)
        if not respuesta:
            return True  # stop
        fps, rot, hist, color, nivel, obj, local = respuesta
        data = {
            'fps': fps,
            'rot': rot,
            'hist': hist,
            'color': color,
            'max_diff': 1 - nivel,
            'archivos': [],
            'objetivo': None,
        }
        if obj == 'Todos':
            for carpeta in self.carpetas:
                data['archivos'].extend(carpeta.archivos)
        elif isinstance(obj, (list, tuple)):
            for b in obj:
                data['archivos'].extend(self.banderas.get(b))
        else:
            if isinstance(obj, Carpeta):
                data['archivos'].extend(obj.archivos)
            else:  # isinstance(obj, Archivo)
                if local:
                    data['archivos'].extend(obj.carpeta.archivos)
                else:
                    for carpeta in self.carpetas:
                        data['archivos'].extend(carpeta.archivos)
                data['objetivo'] = obj

        self.wait_env[key]['data'] = data


    #@Logging
    def similares_thread(self, key):
        data = self.wait_env[key]['data']
        fhget = (data['fps'], data['rot'], data['hist'], data['color'])
        sim = data['sim'] = {}

        self.fph.cache.activar(True)

        if data['objetivo']:
            f, h, err = self.fph.get(data['objetivo'], *fhget)
            if err or not (f or h):
                yield 1
            resto = (i for i in data['archivos'] if i != data['objetivo'])
            conjunto = product([data['objetivo']], resto)
            n = len(data['archivos']) - 1
        else:
            conjunto = combinations(data['archivos'], 2)
            n = ncombinaciones(len(data['archivos']), 2)

        plural = 'comparación' if n == 1 else 'comparaciones'
        self.wait_env[key]['sublabel'] = 'Realizando {} {}'.format(n, plural)
        if n > 40:
            self.wait_env[key]['n'] = n
        n += 1
        yield 1 / n

        grupos = {}
        for i, (a1, a2) in enumerate(conjunto, 2):
            f1, h1, err1 = self.fph.get(a1, *fhget)
            f2, h2, err2 = self.fph.get(a2, *fhget)
            if not (err1 or err2):
                if data['fps']:
                    for x, y in product(f1, f2):
                        if self.fph.diff_fp(x, y) <= data['max_diff']:
                            fsim = True
                            break
                    else:
                        fsim = False
                else:
                    fsim = True
                if data['hist']:
                    if h1 and h2 and self.fph.diff_hist(h1, h2) <= data['max_diff']:
                        hsim = True
                    else:
                        hsim = False
                else:
                    hsim = True if data['fps'] else False  # evita falso positivo
                if fsim and hsim:
                    g = grupos.get(a1) or grupos.get(a2) or len(sim) + 1
                    sim.setdefault(g, set()).update([a1, a2])
                    grupos[a1] = g
                    grupos[a2] = g
            yield i / n

        self.fph.cache.activar(False)

        if data['objetivo']:
            g = sim.get(1)
            if g:
                g.remove(data['objetivo'])
                sim[1] = [data['objetivo']] + sorted(g, key=normalizar)
        else:
            for g in sim:
                sim[g] = sorted(sim[g], key=normalizar)

        yield 1


    #@Logging
    def similares_complete(self, key):
        data = self.wait_env[key].pop('data')

        if self.fph.errores:
            n = len(self.fph.errores)
            plural = (n, 'imagen', '', '') if n == 1 else (n, 'imágenes', 'n', 's')
            mjs = 'Error cargando {0} {1}, '.format(*plural)
            mjs += 'será{2} agregada{3} a la lista negra.'.format(*plural)
            from dialogs import v_información
            v_información(self.widgets['window'], 'ERROR', mjs)
            for archivo in self.fph.errores:
                self.banderas.unset(archivo)
                self.formatos.unset(archivo)
                self.paths.add_lisneg(archivo.dirección)
                archivo.carpeta.eliminar(archivo)
            self.fph.errores.clear()

        if data['sim']:
            ancho = self.cfg['fingerprint_imagen_ancho']
            alto = self.cfg['fingerprint_imagen_alto']
            tam = self.cfg['fingerprint_mostrar_tamaño']
            carpetas = self.carpetas_disponibles(actual=True)
            from listas import ListaSimilares
            dialog = ListaSimilares(self.widgets['window'], data['sim'], carpetas,
                self.banderas, self.movcop, self.borrar, (ancho, alto, tam))
            dialog.ejecutar()
            self.set_imagen(forzar=True)
        else:
            mjs = 'No se encontraron similitudes entre las imágenes'
            from dialogs import v_información
            v_información(self.widgets['window'], 'Sin resultados', mjs)


    # ------------------------------------------------------------------------ #


    @staticmethod
    def posmenu(menu, data):
        widget, event = data
        x_offset = - menu.get_allocated_width()
        y_offset = 0
        x = event.x_root - event.x + widget.get_allocated_width() + x_offset
        y = event.y_root - event.y + widget.get_allocated_height() + y_offset
        push_in = True
        return (x, y, push_in)


    #@Logging
    @try_except
    def on_button_menu_button_press_event(self, widget, event):
        grupos = bool(self.banderas.get_activas(range(1, 10)))
        borrar = bool(self.banderas.get_activas(10))
        modifi = bool(self.formatos.buff() or self.formatos.buscar_cambios())
        estados = (
            ('action_borrar',       borrar),
            ('action_movcop',       grupos),
            ('action_limpiar',      grupos),
            ('action_olvidar',      grupos or borrar),
            ('action_normalizar',   grupos),
            ('action_modificados',  modifi),
            ('action_freset',       bool(self.formatos)),
            ('action_lisneg',       bool(self.paths.lisneg)),
            ('action_similares',    bool(len(self))),
        )
        for w, estado in estados:
            self.widgets[w].set_sensitive(estado)
        var = (None, None, self.posmenu, (widget, event), event.button, event.time)
        self.widgets['menu'].popup(*var)
        return True  # stop event


    #@Logging
    @try_except
    def on_action_movcop_activate(self, widget):
        banderas = self.banderas.get_activas(range(1, 10))
        actual = bool(len(banderas) > 1)
        para = '{} grupo'.format('el' if len(banderas) == 1 else 'cada')
        self.movcop(para, actual, banderas, grupos=banderas)


    #@Logging
    @try_except
    def on_action_limpiar_activate(self, widget):
        grupos = self.banderas.get_activas(range(1, 10))
        título = 'Limpiar archivos'
        mjs = 'Elija que grupos de archivos desea limpiar'
        from dialogs import v_pregunta_elegir_banderas
        respuesta = v_pregunta_elegir_banderas(self.widgets['window'], 
            título, mjs, grupos)
        if respuesta:
            archivos = []
            for grupo in respuesta:
                archivos.extend(self.banderas.get(grupo, []))
            self.banderas.unset(banderas=respuesta)
            self.limpiar(archivos)


    #@Logging
    @try_except
    def on_action_borrar_activate(self, widget):
        objetivo = 'las imágenes marcadas con B'
        archivos = list(self.banderas.get(10))
        self.borrar(objetivo, archivos)


    #@Logging
    @try_except
    def on_action_similares_activate(self, widget):
        self.similares()


    #@Logging
    @try_except
    def on_action_normalizar_activate(self, widget):
        grupos = self.banderas.get_activas(range(1, 10))
        título = 'Normalizar extensiones'
        mjs = 'Elija que grupos de archivos\ndesea normalizar sus extensiones'
        from dialogs import plugin_pis, v_pregunta_elegir_banderas
        pis = plugin_pis(orientation='m', top=15)
        respuesta = v_pregunta_elegir_banderas(self.widgets['window'], 
            título, mjs, grupos, widget=pis)
        if respuesta:
            política = pis.get()
            archivos = []
            for grupo in respuesta:
                archivos.extend(self.banderas.get(grupo, []))
            self.banderas.unset(banderas=respuesta)
            self.normalizar(archivos, política)


    #@Logging
    @try_except
    def on_action_olvidar_activate(self, widget):
        mjs = '¿Desea olvidar todas las marcas?'
        aclaración = 'Los grupos del 1 al 9'
        from dialogs import v_pregunta_si_no
        respuesta = v_pregunta_si_no(self.widgets['window'], mjs, aclaración)
        if respuesta:
            self.banderas.unset()
            self.set_imagen(forzar=True)


    #@Logging
    @try_except
    def on_action_freset_activate(self, widget):
        mjs = '¿Desea olvidar todos los formatos?'
        aclaración = 'Datos de zoom, alineación, giro y reflejos'
        from dialogs import v_pregunta_si_no
        respuesta = v_pregunta_si_no(self.widgets['window'], mjs, aclaración)
        if respuesta:
            self.formatos.reset()
            self.set_imagen(forzar=True)


    #@Logging
    @try_except
    def on_action_panel_carpetas_activate(self, widget):
        self.carpetas_activar_panel()


    #@Logging
    @try_except
    def on_action_keymap_activate(self, widget):
        keymaps = self.encode_keymaps()
        from listas import ListaCombinacionesTeclas
        dialog = ListaCombinacionesTeclas(self.widgets['window'], keymaps)
        response = dialog.ejecutar()
        if response:
            if response == 'Reset':
                self.getcfg.borrar(self.cfg.borseg, 'keymap')
                self.keymap_gen_mask_key_acción()
            else:
                self.decode_keymaps(response)


    #@Logging
    @try_except
    def on_action_modificados_activate(self, widget):
        if self.formatos.buffer:
            mjs = '¿Desea cargar todos los formatos del archivo?'
            aclaración = 'Sino verá solo las modificaciones de las imágenes que visitó.'
            from dialogs import v_pregunta_si_no
            respuesta = v_pregunta_si_no(self.widgets['window'], mjs, aclaración)
            if respuesta:
                for carpeta in self.carpetas:
                    for archivo in carpeta.archivos:
                        self.formatos.cargar_archivo(archivo)
        carpetas = self.carpetas_disponibles()
        base = (self.cfg, self.carpetas, self.formatos, self.banderas)
        convert = self.cfg['ventana_usar_convert'] and self.cfg['convert']
        from listas import ListaModificados
        var = (carpetas, base, convert, self.cfg.borseg)
        dialog = ListaModificados(self.widgets['window'], *var)
        response = dialog.ejecutar()
        if response:
            self.agregar_nuevas_carpetas(response)
        self.set_imagen(forzar=True)


    #@Logging
    @try_except
    def on_action_lisneg_activate(self, widget):
        lisneg = self.paths.lisneg
        borseg = self.cfg.borseg
        carpetas = self.carpetas_disponibles()
        from listas import ListaNegra
        dialog = ListaNegra(self.widgets['window'], lisneg, carpetas, borseg)
        actulizar_carpetas = dialog.ejecutar()
        if actulizar_carpetas:
            self.actualizar_carpetas(forzar=True)
        self.set_imagen(forzar=True)


    #@Logging
    @try_except
    def on_action_configurar_activate(self, widget):
        from config import Config
        dialog = Config(self.widgets['window'], self.cfg)
        respuesta = dialog.ejecutar()
        if respuesta:
            if respuesta == 'Borrar':
                self.getcfg.borrar(self.cfg.borseg)
                for carpeta in self.carpetas:
                    for archivo in carpeta:
                        self.banderas.unset(archivo)
                        self.formatos.unset(archivo)
                    self.carpetas.eliminar(carpeta)
                self.widgets['carpetas_liststore'].clear()
                self.cfg.reset()
                self.fph.reset()
                self.paths.reset()
                self.i = None
            elif respuesta == 'Actualizar':
                self.actualizar_carpetas(forzar=True)
            self.keymap_gen_mask_key_acción()
            self.actualizar()
            self.set_imagen(forzar=True)


    #@Logging
    @try_except
    def on_action_salir_activate(self, widget):
        self.salir()


    # ------------------------------------------------------------------------ #


    #@Logging
    @try_except
    def on_carpetas_treeview_button_release_event(self, widget, event):
        if event.button == Gdk.BUTTON_SECONDARY:
            ca = bool(self.ca and self.ca.archivos)
            self.widgets['actiongroup_menu_contextual_carpetas_imagenes'].set_sensitive(ca)
            self.widgets['action_carpetas_similares'].set_sensitive(ca)
            self.widgets['action_carpetas_quitar'].set_sensitive(bool(self.carpetas))
            var = (None, None, None, None, event.button, event.time)
            self.widgets['menu_contextual_carpetas'].popup(*var)
            return True  # stop event


    #@Logging
    @try_except
    def on_action_carpetas_movcop_activate(self, widget):
        banderas = len(self.ca)
        actual = False
        para = 'las imágenes'
        self.movcop(para, actual, banderas, archivos=self.ca.archivos)


    #@Logging
    @try_except
    def on_action_carpetas_limpiar_activate(self, widget):
        mjs = '¿Desea limpiar las imágenes de esta carpeta?'
        aclaración = 'No se modificarán los grupos de los archivos.'
        from dialogs import v_pregunta_si_no
        respuesta = v_pregunta_si_no(self.widgets['window'], mjs, aclaración)
        if respuesta:
            self.limpiar(self.ca.archivos)


    #@Logging
    @try_except
    def on_action_carpetas_borrar_activate(self, widget):
        objetivo = 'las imágenes de esta carpeta'
        archivos = self.ca.archivos
        self.borrar(objetivo, archivos)


    #@Logging
    @try_except
    def on_action_carpetas_similares_activate(self, widget):
        self.similares(self.ca)


    #@Logging
    @try_except
    def on_action_carpetas_normalizar_activate(self, widget):
        mjs = '¿Desea normalizar las extensiones de las imágenes de esta carpeta?'
        aclaración = ('Elimina caracteres diacríticos, convierte en minúscula '
            'y reemplaza .jpeg por .jpg.')
        from dialogs import plugin_pis, v_pregunta_si_no
        pis = plugin_pis(left=55)
        respuesta = v_pregunta_si_no(self.widgets['window'], mjs, aclaración, pis)
        if respuesta:
            política = pis.get()
            self.normalizar(self.ca.archivos, política)


    #@Logging
    @try_except
    def on_action_carpetas_agregar_activate(self, widget):
        título = 'Elija la/s carpeta/s a agregar (repetidas se ignoran)'
        current = os.path.dirname(self.paths.cultima)
        from dialogs import v_pregunta_carpeta
        respuesta = v_pregunta_carpeta(self.widgets['window'], título, current, multiple=True)
        if respuesta:
            carpetas = [(os.path.basename(d), d) for d in respuesta if d not in self.carpetas]
            self.agregar_carpetas(carpetas)
            if not self.ca:
                self.set_carpeta(setter=0)


    #@Logging
    @try_except
    def on_action_carpetas_quitar_activate(self, widget):
        mjs = '¿Desea quitar esta carpeta?'
        aclaración = 'También será quitada del historial (no se borra del disco).'
        from dialogs import v_pregunta_si_no
        respuesta = v_pregunta_si_no(self.widgets['window'], mjs, aclaración)
        if respuesta:
            modelo, iterador = self.widgets['carpetas_selection'].get_selected()
            for archivo in self.ca:
                self.banderas.unset(archivo)
                self.formatos.unset(archivo)
            self.paths.remove_carpeta(self.ca.dirección)
            self.carpetas.eliminar(self.ca)
            self.widgets['carpetas_liststore'].remove(iterador)


    # ------------------------------------------------------------------------ #


    #@Logging
    @try_except
    def on_imagen_viewport_button_press_event(self, widget, event):
        if event.button == Gdk.BUTTON_SECONDARY and self.hay_archivo_activo:
            var = (None, None, None, None, event.button, event.time)
            self.widgets['menu_contextual_imagen'].popup(*var)
            return True  # stop event


    #@Logging
    @try_except
    def on_action_imagen_movcop_activate(self, widget):
        banderas = 1
        actual = False
        para = 'la imagen'
        self.movcop(para, actual, banderas, archivos=[self.ca.aa])


    #@Logging
    @try_except
    def on_action_imagen_limpiar_activate(self, widget):
        mjs = '¿Desea limpiar la imagen actual?'
        aclaración = 'No se modificarán sus grupos.'
        from dialogs import v_pregunta_si_no
        respuesta = v_pregunta_si_no(self.widgets['window'], mjs, aclaración)
        if respuesta:
            self.limpiar([self.ca.aa])


    #@Logging
    @try_except
    def on_action_imagen_borrar_activate(self, widget):
        objetivo = 'la imagen actual'
        archivos = [self.ca.aa]
        self.borrar(objetivo, archivos)


    #@Logging
    @try_except
    def on_action_imagen_similares_activate(self, widget):
        self.similares(self.ca.aa)


    #@Logging
    @try_except
    def on_action_imagen_normalizar_activate(self, widget):
        mjs = '¿Desea normalizar la extensión de la imagen actual?'
        aclaración = ('Elimina caracteres diacríticos, convierte en minúscula '
            'y reemplaza .jpeg por .jpg.')
        from dialogs import plugin_pis, v_pregunta_si_no
        pis = plugin_pis(left=55)
        respuesta = v_pregunta_si_no(self.widgets['window'], mjs, aclaración, pis)
        if respuesta:
            política = pis.get()
            self.normalizar([self.ca.aa], política)


    #@Logging
    @try_except
    def on_action_imagen_copiar_direccion_activate(self, widget):
        self.clipboard.set_text(self.ca.aa.dirección, -1)


    #@Logging
    @try_except
    def on_action_imagen_abrir_gimp_activate(self, widget):
        timeout = self.cfg['imagen_watchdog']
        if timeout:
            GObject.timeout_add(timeout, self.sentinela, Sentinela(self.ca.aa))
        subprocess.Popen(['gimp', self.ca.aa.dirección])


    #@Logging
    def sentinela(self, sen):
        if self.hay_archivo_activo and sen.archivo == self.ca.aa:
            os.system('sync')
            if sen.get_actualizar():
                self.set_imagen(forzar=True)
            return True


    # ------------------------------------------------------------------------ #


    #@Logging
    def encode_keymaps(self):
        keymaps = collections.OrderedDict()
        for area, mka in self.area_mask_key_acción.items():
            acciones = set()
            for mask, key_acción in mka.items():
                for key, acción in key_acción.items():
                    hotkey = maskkey_to_hotkey((mask, key))
                    keymaps.setdefault(area, {}).setdefault(acción, []).append(hotkey)
                    acciones.add(acción)
            for acción in (self.area_setacción[area] - acciones):
                keymaps.setdefault(area, {}).setdefault(acción, []).append('')
        return keymaps


    #@Logging
    def decode_keymaps(self, keymaps):
        self.ventana_mask_key_acción.clear()
        self.carpetas_mask_key_acción.clear()
        self.archivos_mask_key_acción.clear()
        for area, acción_hotkeys in keymaps.items():
            for acción, hotkeys in acción_hotkeys.items():
                for hotkey in hotkeys:
                    mask, key = hotkey_to_maskkey(hotkey)
                    if key:
                        self.area_mask_key_acción[area].setdefault(mask, {})[key] = acción


    #@Logging
    def keymap_gen_mask_key_acción(self):
        self.ventana_mask_key_acción = {
            NO_MASK: {
                Gdk.KEY_Escape:         'escape',
                Gdk.KEY_f:              'similares',
                Gdk.KEY_F9:             'decoración',
                Gdk.KEY_F10:            'maximizado',
                Gdk.KEY_F11:            'fullscreen',
                Gdk.KEY_F12:            'panel carpeta',
            },
            CONTROL: {
                Gdk.KEY_Escape:         'salir',
            },
        }
        self.carpetas_mask_key_acción = {
            NO_MASK: {
                Gdk.KEY_Up:             'retroceso',
                Gdk.KEY_Down:           'avance',
            },
            CONTROL: {
                Gdk.KEY_Up:             'retroceso multiple',
                Gdk.KEY_Down:           'avance multiple',
            },
            SHIFT: {
                Gdk.KEY_Up:             'comienzo',
                Gdk.KEY_Down:           'final',
            },
        }
        self.archivos_mask_key_acción = {
            NO_MASK: {
                Gdk.KEY_i:              'ir a',
                Gdk.KEY_Home:           'comienzo',
                Gdk.KEY_Page_Up:        'retroceso multiple',
                Gdk.KEY_Left:           'retroceso',
                Gdk.KEY_Right:          'avance',
                Gdk.KEY_Page_Down:      'avance multiple',
                Gdk.KEY_End:            'final',
                Gdk.KEY_KP_Subtract:    'menos zoom',
                Gdk.KEY_minus:          'menos zoom',
                Gdk.KEY_KP_0:           'no zoom',
                Gdk.KEY_0:              'no zoom',
                Gdk.KEY_KP_Add:         'mas zoom',
                Gdk.KEY_plus:           'mas zoom',
                Gdk.KEY_e:              'girar izquierda',
                Gdk.KEY_r:              'girar derecha',
                Gdk.KEY_h:              'reflejo horizontal',
                Gdk.KEY_v:              'reflejo vertical',
                Gdk.KEY_1:              'grupo 1',
                Gdk.KEY_2:              'grupo 2',
                Gdk.KEY_3:              'grupo 3',
                Gdk.KEY_4:              'grupo 4',
                Gdk.KEY_5:              'grupo 5',
                Gdk.KEY_6:              'grupo 6',
                Gdk.KEY_7:              'grupo 7',
                Gdk.KEY_8:              'grupo 8',
                Gdk.KEY_9:              'grupo 9',
                Gdk.KEY_KP_1:           'grupo 1',
                Gdk.KEY_KP_2:           'grupo 2',
                Gdk.KEY_KP_3:           'grupo 3',
                Gdk.KEY_KP_4:           'grupo 4',
                Gdk.KEY_KP_5:           'grupo 5',
                Gdk.KEY_KP_6:           'grupo 6',
                Gdk.KEY_KP_7:           'grupo 7',
                Gdk.KEY_KP_8:           'grupo 8',
                Gdk.KEY_KP_9:           'grupo 9',
                Gdk.KEY_Delete:         'borrar',
            },
            CONTROL: {
                Gdk.KEY_KP_0:           'reset',
                Gdk.KEY_Page_Up:        'retroceso multiple 10',
                Gdk.KEY_Left:           'retroceso multiple',
                Gdk.KEY_Right:          'avance multiple',
                Gdk.KEY_Page_Down:      'avance multiple 10',
            },
        }
        self.area_mask_key_acción = collections.OrderedDict((
            # area      {mask: {key: 'acción'}}
            ('ventana',  self.ventana_mask_key_acción),
            ('carpetas', self.carpetas_mask_key_acción),
            ('archivos', self.archivos_mask_key_acción),
        ))


    #@Logging
    def keymap_gen_acción_método(self):
        from functools import partial as f
        
        am = self.cfg['avances_multiples']
        menzf = float(1 / self.cfg['imagen_zoom_factor'])
        maszf = float(self.cfg['imagen_zoom_factor'])
        
        self.ventana_acción_método = {
            'decoración':               f(self.decoración),
            'maximizado':               f(self.maximize),
            'fullscreen':               f(self.fullscreen),
            'panel carpeta':            f(self.carpetas_activar_panel),
            'escape':                   f(self.key_escape),
            'salir':                    f(self.salir),
            'similares':                f(self.similares),
        }
        self.carpetas_acción_método = {
            'comienzo':                 f(self.set_carpeta, offset='ini'),
            'retroceso multiple':       f(self.set_carpeta, offset=-am),
            'retroceso':                f(self.set_carpeta, offset=-1),
            'avance':                   f(self.set_carpeta, offset=+1),
            'avance multiple':          f(self.set_carpeta, offset=+am),
            'final':                    f(self.set_carpeta, offset='fin'),
        }
        self.archivos_acción_método = {
            'ir a':                     f(self.ir_a),
            'reset':                    f(self.reset_imagen_actual),
            'menos zoom':               f(self.set_zoom, factor=menzf),
            'no zoom':                  f(self.set_zoom, factor=1),
            'mas zoom':                 f(self.set_zoom, factor=maszf),
            'comienzo':                 f(self.set_imagen, offset='ini'),
            'retroceso multiple 10':    f(self.set_imagen, offset=-am * 10),
            'retroceso multiple':       f(self.set_imagen, offset=-am),
            'retroceso':                f(self.set_imagen, offset=-1),
            'avance':                   f(self.set_imagen, offset=+1),
            'avance multiple':          f(self.set_imagen, offset=+am),
            'avance multiple 10':       f(self.set_imagen, offset=+am * 10),
            'final':                    f(self.set_imagen, offset='fin'),
            'girar izquierda':          f(self.set_pixbuf, giro=+90),
            'girar derecha':            f(self.set_pixbuf, giro=-90),
            'reflejo horizontal':       f(self.set_pixbuf, reflejo_h=True),
            'reflejo vertical':         f(self.set_pixbuf, reflejo_v=True),
            'grupo 1':                  f(self.set_bandera, bandera=1),
            'grupo 2':                  f(self.set_bandera, bandera=2),
            'grupo 3':                  f(self.set_bandera, bandera=3),
            'grupo 4':                  f(self.set_bandera, bandera=4),
            'grupo 5':                  f(self.set_bandera, bandera=5),
            'grupo 6':                  f(self.set_bandera, bandera=6),
            'grupo 7':                  f(self.set_bandera, bandera=7),
            'grupo 8':                  f(self.set_bandera, bandera=8),
            'grupo 9':                  f(self.set_bandera, bandera=9),
            'borrar':                   f(self.set_bandera, bandera=10),
        }
        self.acción_método = (
            # {'acción': metodo}
            self.ventana_acción_método,
            self.carpetas_acción_método,
            self.archivos_acción_método,
        )
        self.area_setacción = {
            # area      set('acción')
            'ventana':  set(self.ventana_acción_método),
            'carpetas': set(self.carpetas_acción_método),
            'archivos': set(self.archivos_acción_método),
        }


    #@Logging
    def keymap_cargar(self):
        self.log.info('Cargando keymap')
        try:
            with open(self.keymap_archivo.path, mode='r', encoding='utf-8') as fichero:
                keymaps = json.load(fichero)
            self.decode_keymaps(keymaps)
        except:
            exctype, value = sys.exc_info()[:2]
            self.log.debug('ERROR en keymap: %s: %s', exctype.__name__, value)
            self.keymap_gen_mask_key_acción()  # default


    #@Logging
    def keymap_guardar(self):
        self.log.info('Guardando keymap')
        keymaps = self.encode_keymaps()
        with open(self.keymap_archivo.path, mode='w', encoding='utf-8') as fichero:
            json.dump(keymaps, fichero, ensure_ascii=False, indent=2, sort_keys=True)


