#!/usr/bin/python3
# -*- coding: utf-8 -*-

from cx_Freeze import setup, Executable

import os
import sys

import gi
import gi.repository
from gi.repository import Gtk
from gi._gi import _API

PATH = os.path.abspath(os.path.dirname(__file__))
sys.path.insert(0, os.path.join(PATH, 'py'))

import main

os.chdir(PATH)

setup(
    name = "SCHVisor",
    author = "Schmidt Cristian Hernán",
    version = main.VERSION,
    description = "SCHVisor",
    options = {
        "build_exe": {
	        "includes": ["gi", "gi.overrides.Gtk"],
            "packages": ["gi"],
            "optimize": 2,
            "compressed": True,
        }
    },
	executables = [Executable(**{
        "script": "main.py",
        "compress": True,
        "targetName": "schvisor",
    })],
)

# build with "python3 freeze.py build" or "./freeze.py build"
